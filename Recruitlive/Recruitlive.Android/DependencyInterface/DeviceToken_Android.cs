using System;
using Recruitlive.DependencyInterface;
using Xamarin.Forms;
using Recruitlive.Droid.DependencyInterface;
using Android.Telephony;

[assembly: Dependency(typeof(DeviceToken_Android))]
namespace Recruitlive.Droid.DependencyInterface
{
    public class DeviceToken_Android : IDevice
    {
        /// <summary>
        /// getting the unique IMEI Number for the android to send in to the login method
        /// </summary>
        /// <returns></returns>
        public string GetIdentifier()
        {
            return Android.Provider.Settings.Secure.GetString(MainActivity._activity.ContentResolver, Android.Provider.Settings.Secure.AndroidId);
        
        }
    }
}