using Recruitlive.DependencyInterface;
using Recruitlive.Droid.DependencyInterface;
using Xamarin.Forms;
using System.Net;
using Android.Content.PM;
using Android.OS;

[assembly: Dependency(typeof(IPAddressManager))]
namespace Recruitlive.Droid.DependencyInterface
{
    public class IPAddressManager : IIPAddressManager
    {
        public string GetIPAddress()
        {
            IPAddress[] adresses = Dns.GetHostAddresses(Dns.GetHostName());

            if (adresses != null && adresses[0] != null)
            {
                return adresses[0].ToString();
            }
            else
            {
                return null;
            }
        }

        public string GetVersion()
        {
            string deviceOs = Build.VERSION.Release;
            return deviceOs;
        }
    }
}