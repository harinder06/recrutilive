using Recruitlive.Droid.DependencyInterface;
using Xamarin.Forms;
using Recruitlive.DependencyInterface;
using System.IO;
using System;
using Android.Content;
using System.Threading.Tasks;
using Android.Widget;

[assembly: Dependency(typeof(GeneratePdfDroid))]
namespace Recruitlive.Droid.DependencyInterface
{
    public class GeneratePdfDroid : IGeneratePDF
    {

        // Creating and saving the file based on the bytes that are fetched from the api.
        public string SaveFiles(string filename, byte[] bytes)
        {
            var directory = new Java.IO.File(Android.OS.Environment.ExternalStorageDirectory, "Recruitlive").ToString();
            if (!Directory.Exists(directory))
            {
                // creating new directory
                Directory.CreateDirectory(directory);
            }
            // generating file path with directory name and file name 
            string filePath = System.IO.Path.Combine(directory, filename + ".pdf");

            if (File.Exists(filePath))
            {
                // deleting file if already exists
                File.Delete(filePath);
            }

            // writing data to the created file
            File.WriteAllBytes(filePath, bytes);

            // opening the file 
            OpenFile(filePath, filename);
            return filePath;

        }


        /// <summary>
        /// this method will be used to open the file in the default application of the phone based on the file extension
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="filename"></param>
        public void OpenFile(string filePath, string filename)
        {

            var bytes = File.ReadAllBytes(filePath);

            //Copy the private file's data to the EXTERNAL PUBLIC location
            string externalStorageState = global::Android.OS.Environment.ExternalStorageState;
            string application = "";

            //Get extension of the file.
            string extension = System.IO.Path.GetExtension(filePath);

            switch (extension.ToLower())
            {
                case ".doc":
                case ".docx":
                    application = "application/msword";
                    break;
                case ".pdf":
                    application = "application/pdf";
                    break;
                case ".xls":
                case ".xlsx":
                    application = "application/vnd.ms-excel";
                    break;
                case ".jpg":
                case ".jpeg":
                case ".png":
                    application = "image/jpeg";
                    break;
                default:
                    application = "application/pdf";
                    break;
            }
            var externalPath = global::Android.OS.Environment.ExternalStorageDirectory.Path + "/" + filename + extension;
            File.WriteAllBytes(externalPath, bytes);

            Java.IO.File file = new Java.IO.File(externalPath);
            file.SetReadable(true);

            // opening of the file
            Android.Net.Uri uri = Android.Net.Uri.FromFile(file);
            Intent intent = new Intent(Intent.ActionView);
            intent.SetDataAndType(uri, application);
            intent.SetFlags(ActivityFlags.ClearWhenTaskReset | ActivityFlags.NewTask);

            try
            {
                Android.App.Application.Context.StartActivity(intent);
            }
            catch (Exception)
            {
                Toast.MakeText(Android.App.Application.Context, "There's no app to open pdf files", ToastLength.Long).Show();
            }
        }
    }
}
