﻿using System;
using Android.App;
using Android.Content.PM;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin;
using Android.Graphics;
using HockeyApp.Android;

namespace Recruitlive.Droid
{
    [Activity(Label = "Recruitlive.Android", Icon = "@drawable/appicon", Theme = "@style/MyTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static MainActivity _activity;
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;


            _activity = this;
            var width = Resources.DisplayMetrics.WidthPixels;
            var height = Resources.DisplayMetrics.HeightPixels;
            var density = Resources.DisplayMetrics.Density;

            App.ScreenWidth = (width - 0.5f) / density;
            App.ScreenHeight = (height - 0.5f) / density;




            base.OnCreate(bundle);


            FormsMaps.Init(this, bundle);


            global::Xamarin.Forms.Forms.Init(this, bundle);

            LoadApplication(new App());

            Window.SetSoftInputMode(Android.Views.SoftInput.AdjustResize);

            WindowSoftInputMode.assistActivity(this);
        }

        string HOCKEYAPP_APPID = "c022cd37bc6945cb90f023a9afa6a347";

        protected override void OnResume()
        {
            base.OnResume();
            CrashManager.Register(this, HOCKEYAPP_APPID);            
        }
    }

    public class WindowSoftInputMode
    {
        public static void assistActivity(Activity activity)
        {
            new WindowSoftInputMode(activity);
        }

        private Android.Views.View _mChildOfContent;
        private int _usableHeightPrevious;
        private FrameLayout.LayoutParams _frameLayoutParams;

        private WindowSoftInputMode(Activity activity)
        {
            FrameLayout content = (FrameLayout)activity.FindViewById(Android.Resource.Id.Content);
            _mChildOfContent = content.GetChildAt(0);
            ViewTreeObserver vto = _mChildOfContent.ViewTreeObserver;
            vto.GlobalLayout += (object sender, EventArgs e) => {
                possiblyResizeChildOfContent();
            };
            _frameLayoutParams = (FrameLayout.LayoutParams)_mChildOfContent.LayoutParameters;
        }

        private void possiblyResizeChildOfContent()
        {
            int usableHeightNow = _computeUsableHeight();
            if (usableHeightNow != _usableHeightPrevious)
            {
                int usableHeightSansKeyboard = _mChildOfContent.RootView.Height;
                int heightDifference = usableHeightSansKeyboard - usableHeightNow;

                _frameLayoutParams.Height = usableHeightSansKeyboard - heightDifference;

                _mChildOfContent.RequestLayout();
                _usableHeightPrevious = usableHeightNow;
            }
        }

        private int _computeUsableHeight()
        {
            Rect r = new Rect();
            _mChildOfContent.GetWindowVisibleDisplayFrame(r);
            return (r.Bottom - r.Top);
        }
    }
}