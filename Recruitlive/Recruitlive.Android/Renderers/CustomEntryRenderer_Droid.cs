﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Recruitlive.Droid.Renderers;
using Android.Content.Res;
using Android.Text;

[assembly: ExportRenderer(typeof(Entry), typeof(CustomEntryRenderer_Droid))]
namespace Recruitlive.Droid.Renderers
{
    public class CustomEntryRenderer_Droid : EntryRenderer
    {
        public CustomEntryRenderer_Droid(Context context) : base(context)
        {

        }
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
           Control?.SetBackgroundColor(Android.Graphics.Color.Transparent);

            Control?.SetPadding(0, 10, 10, 10);

            //Control?.SetHintTextColor(ColorStateList.ValueOf(global::Android.Graphics.Color.White));

            if (Control != null)
            {
              //  this.Control.InputType = InputTypes.TextFlagNoSuggestions;
                // this.Control.Background = Resources.GetDrawable(Resource.Layout.CustomEntryDrawable);
            }


        }
    }
}