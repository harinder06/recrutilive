﻿using Android.Content;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Recruitlive.Droid.Renderers;
using Recruitlive.CustomConntrols;

[assembly: ExportRenderer(typeof(BorderlessEditor), typeof(BorderlessEditorRenderer))]

namespace Recruitlive.Droid.Renderers
{
    public class BorderlessEditorRenderer : EditorRenderer
    {

        public BorderlessEditorRenderer(Context context) : base(context)
        {

        }
        public static void Init() { }
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                // Control.Background = null;

                Control.Background = Android.App.Application.Context.GetDrawable(Resource.Drawable.RoundedEditText);

                var layoutParams = new MarginLayoutParams(Control.LayoutParameters);
                layoutParams.SetMargins(10, 5, 0, 0);
                LayoutParameters = layoutParams;
                Control.LayoutParameters = layoutParams;
                Control.SetPadding(10, 5, 0, 0);
                SetPadding(0, 0, 0, 0);
            }
        }
    }
}