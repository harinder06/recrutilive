﻿using Android.Graphics;
using Android.Graphics.Drawables;
using Recruitlive.Droid.Renderers;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.RangeSlider.Common;

[assembly: ExportEffect(typeof(RangeSliderEffect), nameof(RangeSliderEffect))]


namespace Recruitlive.Droid.Renderers
{
    public class RangeSliderEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            var themeColor = Xamarin.Forms.Color.Fuchsia.ToAndroid();
            var ctrl = (Xamarin.RangeSlider.RangeSliderControl)Control;
            ctrl.ActiveColor = themeColor;

            var thumbImage = new BitmapDrawable(ctrl.ThumbImage);
            thumbImage.SetColorFilter(new PorterDuffColorFilter(themeColor, PorterDuff.Mode.SrcIn));
            ctrl.ThumbImage = ConvertToBitmap(thumbImage, ctrl.ThumbImage.Width, ctrl.ThumbImage.Height);

            var thumbPressedImage = new BitmapDrawable(ctrl.ThumbPressedImage);
            thumbPressedImage.SetColorFilter(new PorterDuffColorFilter(themeColor, PorterDuff.Mode.SrcIn));
            ctrl.ThumbPressedImage = ConvertToBitmap(thumbPressedImage, ctrl.ThumbPressedImage.Width, ctrl.ThumbPressedImage.Height);
        }

        protected override void OnDetached()
        {
        }

        private static Bitmap ConvertToBitmap(Drawable drawable, int widthPixels, int heightPixels)
        {
            var mutableBitmap = Bitmap.CreateBitmap(widthPixels, heightPixels, Bitmap.Config.Argb8888);
            var canvas = new Canvas(mutableBitmap);
            drawable.SetBounds(0, 0, widthPixels, heightPixels);
            drawable.Draw(canvas);
            return mutableBitmap;
        }



    }
}