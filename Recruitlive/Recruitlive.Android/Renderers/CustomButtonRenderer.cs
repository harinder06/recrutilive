﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;

using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;

using Recruitlive.Droid.Renderers;
using Recruitlive.CustomConntrols;

[assembly: ExportRendererAttribute(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace Recruitlive.Droid.Renderers
{

    public class CustomButtonRenderer : ButtonRenderer
    {
        public CustomButtonRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs< Xamarin.Forms.Button>e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				Control.SetAllCaps(false);
			}

	    }

    }

}