﻿using System;
using Recruitlive.DependencyInterface;
using Recruitlive.iOS.DependencyInterface;
using Xamarin.Forms;
using System.IO;
using UIKit;
using Foundation;
using System.Threading.Tasks;
using System.Linq;

[assembly: Dependency(typeof(GeneratePdfiOS))]
namespace Recruitlive.iOS.DependencyInterface
{
    public class GeneratePdfiOS : IGeneratePDF
    {
        public GeneratePdfiOS()
        {
            
        }
        // Creating and saving the file based on the bytes that are fetched from the api.
        public string SaveFiles(string filename, byte[] bytes)
        {
            var directory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            if (!Directory.Exists(directory))
            {
                // creation of the directory
                Directory.CreateDirectory(directory);
            }

            // generating the file path based on the directory and file name.
            string filePath = System.IO.Path.Combine(directory, filename+".pdf");

            if (File.Exists(filePath))
            {
                // deleting the file if already exists
                File.Delete(filePath);
            }

            // writing the data to the file apth
            File.WriteAllBytes(filePath, bytes);

            OpenFile(filePath);
            return filePath;

        }

        /// <summary>
        /// this method will be used to open the application in the default application of the phone.
        /// </summary>
        /// <param name="filePath"></param>
        public void OpenFile(string filePath)
        {
            var bytes = File.ReadAllBytes(filePath);
            var PreviewController = UIDocumentInteractionController.FromUrl(NSUrl.FromFilename(filePath));
                     PreviewController.Delegate = new UIDocumentInteractionControllerDelegateClass(UIApplication.SharedApplication.KeyWindow.RootViewController);
            Device.BeginInvokeOnMainThread(() =>
            {
                PreviewController.PresentPreview(true);
            });

        }
    }

    public class UIDocumentInteractionControllerDelegateClass : UIDocumentInteractionControllerDelegate
    {
        UIViewController ownerVC;

        public UIDocumentInteractionControllerDelegateClass(UIViewController vc)
        {
            ownerVC = vc;
        }

        public override UIViewController ViewControllerForPreview(UIDocumentInteractionController controller)
        {
            return ownerVC;
        }

        public override UIView ViewForPreview(UIDocumentInteractionController controller)
        {
            return ownerVC.View;
        }
    }
}
