﻿using System;
using System.IO;
using Recruitlive.DependencyInterface;
using Recruitlive.iOS.DependencyInterface;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileHelper))]
namespace Recruitlive.iOS.DependencyInterface
{
    public class FileHelper : IFileHelper
    {
        public FileHelper ()
        {
        }
        public string GetLocalFilePath(string filename)
        {
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libFolder = Path.Combine(docFolder, "..", "Library", "Databases");

            if (!Directory.Exists(libFolder))
            {
                Directory.CreateDirectory(libFolder);
            }

            return Path.Combine(libFolder, filename);
        }
    }
}