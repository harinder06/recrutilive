﻿using Foundation;
using Recruitlive.DependencyInterface;
using Recruitlive.iOS.DependencyInterface;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(IPAddressManager))]
namespace Recruitlive.iOS.DependencyInterface
{
    public class IPAddressManager : IIPAddressManager
    {
        public IPAddressManager()
        {
            
        }
        public string GetIPAddress()
        {
            string ipAddress = "";

            foreach (var netInterface in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (netInterface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 ||
                    netInterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    foreach (var addrInfo in netInterface.GetIPProperties().UnicastAddresses)
                    {
                        if (addrInfo.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            ipAddress = addrInfo.Address.ToString();

                        }
                    }
                }
            }

            return ipAddress;
        }

        public string GetVersion()
        {
            string deviceOs = UIDevice.CurrentDevice.SystemVersion;
            return deviceOs;
        }
    }
}
