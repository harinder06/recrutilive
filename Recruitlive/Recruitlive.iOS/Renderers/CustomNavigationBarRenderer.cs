﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Recruitlive.iOS.Renderers;

[assembly: ExportRenderer(typeof(NavigationRenderer), typeof(CustomNavigationBarRenderer))]
namespace Recruitlive.iOS.Renderers
{
 
    public class CustomNavigationBarRenderer :NavigationRenderer
    {

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                var att = new UITextAttributes();
                att.Font = UIFont.FromName("Roboto-Medium", 18);
                UINavigationBar.Appearance.SetTitleTextAttributes(att);
            }
        }


    }


}