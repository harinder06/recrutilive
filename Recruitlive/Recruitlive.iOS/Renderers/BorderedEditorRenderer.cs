﻿using System;
using Recruitlive.CustomConntrols;
using Recruitlive.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportRenderer(typeof(BorderlessEditor), typeof(BorderedEditorRenderer))]
namespace Recruitlive.iOS.Renderers
{
    public class BorderedEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.Layer.CornerRadius = 3;
                Control.Layer.BorderColor = Color.FromHex("#808080").ToCGColor();

             //   Control.Layer.BorderColor = Color.Gray;
                Control.Layer.BorderWidth = 2;
            }
        }
    }
}