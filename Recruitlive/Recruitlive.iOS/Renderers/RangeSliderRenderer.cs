﻿using System;
using System.Runtime.Remoting.Contexts;
using Recruitlive.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Xamarin.RangeSlider;
using Xamarin.RangeSlider.Forms;

[assembly: ExportRenderer(typeof(RangeSlider), typeof(RangeSliderRenderers))]
namespace Recruitlive.iOS.Renderers
{
    public class RangeSliderRenderers :RangeSliderRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<RangeSlider> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
               // string colorSlider = "#008000";
                var ctrl = (Xamarin.RangeSlider.RangeSliderControl)Control;
                ctrl.TintColor = Color.Fuchsia.ToUIColor();

                
				//ctrl.

            }
        }

     
    }
}