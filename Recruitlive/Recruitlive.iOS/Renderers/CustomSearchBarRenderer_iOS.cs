﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms.Platform.iOS;

using System.ComponentModel;
using Xamarin.Forms;
using UIKit;
using Foundation;
using Recruitlive.iOS.Renderers;
using Recruitlive.CustomConntrols;
using Recruitlive.iOS;

[assembly: ExportRenderer(typeof(CustomSearchBar), typeof(CustomSearchBarRenderer_iOS))]
namespace Recruitlive.iOS.Renderers
{
    public class CustomSearchBarRenderer_iOS : SearchBarRenderer
    {


        protected override void OnElementPropertyChanged(object sender,
         PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (Control != null)
            {
                
                var uiTextField = (UITextField)Control.ValueForKey(new NSString("_searchField"));

               
                // uiTextField.Font = YourFontHere;
                uiTextField.TextColor = UIColor.Black;
                uiTextField.BackgroundColor = UIColor.White;

                // Placeholder colour is white 
                uiTextField.AttributedPlaceholder = new NSAttributedString("Search", null, UIColor.Gray);
                Control.ShowsCancelButton = false;
               

               


                //var csb = (POC.CustomControls.CustomSearchBar)Element;
                // if (csb.BarTint != null)
                //   Control.BarTintColor = UIColor.Clear;// csb.BarTint.GetValueOrDefault().ToUIColor();
                //Control.BarStyle = (UIBarStyle)Enum.Parse(typeof(UIBarStyle), csb.BarStyle);
           

                //Control.SearchBarStyle = (UISearchBarStyle)Enum.Parse(typeof(UISearchBarStyle), csb.BarStyle);
           
            
            
            }
        }
    }
}
