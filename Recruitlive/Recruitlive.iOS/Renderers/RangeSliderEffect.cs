﻿using System;
using Recruitlive.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ResolutionGroupName("MyCompany")]
[assembly: ExportEffect(typeof(RangeSliderEffect), nameof(RangeSliderEffect))]


namespace Recruitlive.iOS.Renderers
{
    
    public class RangeSliderEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            var ctrl = (Xamarin.RangeSlider.RangeSliderControl)Control;
            if(ctrl!=null)
            {
                ctrl.TintColor = Color.White.ToUIColor();
            }

        }

        protected override void OnDetached()
        {
        }
    }
}
