﻿using Foundation;
using HockeyApp.iOS;
using Syncfusion.SfRangeSlider.XForms.iOS;
using UIKit;
using Xamarin.RangeSlider.Forms;

namespace Recruitlive.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
        const string HOCKEYAPP_APPID = "12932565229a43c3ac558256280f273e";
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
            //Get the shared instance
            var manager = BITHockeyManager.SharedHockeyManager;

            //Configure it to use our APP_ID
            manager.Configure(HOCKEYAPP_APPID);

            //Start the manager
            manager.StartManager();


            
            //new SfRangeSliderRenderer();
          
            global::Xamarin.Forms.Forms.Init();

            App.ScreenWidth = UIScreen.MainScreen.Bounds.Width;
            App.ScreenHeight = UIScreen.MainScreen.Bounds.Height;


            //SfPickerRenderer.Init();

            Xamarin.FormsMaps.Init();

			LoadApplication(new App());

            new SfRangeSliderRenderer();

			return base.FinishedLaunching(app, options);
		}
	}
}
