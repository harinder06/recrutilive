﻿using System.Collections.Generic;
using Xamarin.Forms.Maps;

namespace Recruitlive.Controls
{
    public class RecruitliveMap : Map
    {
        public List<CustomPin> CustomPins { get; set; }

    }
    public class CustomPin : Pin
    {
        public string Url { get; set; }

    }
}