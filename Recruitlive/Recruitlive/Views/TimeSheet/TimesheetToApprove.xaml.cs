﻿using Recruitlive.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TimesheetToApprove : ContentPage
    {
        public TimesheetToApprove()
        {
            InitializeComponent();

            this.Title = "Timesheets To Approve";


            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () => {
                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

            // integrated with view model

            BindingContext = new TimesheetApproveViewModel();

            // device platform specific code 

            if (Device.RuntimePlatform == Device.iOS)
            {

                NavigationPage.SetBackButtonTitle(this, "");
                timesheetList.Margin = new Thickness(0, 10, 0, 10);

            }
            else
            {
            }
        }

        void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;
        }       

        

    }
}