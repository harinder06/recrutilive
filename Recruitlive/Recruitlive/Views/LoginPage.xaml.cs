﻿using Recruitlive.Models;
using Recruitlive.Repo;
using Rg.Plugins.Popup.Extensions;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        int restrictCount = 3;
        double screenHeight, screenWidth;
        public LoginPage()
        {
            InitializeComponent();


            try
            {
                screenHeight = App.ScreenHeight;

                screenWidth = App.ScreenWidth;

                //this.BackgroundImage = "login_bg.png";

                password.IsPassword = true;

                NavigationPage.SetHasNavigationBar(this, false);


                if (Device.RuntimePlatform == Device.Android)
                {

                    upperLayout.Height = screenHeight * .1;

                    bottomLayout.Height = screenHeight * 0.8;
                    // below code is for setting home page screen for tablet

                    if (screenWidth >= 1000)
                    {
                        firstLayout.Height = 120;

                        secondLayout.Height = 120;

                        thirdLayout.Height = 120;

                        fourthLayout.Height = 120;

                    }
                    else
                    {


                    }




                }
                else
                {

                    NavigationPage.SetBackButtonTitle(this, "");

                    if (Width >= 740)
                    {
                        parentLayout.Padding = 30;
                    }

                    upperLayout.Height = screenHeight * 0.2;

                    bottomLayout.Height = screenHeight * 0.8;

                    firstLayout.Height = 90;

                    secondLayout.Height = 90;

                    thirdLayout.Height = 90;

                    fourthLayout.Height = 90;
                }
            }
            catch (Exception)
            {

            }

        }


        /// <summary>
        /// Method that will restrict the no of char in the branch name field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnBranchTextChanged(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            String val = entry.Text; //Get Current Text

            if (val.Length > restrictCount)//If it is more than your character restriction
            {
                val = val.Remove(val.Length - 1);// Remove Last character 
                entry.Text = val; //Set the Old value
            }
        }

        public string CheckLoginValidations()
        {
            string msg = string.Empty;
            if (string.IsNullOrEmpty(userName.Text))
            {
                msg += "Please Enter User Name" + Environment.NewLine;
            }
            if (string.IsNullOrEmpty(password.Text))
            {
                msg += "Please Enter Password" + Environment.NewLine;
            }
            if (string.IsNullOrEmpty(branchName.Text))
            {
                msg += "Please Enter Branch" + Environment.NewLine;
            }
            if (string.IsNullOrEmpty(companyName.Text))
            {
                msg += "Please Enter Company Name" + Environment.NewLine;
            }
            return msg;

        }

        async void Login_Clicked(object sender, EventArgs e)
        {
            var returnMessage = CheckLoginValidations();
            if (!string.IsNullOrEmpty(returnMessage))
            {
                await App.Current.MainPage.DisplayAlert("Recruitlive", returnMessage, "OK");
                return;
            }
            Login();
        }

        async void Login()
        {
            try
            {
                string postData = "UserName=" + userName.Text + "&Password=" + password.Text + "&CompanyName=" + companyName.Text + "&Branch=" + branchName.Text + "&grant_type=password";

                HttpClientBase _base = new HttpClientBase();
                if (_base.CheckConnection())
                {
                    await Application.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                    var result = await _base.Login(CommonLib.MainUrl + ApiUrl.LoginUrl, postData);
                    if (!string.IsNullOrEmpty(result.access_token))
                    {
                        SaveLoginResponse _obj = new SaveLoginResponse();
                        _obj.access_token = result.access_token;
                        _obj.expires_in = result.expires_in;
                        _obj.token_type = result.token_type;
                        _obj.user_name = userName.Text;
                        _obj.branch = branchName.Text;
                        _obj.company_name = companyName.Text;
                        App.Database.SaveLoggedUser(_obj);
                        LoaderPopup.CloseAllPopup();
                        await Application.Current.MainPage.Navigation.PushAsync(new HomePage());

                    }
                    else
                    {
                        LoaderPopup.CloseAllPopup();
                        await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please fill correct information", "OK");
                    }
                }
            }
            catch (Exception ex)
            {
                LoaderPopup.CloseAllPopup();
                await Application.Current.MainPage.DisplayAlert("", ex.Message, "OK");

            }
            finally
            {

            }

        }
    }
}