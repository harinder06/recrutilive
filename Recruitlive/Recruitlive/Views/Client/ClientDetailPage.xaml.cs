﻿using Recruitlive.Models.Contact;
using Recruitlive.ViewModels;
using Recruitlive.Views.Contact;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ClientDetailPage : ContentPage
    {
        public int clientId;
        public ClientDetailPage(int clientID)
        {
            double height = App.ScreenHeight;
            double width = App.ScreenWidth;
            clientId = clientID;

            InitializeComponent();

            this.Title = "Client Detail";

            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");
                if (width >= 740)
                {

                    firstButton.WidthRequest = width / 2 - 50;

                    secondButton.WidthRequest = width / 2 - 50;

                }
                else
                {

                    firstButton.WidthRequest = width / 2 - 20;

                    secondButton.WidthRequest = width / 2 - 20;

                }


            }

            else
            {
                if (width >= 1000)
                {

                    firstButton.WidthRequest = width / 4 - 20;

                    secondButton.WidthRequest = width / 4 - 20;

                }
                else
                {

                    firstButton.WidthRequest = width / 2 - 20;

                    secondButton.WidthRequest = width / 2 - 20;

                }


            }
            // home toolbar code
            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () => {

                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));


            BindingContext = new ClientDetailsViewModel(clientId, Navigation);
        }


        void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;
        }
      
        void Contacts_Clicked(object sender,EventArgs e)
        {
            ContactSearchPost json = new ContactSearchPost();
            json.ClientValue = clientId.ToString();
            Email _email = new Email();
            _email.Details = string.Empty;
            json.Email = _email;
            Application.Current.MainPage.Navigation.PushAsync(new ClientContactsDetails(json));
        }

    }
}