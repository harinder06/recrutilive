﻿using Recruitlive.Models;
using Recruitlive.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ClientListPage : ContentPage
    {
        double screenWidth = App.ScreenWidth;
        public static string SearchBartxt;
        public static ClientSearchPost _postData;
        public static bool checkVal;
        public static bool IsHighlighted = false;
        public ClientListPage(ClientSearchPost _data, bool check)
        {
            InitializeComponent();

            _postData = _data;
            checkVal = check;

            this.Title = "Client";

            //tool bar code
            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () => {
                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

            // device platform specific code 

            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");

                if (screenWidth >= 740)
                {
                    searchBar.WidthRequest = screenWidth - 150;
                }
                else
                {
                    searchBar.WidthRequest = 300;
                }


                searchBarFrame.HeightRequest = 70;

                searchBarFrame.Padding = new Thickness(0, 0, 0, 0);

                searchBar.Margin = new Thickness(0, 0, 0, 0);

                advanceFilterIcon.Margin = new Thickness(0, 0, 10, 0);

                advanceFilterIcon.HeightRequest = 30;

                advanceFilterIcon.WidthRequest = 30;

                stacklayoutBarFrame.HeightRequest = 70;

                stacklayoutBarFrame.Padding = new Thickness(0, 0, 0, 0);

              // searchBar.WidthRequest = 300;
             
                clientList.Margin = new Thickness(0, 10, 0, 10);


            }
            else
            {

            }

            if (_postData == null)
            {
                BindingContext = new ClientListViewModel(null, checkVal);
            }
            else
            {
                BindingContext = new ClientListViewModel(_postData, checkVal);
            }

        }

        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();

        }

        /// <summary>
        /// method runs when the page appeared on the screen
        /// </summary>
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (IsHighlighted == true)
            {
                var bindingContext = (ClientListViewModel)BindingContext;
                bindingContext.CheckIsHighLighted(IsHighlighted);
                IsHighlighted = false;
            }

            if (!String.IsNullOrEmpty(SearchBartxt))
            {
                searchBar.Text = SearchBartxt;
            }
            else
            {
                searchBar.Text = string.Empty;
            }
        }

        //below tapped push to advanced search page
        async void advancedClientFltr_Tapped(object sender, EventArgs e)
        {
            
            await Application.Current.MainPage.Navigation.PushAsync(new ClientAdvancedSearchList(searchBar.Text));
        }

        void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            //DisplayAlert("Item Selected", e.SelectedItem.ToString(), "Ok");
            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selecteditem = e.Item as CandidateListModel;
            var splitClientId = selecteditem.emmployeeID.Split(' ');
            var clientId = Convert.ToInt32(splitClientId[1]);
            Navigation.PushAsync(new ClientDetailPage(clientId));
        }

        /// <summary>
        /// Method that will be used when the search button is pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Search_Button_Pressed(object sender, EventArgs e)
        {
            var searchBar = (SearchBar)sender;
            var getClientFilters = App.Database.GetClientFilters();
            if (getClientFilters != null)
            {
                if (!string.IsNullOrEmpty(searchBar.Text))
                {
                    getClientFilters.CompanyName = searchBar.Text;
                }
                else
                {
                    getClientFilters.CompanyName = string.Empty;
                }
                if (_postData != null)
                {
                    if (_postData.ClientName != searchBar.Text)
                    {
                        getClientFilters.AutoStatus = string.Empty;
                        getClientFilters.AutoType = string.Empty;
                        getClientFilters.City = string.Empty;
                        getClientFilters.FirstName = string.Empty;
                        getClientFilters.LastName = string.Empty;
                        getClientFilters.PostCode = string.Empty;
                        getClientFilters.StatusPicker = string.Empty;
                        getClientFilters.Street = string.Empty;
                        getClientFilters.TypePicker = string.Empty;
                    }
                }

                App.Database.SaveClientFilters(getClientFilters);
            }
            _postData = new ClientSearchPost();
            _postData.ClientName = searchBar.Text;          
            // setting the binding context
            BindingContext = new ClientListViewModel(_postData, false);
            searchedText.IsVisible = true;
            SearchBartxt = searchBar.Text;          
        }


    }
}