﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using Recruitlive.Models;
using Recruitlive.Repo;
using System.Collections.Generic;
using System.Net.Http;
using Rg.Plugins.Popup.Extensions;
using System.Linq;
using Recruitlive.Models.Client;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ClientAdvancedSearchList : ContentPage
    {
        HttpClientBase _base = new HttpClientBase();
        string selectedStatusVal = string.Empty;
        string selectedTypeVal = string.Empty;
        public Dictionary<List<CandidateTypes>, HttpResponseMessage> result;
        public Dictionary<List<CandidateTypes>, HttpResponseMessage> resultStatus;
        public static List<CandidateTypes> _listTypes = new List<CandidateTypes>();
        public static List<CandidateTypes> _listStatus = new List<CandidateTypes>();
        public static ObservableCollection<string> _loadTypeData = new ObservableCollection<string>();
        public static ObservableCollection<string> _loadStatusData = new ObservableCollection<string>();
        public ClientFilters getClientFilters = App.Database.GetClientFilters();
        public static string advanceFirstNameTxt;
        public static ClientSearchPost _postData = new ClientSearchPost();
        static string getStatus = string.Empty;
        static string getType = string.Empty;
        public bool IsApplied = false;

        public ClientAdvancedSearchList(string searchedText)
        {
            InitializeComponent();
            IsApplied = false;
            getStatus = string.Empty;
            getType = string.Empty;

            try
            {
                #region setting search bar text
                //auto fill first name text

                if (!string.IsNullOrEmpty(searchedText))
                {
                    companyName.Text = searchedText;

                    advanceFirstNameTxt = searchedText;
                }

                #endregion
            }
            catch (Exception)
            {

            }

            //toolbar code

            ToolbarItems.Add(new ToolbarItem("Apply", "applyicon.png", async () =>
            {
                var returnMessage = CheckApplyValidation();
                if (!string.IsNullOrEmpty(returnMessage))
                {
                    await App.Current.MainPage.DisplayAlert("Recruitlive", returnMessage, "OK");
                    return;
                }
                ApplyMethod();
            }));

            this.Title = "Client";

            // calling the type and status data method to set data in the dropdowns
            GetTypesStatus();

            #region Setting Client Filters

            if (getClientFilters != null)
            {
                //setting all the data in all the text boxes that is get from the local database of the client filters.
                if (!string.IsNullOrEmpty(getClientFilters.FirstName))
                    firstName.Text = getClientFilters.FirstName; // First Name TextBox
                if (!string.IsNullOrEmpty(getClientFilters.LastName))
                    lastName.Text = getClientFilters.LastName; // Last Name TextBox
                if (!string.IsNullOrEmpty(getClientFilters.Street))
                    street.Text = getClientFilters.Street; // Street TextBox
                if (!string.IsNullOrEmpty(getClientFilters.PostCode))
                    postCode.Text = getClientFilters.PostCode; //PostCode TextBox
                if (!string.IsNullOrEmpty(getClientFilters.AutoStatus))
                {
                    labelPicker.Text = getClientFilters.AutoStatus; //AutoStatus TextBox
                    getStatus = getClientFilters.AutoStatus;
                }
                if (!string.IsNullOrEmpty(getClientFilters.AutoType))
                {
                    labelPickerType.Text = getClientFilters.AutoType; //AutoType TextBox
                    getType = getClientFilters.AutoType;
                }
                if (!string.IsNullOrEmpty(getClientFilters.CompanyName) && string.IsNullOrEmpty(advanceFirstNameTxt))
                    companyName.Text = getClientFilters.CompanyName; // Company Name Text Box
                if (!string.IsNullOrEmpty(getClientFilters.City))
                    city.Text = getClientFilters.City; // City Text Box               

            }
            #endregion

            //specific platform code
            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");
            }
            else
            {

            }

            pickerStatus.SelectedIndexChanged += Picker_OkButtonClicked;
            //pickerStatus.CancelButtonClicked += Picker_CancelButtonClicked;

            pickerType.SelectedIndexChanged += Picker_OkButtonClicked1;
            //pickerType.CancelButtonClicked += Picker_CancelButtonClicked1;

        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            // passing the data to the search candidate page to set in the search bar
            if (!String.IsNullOrEmpty(companyName.Text))
            {
                ClientListPage.SearchBartxt = companyName.Text;
            }
            else
            {
                ClientListPage.SearchBartxt = string.Empty;
            }

            ClientFilters filterData = new ClientFilters();
            var getLoginDetails = App.Database.GetLoginUser();
            if (!IsApplied)
            {
                // gettimg candidate filters stored in local storage
                filterData = App.Database.GetClientFilters();
                if (filterData == null)
                {
                    // svaing the candidate filters for the first time
                    filterData = new ClientFilters();
                    filterData.CompanyName = companyName.Text;
                    filterData.Section = "Client";
                    filterData.UserName = getLoginDetails.user_name;
                    filterData.BranchName = getLoginDetails.branch;

                    App.Database.SaveClientFilters(filterData);
                }
                else
                {
                    filterData.CompanyName = companyName.Text;
                    filterData.LastName = filterData.LastName == lastName.Text ? filterData.LastName : "";
                    filterData.PostCode = filterData.PostCode == postCode.Text ? filterData.PostCode : "";
                    filterData.Street = filterData.Street == street.Text ? filterData.Street : "";
                    filterData.City = filterData.City == city.Text ? filterData.City : "";
                    filterData.FirstName = filterData.FirstName == firstName.Text ? filterData.FirstName : "";
                    filterData.AutoStatus = filterData.AutoStatus == getStatus ? filterData.AutoStatus : "";
                    filterData.AutoType = filterData.AutoType == getType ? filterData.AutoType : "";
                    filterData.Section = "Client";
                    filterData.UserName = getLoginDetails.user_name;
                    filterData.BranchName = getLoginDetails.branch;


                    App.Database.SaveClientFilters(filterData);
                }

                ClientListPage.IsHighlighted = true;
            }
            else
            {
                // saving the candidate filters if apply button is clicked
                SaveClientFilters(getLoginDetails);
            }

        }

        /// <summary>
        /// Method that will to get all the data of the type and status from the api.
        /// </summary>
        public async void GetTypesStatus()
        {
            if (_loadTypeData.Count == 0 && _loadStatusData.Count == 0)
            {
                try
                {
                    // Checking the internet connection whether user is connected to the internet or not.
                    if (_base.CheckConnection())
                    {
                        // displaying the loading circle to the user.
                        await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                        result = await _base.GetTypes(CommonLib.MainUrl + ApiUrl.GetClientTypes);

                        resultStatus = await _base.GetTypes(CommonLib.MainUrl + ApiUrl.GetClientStatus);

                        foreach (var item in result)
                        {
                            if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                if (item.Key != null) // if api returns data successfully.
                                {
                                    //_loadTypeData = new ObservableCollection<string>();
                                    //_listTypes = new List<CandidateTypes>();
                                    // Setting all the data on their respective fields.
                                    for (int i = 0; i < item.Key.Count; i++)
                                    {
                                        _loadTypeData.Add(item.Key[i].text);
                                        _listTypes.Add(item.Key[i]);
                                    }                                   
                                    LoaderPopup.CloseAllPopup(); // closing the loading popup.

                                }

                                else //if api is not returning the data.
                                {
                                    CommonLib.ApiNotReturningData();
                                }
                            }
                            else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                            {
                                CommonLib.ApiResponseSlow();
                            }
                            else
                            {
                                CommonLib.Unauthorize();
                            }
                        }

                        foreach (var item1 in resultStatus)
                        {
                            if (item1.Value.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                if (item1.Key != null) // if api returns data successfully.
                                {
                                    //_loadStatusData = new ObservableCollection<string>();
                                    //_listStatus = new List<CandidateTypes>();
                                    // Setting all the data on their respective fields.
                                    for (int i = 0; i < item1.Key.Count; i++)
                                    {
                                        _loadStatusData.Add(item1.Key[i].text);
                                        _listStatus.Add(item1.Key[i]);
                                    }                                  
                                    LoaderPopup.CloseAllPopup(); // closing the loading popup.

                                }

                                else //if api is not returning the data.
                                {
                                    CommonLib.ApiNotReturningData();
                                }
                            }
                        }

                    }
                }

                catch (Exception)
                {
                    LoaderPopup.CloseAllPopup(); // closing the loading popup.
                }
                finally
                {

                }
            }
            pickerType.ItemsSource = _loadTypeData;
            //pickerType.SelectedItem = _loadTypeData[0];
            //pickerType.SelectedIndex = 0;
            if (getClientFilters != null)
            {
                if (!string.IsNullOrEmpty(getClientFilters.AutoType))
                {
                    pickerType.SelectedItem = getClientFilters.AutoType;
                    pickerType.SelectedIndex = Convert.ToInt32(getClientFilters.TypePicker);
                }
            }
            pickerStatus.ItemsSource = _loadStatusData;
            //pickerStatus.SelectedItem = _loadStatusData[0];
            //pickerStatus.SelectedIndex = 0;
            if (getClientFilters != null)
            {
                if (!string.IsNullOrEmpty(getClientFilters.AutoStatus))
                {
                    pickerStatus.SelectedItem = getClientFilters.AutoStatus;
                    pickerStatus.SelectedIndex = Convert.ToInt32(getClientFilters.StatusPicker);
                }
            }
        }

        #region Status Picker
        /// <summary>
        /// Status Picker Cancel Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void Picker_CancelButtonClicked(object sender, SelectionChangedEventArgs e)
        //{
        //    labelPicker.Text = "Select a Status";
        //    getStatus = string.Empty;

        //}

        /// <summary>
        /// Status Picker OK Button Pressed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Picker_OkButtonClicked(object sender, EventArgs e)
        {
            labelPicker.Text = Convert.ToString(pickerStatus.SelectedItem);
            getStatus = pickerStatus.SelectedItem.ToString();

        }

        // Opening of the status dropdown
        private void statusLabel_Tapped(object sender, EventArgs e)
        {
            pickerStatus.Focus();
            //pickerStatus.IsOpen = !pickerStatus.IsOpen;
        }

        #endregion

        #region Type Picker
        /// <summary>
        /// type picker cancel button pressed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void Picker_CancelButtonClicked1(object sender, EventArgs e)
        //{
        //    labelPickerType.Text = "Select a Type";
        //    getType = string.Empty;
        //}

        /// <summary>
        /// type picker Ok button pressed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Picker_OkButtonClicked1(object sender, EventArgs e)
        {
            labelPickerType.Text = Convert.ToString(pickerType.SelectedItem);
            getType = pickerType.SelectedItem.ToString();
        }

        private void typeLabel_Tapped(object sender, EventArgs e)
        {
            pickerType.Focus();
            //pickerType.IsOpen = !pickerType.IsOpen;
        }
        #endregion          

        //below moreTapped invoke visiblity of other layout

        void more_Tapped(object sender, EventArgs e)
        {
            if (companyLayout.IsVisible == true)
            {
                moreAndLessLabel.Text = "More";
                moreAndLessImage.Source = "more.png";
                companyLayout.IsVisible = false;
            }
            else
            {
                moreAndLessLabel.Text = "Less";
                moreAndLessImage.Source = "less.png";
                companyLayout.IsVisible = true;
            }
        }

        /// <summary>
        /// Method for checking the apply button validation if all the values are null then it give the apply button validation message.
        /// </summary>
        /// <returns></returns>
        public string CheckApplyValidation()
        {
            string msg = string.Empty;
            if (string.IsNullOrEmpty(firstName.Text) && string.IsNullOrEmpty(lastName.Text) && string.IsNullOrEmpty(getStatus) && string.IsNullOrEmpty(getType)
                && string.IsNullOrEmpty(street.Text) && string.IsNullOrEmpty(postCode.Text) && string.IsNullOrEmpty(city.Text) && string.IsNullOrEmpty(companyName.Text))
            {
                msg = "Please select filters first before applying";
            }
            return msg;
        }



        /// <summary>
        /// method that will use to save all the filters into the local database and to set all the 
        /// filters in the object to pass this object on the search candidate page.
        /// </summary>
        public void ApplyMethod()
        {
            IsApplied = true;
            _postData.ContactFirstName = string.IsNullOrEmpty(firstName.Text) ? string.Empty : firstName.Text;
            _postData.ContactLastName = string.IsNullOrEmpty(lastName.Text) ? string.Empty : lastName.Text;

            // status data
            if (labelPicker.Text != "Select a Status")
            {
                selectedStatusVal = pickerStatus.SelectedItem != null ? pickerStatus.SelectedItem.ToString() : string.Empty;
                getStatus = !string.IsNullOrEmpty(selectedStatusVal) ? _listStatus[Convert.ToInt32(pickerStatus.SelectedIndex)].value : string.Empty;
            }

            // type data
            if (labelPickerType.Text != "Select a Type")
            {
                selectedTypeVal = pickerType.SelectedItem != null ? pickerType.SelectedItem.ToString() : string.Empty;
                getType = !string.IsNullOrEmpty(selectedTypeVal) ? _listTypes[Convert.ToInt32(pickerType.SelectedIndex)].value : string.Empty;
            }

            _postData.Status = !string.IsNullOrEmpty(getStatus) ? getStatus : string.Empty;
            _postData.InternalExternal = !string.IsNullOrEmpty(getType) ? getType : string.Empty;
            _postData.Street = string.IsNullOrEmpty(street.Text) ? string.Empty : street.Text;
            _postData.PostCode = string.IsNullOrEmpty(postCode.Text) ? string.Empty : postCode.Text;
            _postData.AutoSuburb = string.IsNullOrEmpty(city.Text) ? string.Empty : city.Text;
            _postData.ClientName = string.IsNullOrEmpty(companyName.Text) ? string.Empty : companyName.Text;


            var getNavigationStack = Navigation.NavigationStack.ToList();
            var removeCurrentPage = getNavigationStack[getNavigationStack.Count - 1];
            var removeCandidateListPage = getNavigationStack[getNavigationStack.Count - 2];
            // Clearing the navigation stack
            if (Device.RuntimePlatform == Device.Android)
            {
                Navigation.RemovePage(removeCurrentPage);
                Navigation.RemovePage(removeCandidateListPage);
                Application.Current.MainPage.Navigation.PushAsync(new ClientListPage(_postData, true));
            }
            else
            {
                Navigation.InsertPageBefore(new ClientListPage(_postData, true), Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
                Navigation.RemovePage(this);
                Navigation.RemovePage(removeCandidateListPage);
            }




        }


        public bool SaveClientFilters(SaveLoginResponse getLoginDetails)
        {
            bool result = false;
            try
            {
                #region Saving Details In Local Database

                // Saving the filters into the local database.

                ClientFilters _obj = new ClientFilters();
                _obj.FirstName = firstName.Text;
                _obj.LastName = lastName.Text;
                if (labelPicker.Text != "Select a Status")
                {
                    selectedStatusVal = pickerStatus.SelectedItem != null ? pickerStatus.SelectedItem.ToString() : string.Empty;
                }

                if (labelPickerType.Text != "Select a Type")
                {
                    // type data
                    selectedTypeVal = pickerType.SelectedItem != null ? pickerType.SelectedItem.ToString() : string.Empty;
                }
                _obj.AutoStatus = selectedStatusVal;
                _obj.AutoType = selectedTypeVal;
                if (pickerType.SelectedIndex != null)
                    _obj.TypePicker = pickerType.SelectedIndex.ToString();
                if (pickerStatus.SelectedIndex != null)
                    _obj.StatusPicker = pickerStatus.SelectedIndex.ToString();
                _obj.PostCode = postCode.Text;
                _obj.Street = street.Text;
                _obj.CompanyName = companyName.Text;
                _obj.City = city.Text;
                _obj.Street = street.Text;
                _obj.Section = "Client";
                _obj.UserName = getLoginDetails.user_name;
                _obj.BranchName = getLoginDetails.branch;

                IsApplied = true;
                // saving the deatils into the database.
                App.Database.SaveClientFilters(_obj);

                #endregion
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }
    }
}