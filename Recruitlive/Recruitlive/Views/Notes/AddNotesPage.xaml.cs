﻿using Recruitlive.Models;
using Recruitlive.Repo;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddNotesPage : ContentPage
    {
        HttpClientBase _base = new HttpClientBase();
        public int candidateID;
        public string ModuleName;
        Label label = new Label();
        StackLayout _layout = new StackLayout();
        Frame _frameLayout = new Frame();
        public Dictionary<List<CandidateTypes>, HttpResponseMessage> result;
        public static string value = String.Empty;
        public AddNotesPage(int candidateid,string _moduleName)
        {
            InitializeComponent();

            candidateID = candidateid;

            ModuleName = _moduleName;
            this.Title = "Add Notes";

            InitializeControls();

            // Calling the category method to get categories in the dropdown from the api
            GetCategoryInDropDown();


            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", () =>
            {
                Navigation.PushAsync(new HomePage());
            }));

            //platform specific code
            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");
            }
            else
            {

            }
            _frameLayout.OutlineColor = Color.Gray;

            _frameLayout.BackgroundColor = Color.Gray;
            _frameLayout.HasShadow = false;
            _frameLayout.Padding = new Thickness(1, 0, 1, 0);
        }

        /// <summary>
        /// Selection of the category from the dropdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _tapItem_Tapped(object sender, EventArgs e)
        {
            var getLayout = sender as StackLayout;
            var getLabel = getLayout.ClassId.Split(';');
            categoryLbl.Text = getLabel[0];
            value = getLabel[1];
            dropDownLayout.IsVisible = false;
        }

        //set dropdown layout visiblity through dropdown image tapped
        void dropDownLayout_Tapped(object sender, EventArgs e)
        {
            if (dropDownLayout.IsVisible == true)
            {
                dropDownLayout.IsVisible = false;
               categoryFrameLayout.Padding = new Thickness(1, 1, 1, 1);
            }
            else
            {
                dropDownLayout.IsVisible = true;
                categoryFrameLayout.Padding = new Thickness(1, 1, 1, 1);
            }
        }

        //Below code is for text editor

        void InitializeControls()
        {
            txtDesc.Text = string.Empty;
            txtDesc.Text = "Add Comment..";
            txtDesc.FontSize = 14;
            txtDesc.TextColor = Color.Gray;
        }



        private void NotesEditor_Focused(object sender, FocusEventArgs e) //triggered when the user taps on the Editor to interact with it
        {
            if (txtDesc.Text.Equals("Add Comment..")) //if you have the placeholder showing, erase it and set the text color to black
            {
                txtDesc.FontSize = 14;
                // txtDesc.FontAttributes = FontAttributes.Bold;
                txtDesc.TextColor = Color.Gray;
                txtDesc.Text = "";
                txtDesc.TextColor = Color.Black;

            }
        }


        private void NotesEditor_Unfocused(object sender, FocusEventArgs e) //triggered when the user taps "Done" or outside of the Editor to finish the editing
        {
            if (txtDesc.Text.Equals("")) //if there is text there, leave it, if the user erased everything, put the placeholder Text back and set the TextColor to gray
            {

                txtDesc.FontSize = 14;
                //txtDesc.FontAttributes = FontAttributes.Bold;
                txtDesc.TextColor = Color.Black;
                txtDesc.Text = "Add Comment..";
                txtDesc.TextColor = Color.Gray;
            }
        }

        /// <summary>
        /// Method will run when the add notes button pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void Add_Notes_Clicked(object sender, EventArgs e)
        {
            string msg = string.Empty;
            if (categoryLbl.Text == "Category")
            {
                msg += "Please Choose Category" + Environment.NewLine;
            }
            if (txtDesc.Text == "Add Comment..")
            {
                msg += "Please Enter Comment" + Environment.NewLine;
            }
            if (!string.IsNullOrEmpty(msg))
            {
                await App.Current.MainPage.DisplayAlert("Recruitlive", msg, "OK");
                return;
            }
            AddNotes();
        }

        /// <summary>
        /// This Method is used for getting the values of categories in dropdown
        /// </summary>
        public async void GetCategoryInDropDown()
        {
            try
            {
                // Checking the internet connection whether user is connected to the internet or not.
                if (_base.CheckConnection())
                {
                    // displaying the loading circle to the user.
                    await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                    result = await _base.GetTypes(CommonLib.MainUrl + ApiUrl.CandidateNotesCategory);

                    foreach (var item in result)
                    {
                        if (item.Value.StatusCode == HttpStatusCode.OK)
                        {
                            if (item.Key != null) // if api returns data successfully.
                            {
                                for (int i = 0; i < item.Key.Count; i++)
                                {
                                    Label _textCat = new Label();
                                    _textCat.FontSize = 10;
                                    _textCat.FontFamily = Repository.App_FontFamilies.PageTitles;
                                    _textCat.StyleId = Repository.App_FontFamilies.PageTitles;
                                    _textCat.Margin = new Thickness(8, 0, 0, 0);
                                    _textCat.Text = item.Key[i].text;
                                    _textCat.HorizontalOptions = LayoutOptions.StartAndExpand;
                                    _textCat.HorizontalTextAlignment = TextAlignment.Start;
                                    _textCat.TextColor = Color.Gray;


                                    BoxView bottomLine = new BoxView();
                                    bottomLine.HorizontalOptions = LayoutOptions.FillAndExpand;
                                    bottomLine.VerticalOptions = LayoutOptions.End;
                                    bottomLine.HeightRequest = 1;
                                    bottomLine.BackgroundColor = Color.Gray;

                                    StackLayout _newLayout = new StackLayout();
                                    _newLayout.ClassId = item.Key[i].text + ";" + item.Key[i].value;
                                    _newLayout.Children.Add(_textCat);

                                    //this for first box layout top padding
                                    if (i == 0)
                                    {
                                        _layout.Padding = new Thickness(0, 10, 0, 10);
                                    }

                                    else if (i == item.Key.Count - 1)
                                    {

                                        _layout.Padding = new Thickness(0, 10, 0, 0);
                                    }
                                    else
                                    {

                                        _layout.Padding = new Thickness(0, 10, 0, 10);
                                    }


                                    _layout.Children.Add(_newLayout);

                                    _layout.Children.Add(bottomLine);
                                    TapGestureRecognizer _tapItem = new TapGestureRecognizer();


                                    //_textCat.GestureRecognizers.Add(_tapItem);
                                    _newLayout.GestureRecognizers.Add(_tapItem);

                                    _tapItem.Tapped += _tapItem_Tapped;
                                }
                                _layout.BackgroundColor = Color.White;

                                _frameLayout.Content = _layout;

                                dropDownLayout.Children.Add(_frameLayout);
                                LoaderPopup.CloseAllPopup(); // closing the loading popup.

                            }

                            else //if api is not returning the data.
                            {
                                CommonLib.ApiNotReturningData();
                            }
                        }
                        else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                        {
                            CommonLib.ApiResponseSlow();
                        }
                        else
                        {
                            CommonLib.Unauthorize();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoaderPopup.CloseAllPopup(); // closing the loading popup.
                await App.Current.MainPage.DisplayAlert("", ex.Message, "OK");

            }
            finally
            {

            }
        }

        /// <summary>
        /// Method that will save the details of the added notes into the api.
        /// </summary>
        async void AddNotes()
        {
            try
            {
                // checking of the internet connection.
                if (_base.CheckConnection())
                {
                    // Show Loading popup
                    await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                    AddNotesModel _obj = new AddNotesModel();
                    _obj.Category = value;
                    _obj.Content = txtDesc.Text;
                    _obj.ModuleRecordID = candidateID;
                    _obj.ModuleName = ModuleName;

                    // Calling of the api method
                    var result = await _base.AddNotes(CommonLib.MainUrl + ApiUrl.SaveNotes, _obj);
                    foreach (var item in result)
                    {
                        // If the status code is Ok
                        if (item.Value.StatusCode == HttpStatusCode.OK)
                        {
                            if (!string.IsNullOrEmpty(item.Key)) // if note saved successfully
                            {
                                LoaderPopup.CloseAllPopup(); // closing the loader popup
                                await App.Current.MainPage.DisplayAlert("Recruitlive", "Note Added Successfully", "OK");

                                // Clearing the navigation stack
                                var getNavigationStack = Navigation.NavigationStack.ToList();
                                var removeCurrentPage = getNavigationStack[getNavigationStack.Count - 1];
                                var removeNotesPage = getNavigationStack[getNavigationStack.Count - 2];
                                Navigation.RemovePage(removeCurrentPage);
                                Navigation.RemovePage(removeNotesPage);
                                await Application.Current.MainPage.Navigation.PushAsync(new NoteslistPage(candidateID,ModuleName));
                            }

                            else
                            {
                                LoaderPopup.CloseAllPopup();
                                await App.Current.MainPage.DisplayAlert("Recruitlive", "Please fill correct information", "OK");
                            }
                        }
                        else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                        {
                            CommonLib.ApiResponseSlow();
                        }
                        else
                        {
                            CommonLib.Unauthorize();
                        }
                    }

                }
            }
            catch (Exception)
            {
                LoaderPopup.CloseAllPopup();
            }
            finally
            {

            }

        }

    }
}