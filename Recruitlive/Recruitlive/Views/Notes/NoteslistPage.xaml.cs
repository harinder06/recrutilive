﻿using Recruitlive.Models;
using Recruitlive.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NoteslistPage : ContentPage
    {
        public int candidateid;
        public string moduleName;
        /// <summary>
        /// Constructor declaration
        /// </summary>
        /// <param name="candidateID"></param>
        public NoteslistPage(int candidateID,string _moduleName)
        {
            InitializeComponent();

            candidateid = candidateID;
            moduleName = _moduleName;
            this.Title = "Notes";

            //toolbar code
            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", () => {
                Navigation.PushAsync(new HomePage()); }));


            ToolbarItems.Add(new ToolbarItem("Info", "ic_add.png", () => {

                Navigation.PushAsync(new AddNotesPage(candidateid,moduleName));

            }));

            // view model integrated
            BindingContext = new NoteslistPageViewModel(candidateid,moduleName);

            //platform specific code
            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");
            }
        }


        // listview selection
        void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;
        }

        /// <summary>
        /// Open the notes details page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selecteditem = e.Item as NoteListItem;
            string type =selecteditem.SubTitle.Contains("MESSAGEID") ? "Email": "Note";
            Navigation.PushAsync(new NotesPages(selecteditem.NoteId,type,moduleName));
        }



    }
}
