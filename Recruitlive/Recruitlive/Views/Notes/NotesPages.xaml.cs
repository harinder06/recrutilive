﻿using Recruitlive.Repo;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotesPages : ContentPage
    {
        public int noteId;
        public string type;
        public string moduleName;
        HttpClientBase _base = new HttpClientBase();
        double height = App.ScreenHeight;
        double width = App.ScreenWidth;
        public NotesPages(int NoteId, string Type,string ModuleName)
        {
            InitializeComponent();

            noteId = NoteId;
            type = Type;
            moduleName = ModuleName;
            //platform specific code
            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");
            }
            this.Title = "Notes";


            // toolbar code
            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () =>
            {
                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindData();
        }

        /// <summary>
        /// This method will used to bind the api to the view with the data returning from the api.
        /// </summary>
        /// <returns></returns>
        public async void BindData()
        {
            try
            {
                // Checking the internet connection whether user is connected to the internet or not.
                if (_base.CheckConnection())
                {
                    // displaying the loading circle to the user.
                    await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                    // Getting all the details of the logged in user from the local table.
                    var getLoginDetails = App.Database.GetLoginUser();

                    // Calling of the api for getting the result.
                    var result = await _base.GetNoteDetails(CommonLib.MainUrl + ApiUrl.NotesDetails + noteId + "/"+moduleName+"/" + type);

                    foreach (var item in result)
                    {
                        if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            if (item.Key != null) // if api returns data successfully.
                            {
                                // Setting all the data on their respective fields.
                                categoryName.Text = item.Key.category;
                                postedBy.Text = item.Key.userID;
                                postedByLbl.Text = "Posted By:";
                                postedOnLbl.Text = "Posted On:";
                                postedOn.Text = item.Key.dateMade.ToString("MMMM dd,yyyy");
                                descriptionLabel.Text = item.Key.content;
                                LoaderPopup.CloseAllPopup(); // closing the loading popup.
                            }
                            else
                            {
                                CommonLib.ApiNotReturningData();
                            }
                        }
                        else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                        {

                            CommonLib.ApiResponseSlow();
                        }
                        else
                        {
                            CommonLib.Unauthorize();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoaderPopup.CloseAllPopup(); // closing the loading popup.
                await App.Current.MainPage.DisplayAlert("", ex.Message, "OK");

            }
            finally
            {

            }

        }
    }
}