﻿using Recruitlive.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class mapView : ContentPage
    {
        public double Lat;
        public double Long;

        string label_Address;
        Map map;
        public mapView(double _lat, double _long, string labelAddress)
        {
            InitializeComponent();
            this.Title = "Location";

            label_Address = labelAddress;
            Lat = _lat;
            Long = _long;

            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");
            }
            else
            {

            }



            map = new Map
            {
                IsShowingUser = true,
                HeightRequest = 100,
                WidthRequest = 960,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            map.MoveToRegion(MapSpan.FromCenterAndRadius(
                new Position(Lat, Long), Distance.FromMiles(3))); // Santa Cruz golf course

            var position = new Position(Lat,Long); // Latitude, Longitude
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = labelAddress,
                //Address = "custom detail info"
            };
            map.Pins.Add(pin);


           
            // put the page together
            Content = new StackLayout
            {
                Spacing = 0,
                Children = {
                    map
                }
            };
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();

            // On Droid this wraps behind the other views.
            if (Device.RuntimePlatform == Device.Android)
            {
                Grid.SetRowSpan(MapView, 3);

                if (ToolbarItems.Count > 1)
                {
                    var firstItem = ToolbarItems.FirstOrDefault();
                    ToolbarItems.Remove(firstItem);
                }
            }

          
        }
    }
}