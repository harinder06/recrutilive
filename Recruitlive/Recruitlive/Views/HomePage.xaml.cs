﻿using Recruitlive.Repo;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        HttpClientBase _base = new HttpClientBase();
        double screenHeight, screenWidth;
        public HomePage()
        {
            InitializeComponent();

            NavigationPage.SetHasBackButton(this, false);

            scrollViewlayout.IsEnabled = true;
            this.Title = "Dashboard";

            screenHeight = App.ScreenHeight;

            screenWidth = App.ScreenWidth;

           
            //platform specific code

            if (Device.RuntimePlatform == Device.Android)
            {
                // below code is for setting home page screen for tablet
                if (screenWidth >= 800)
                {


                    //first Box

                    upperCandidateLayoutWidth.Width = (screenHeight / 4) + 5;

                    upperCandidateLayoutHeight.Height = screenHeight / 4;

                    firstImage.WidthRequest = 100;

                    firstImage.HeightRequest = 100;

                    firstlabel.FontSize = 20;

                    //second Box

                    upperClientLayoutWidth.Width = (screenHeight / 4) + 5;
                    upperClientLayoutHeight.Height = screenHeight / 4;



                    secondImage.WidthRequest = 100;

                    secondImage.HeightRequest = 100;

                    secondLabel.FontSize = 20;

                    //third box
                    upperJobLayoutWidth.Width = (screenHeight / 4) + 5;
                    upperJobLayoutHeight.Height = screenHeight / 4;

                    thirdImage.WidthRequest = 100;

                    thirdImage.HeightRequest = 100;

                    thirdlabel.FontSize = 20;

                    //fourth box
                    upperTimesheetLayoutWidth.Width = (screenHeight / 4) + 5;
                    upperTimesheetLayoutHeight.Height = screenHeight / 4;

                    fourthImage.WidthRequest = 100;

                    fourthImage.HeightRequest = 100;

                    fourthLabel.FontSize = 20;


                    //fifth box



                    upperContactsLayoutWidth.Width = (screenHeight / 4) + 5;

                    upperContactsLayoutHeight.Height = screenHeight / 4;

                    fifthImage.WidthRequest = 100;

                    fifthImage.HeightRequest = 100;

                    fifthLabel.FontSize = 20;



                    //six box


                    upperSettingsLayoutWidth.Width = (screenHeight / 4) + 5;
                    upperSettingsLayoutHeight.Height = screenHeight / 4;


                    sixthImage.WidthRequest = 100;

                    sixthImage.HeightRequest = 100;

                    sixthLabel.FontSize = 20;
                }
                else
                {
                    //first Box

                    upperCandidateLayoutWidth.Width = (screenHeight / 5) + 5;

                    upperCandidateLayoutHeight.Height = screenHeight / 5;

                    //second Box

                    upperClientLayoutWidth.Width = (screenHeight / 5) + 5;
                    upperClientLayoutHeight.Height = screenHeight / 5;



                    //third box
                    upperJobLayoutWidth.Width = (screenHeight / 5) + 5;
                    upperJobLayoutHeight.Height = screenHeight / 5;

                    //fourth box
                    upperTimesheetLayoutWidth.Width = (screenHeight / 5) + 5;
                    upperTimesheetLayoutHeight.Height = screenHeight / 5;

                    //fifth box

                    upperContactsLayoutWidth.Width = (screenHeight / 5) + 5;
                    upperContactsLayoutHeight.Height = screenHeight / 5;

                    //six box

                    upperSettingsLayoutWidth.Width = (screenHeight / 5) + 5;
                    upperSettingsLayoutHeight.Height = screenHeight / 5;

                }
            }

            else
            {

                NavigationPage.SetBackButtonTitle(this, "");

                if (screenWidth >= 740)
                {
                    scrollViewlayout.IsEnabled = true;
                    //first Box

                    upperCandidateLayoutWidth.Width = (screenHeight / 4) + 5;

                    upperCandidateLayoutHeight.Height = screenHeight / 4;

                    firstImage.WidthRequest = 100;

                    firstImage.HeightRequest = 100;

                    firstlabel.FontSize = 20;



                    //second Box

                    upperClientLayoutWidth.Width = (screenHeight / 4) + 5;

                    upperClientLayoutHeight.Height = screenHeight / 4;



                    secondImage.WidthRequest = 100;

                    secondImage.HeightRequest = 100;

                    secondLabel.FontSize = 20;



                    //third box
                    upperJobLayoutWidth.Width = (screenHeight / 4) + 5;

                    upperJobLayoutHeight.Height = screenHeight / 4;

                    thirdImage.WidthRequest = 100;

                    thirdImage.HeightRequest = 100;

                    thirdlabel.FontSize = 20;



                    //fourth box

                    upperTimesheetLayoutWidth.Width = (screenHeight / 4) + 5;

                    upperTimesheetLayoutHeight.Height = screenHeight / 4;

                    fourthImage.WidthRequest = 100;

                    fourthImage.HeightRequest = 100;

                    fourthLabel.FontSize = 20;


                    //fifth box



                    upperContactsLayoutWidth.Width = (screenHeight / 4) + 5;

                    upperContactsLayoutHeight.Height = screenHeight / 4;

                    fifthImage.WidthRequest = 100;

                    fifthImage.HeightRequest = 100;

                    fifthLabel.FontSize = 20;



                    //six box


                    upperSettingsLayoutWidth.Width = (screenHeight / 4) + 5;
                    upperSettingsLayoutHeight.Height = screenHeight / 4;


                    sixthImage.WidthRequest = 100;

                    sixthImage.HeightRequest = 100;

                    sixthLabel.FontSize = 20;
                }

                else
                {
                    //first Box
                    upperCandidateLayoutWidth.Width = (screenHeight / 6) + 5;
                    upperCandidateLayoutHeight.Height = screenHeight / 6;

                    //second Box
                    upperClientLayoutWidth.Width = (screenHeight / 6) + 5;
                    upperClientLayoutHeight.Height = screenHeight / 6;
                    //third box
                    upperJobLayoutWidth.Width = (screenHeight / 6) + 5;
                    upperJobLayoutHeight.Height = screenHeight / 6;

                    //fourth box
                    upperTimesheetLayoutWidth.Width = (screenHeight / 6) + 5;
                    upperTimesheetLayoutHeight.Height = screenHeight / 6;


                    //fifth box

                    upperContactsLayoutWidth.Width = (screenHeight / 6) + 5;
                    upperContactsLayoutHeight.Height = screenHeight / 6;

                    //six box

                    upperSettingsLayoutWidth.Width = (screenHeight / 6) + 5;
                    upperSettingsLayoutHeight.Height = screenHeight / 6;



                }
            }

            //toolbar code

            ToolbarItems.Add(new ToolbarItem("Info", "logout.png", async () =>
            {
                var ans = await DisplayAlert("Recruitlive", "Are you sure you want to logout?", "Yes", "No");
                if (ans == true)
                {
                    await Navigation.PushAsync(new LoaderPopup());
                    var result = await _base.GetMethod(CommonLib.MainUrl + ApiUrl.LogOut);
                    if (result == "true")
                    {
                        // calling the clear details method from the common library that will clear all the details of the logged in user.
                        CommonLib.ClearAllData();
                        LoaderPopup.CloseAllPopup();
                        // presenting the login page to the user.
                        await Application.Current.MainPage.Navigation.PushAsync(new LoginPage());
                    }

                }

            }));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            //string hh = "jj";
            //int ii = Convert.ToInt32(hh);

            var getCandidateFilters = App.Database.GetCandidateFilters();
            var getClientFilters = App.Database.GetClientFilters();
            var getContactFilters = App.Database.GetContactFilters();
            if (getCandidateFilters != null){ App.Database.ClearCandidateFilterDetails(); }
            if (getClientFilters != null){ App.Database.ClearClientFilterDetails(); }
            if (getContactFilters != null){ App.Database.ClearContactFilterDetails();}            
        }

        // invoke candidate page
        async void candidate_Tapped(object sender, EventArgs e)
        {
            SearchCandidatePage.SearchBartxt = string.Empty;
            await Application.Current.MainPage.Navigation.PushAsync(new SearchCandidatePage(null, false));
        }

        // invoke client page
        async void client_Tapped(object sender, EventArgs e)
        {
            ClientListPage.SearchBartxt = string.Empty;
            await Application.Current.MainPage.Navigation.PushAsync(new ClientListPage(null, false));
        }



        //invoke job page
        async void job_Tapped(object sender, EventArgs e)
        {
            //JobListPage.SearchBartxt = string.Empty;
            await Application.Current.MainPage.Navigation.PushAsync(new JobListPage());
        }



        //invoke timesheet page
        async void timesheet_Tapped(object sender, EventArgs e)
        {
            await Application.Current.MainPage.Navigation.PushAsync(new TimesheetToApprove());
        }


        //invoke contacts page

        async void contacts_Tapped(object sender, EventArgs e)
        {
            ContactListPage.SearchBartxt = string.Empty;
            await Application.Current.MainPage.Navigation.PushAsync(new ContactListPage(null, false));
        }


        //invoke settings page

        async void settings_Tapped(object sender, EventArgs e)
        {
            await Application.Current.MainPage.Navigation.PushAsync(new SettingPage());
        }



    }
}