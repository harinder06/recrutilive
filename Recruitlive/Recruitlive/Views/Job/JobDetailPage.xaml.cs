﻿using Recruitlive.Repo;
using Recruitlive.ViewModels;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class JobDetailPage : ContentPage
    {
        public int jobId;
        public JobDetailPage(int jobID)
        {
            jobId = jobID;

            InitializeComponent();

            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");
            }
            else
            {

            }

            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () =>
            {
                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

            this.Title = "Job Detail";

            BindingContext = new JobDetailViewModel(jobID, Navigation);

        }

        void ListView_OnItemTapped(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;
        }

        /// <summary>
        /// Method that will use to show the client detail page when the client button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void addClient_Tapped(object sender, EventArgs e)
        {
            var _model = BindingContext as JobDetailViewModel;
            if (_model != null)
                await Application.Current.MainPage.Navigation.PushAsync(new ClientDetailPage(_model.ClientId));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

        }

        /// <summary>
        /// calling of the job applicants page when button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void Job_Ad_Responses_Clicked(object sender, EventArgs e)
        {
            var _model = BindingContext as JobDetailViewModel;
            if (_model != null)
            {
                await Application.Current.MainPage.Navigation.PushAsync(new JobApplicantPage(_model.Code,_model.jobID));
            }              
        }

        /// <summary>
        /// calling of the contact detail page on contact button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void AddContactTapped(object sender, EventArgs e)
        {
            var _model = BindingContext as JobDetailViewModel;
            if (_model != null)
            {
                await Application.Current.MainPage.Navigation.PushAsync(new ContactDetailPage(_model.ContactId, null));
            }
        }

    }
}