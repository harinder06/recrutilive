﻿using Recruitlive.Models;
using Recruitlive.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class JobApplicantPage : ContentPage
    {
        public string _JobCode = string.Empty;
        public JobApplicantPage(string jobCode,int jobID)
        {
            InitializeComponent();

            _JobCode = jobCode;

            this.Title = "Job Applicants";


            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () => {

                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

            JobApplicantPostData _data = new JobApplicantPostData();
            _data.AttachmentType = "Resume";
            //_data.JobReference = "JXSU";
            _data.JobReference = jobCode;
            _data.ModuleName = "Job";

            BindingContext = new JobApplicantViewModel(_data,Navigation,jobID);


            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");
            }
            else
            {

            }
        }

        void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }

            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;
        }
        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selecteditem = timesheetList.SelectedItem as JobApplicantModel;
            timesheetList.SelectedItem = null;
        }
    }
}