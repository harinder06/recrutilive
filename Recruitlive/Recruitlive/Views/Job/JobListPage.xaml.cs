﻿using Recruitlive.Models;
using Recruitlive.Models.Job;
using Recruitlive.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class JobListPage : ContentPage
    {
        double screenHeight = App.ScreenHeight;
        double screenWidth = App.ScreenWidth;
        public JobListPage()
        {
            InitializeComponent();

            this.Title = "Job List";


            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () =>
            {
                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));


            // device platform specific code 

            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");

                jobList.Margin = new Thickness(0, 10, 0, 10);
            }
            else
            { }

            BindingContext = new JobListViewModel();
        }


        void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;
        }       


        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selecteditem = e.Item as JobListModel;
            var jobId = Convert.ToInt32(selecteditem.jobID);
            Navigation.PushAsync(new JobDetailPage(jobId));
        }
    }
}