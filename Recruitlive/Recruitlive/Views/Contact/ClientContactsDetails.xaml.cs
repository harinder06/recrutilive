﻿using Recruitlive.Models;
using Recruitlive.Models.Contact;
using Recruitlive.ViewModels.Contact;
using System;
using Xamarin.Forms;

namespace Recruitlive.Views.Contact
{
    public partial class ClientContactsDetails : ContentPage
    {
        public static ContactSearchPost _postData;
        public ClientContactsDetails(ContactSearchPost _data)
        {
            InitializeComponent();


            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () => {

                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

            _postData = _data;

            this.Title = "Contacts";

            // device platform specific code 

            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");

                contactList.Margin = new Thickness(0, 10, 0, 10);
            }

            else
            { }

            BindingContext = new ClientContactViewModel(_data);

        }

        void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            //DisplayAlert("Item Selected", e.SelectedItem.ToString(), "Ok");
            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selecteditem = e.Item as CandidateListModel;
            var splitContactId = selecteditem.emmployeeID.Split(' ');
            var contactId = Convert.ToInt32(splitContactId[1]);
            Navigation.PushAsync(new ContactDetailPage(contactId, _postData));
        }
    }
}
