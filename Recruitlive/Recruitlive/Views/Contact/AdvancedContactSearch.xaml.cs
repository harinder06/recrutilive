﻿using Recruitlive.Models;
using Recruitlive.Models.Contact;
using Recruitlive.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace Recruitlive.Views.Contact
{
    public partial class AdvancedContactSearch : ContentPage
    {
        HttpClientBase _base = new HttpClientBase();
        public ContactFilters getContactFilters = App.Database.GetContactFilters();
        public static string getRoles;
        public static string getSkills;
        public static string advanceFirstNameTxt;
        public static ContactSearchPost _postData = new ContactSearchPost();
        public static List<string> SelectedRoles = new List<string>();
        public static List<string> SelectedSkills = new List<string>();
        public bool IsApplied = false;
        public AdvancedContactSearch(string searchedText)
        {
            IsApplied = false;

            SelectedRoles = new List<string>();
            SelectedSkills = new List<string>();
            getRoles = string.Empty;
            getSkills = string.Empty;

            if (getContactFilters != null)
            {
                // adding the roles data from local storage
                if (!string.IsNullOrEmpty(getContactFilters.AutoCoreRoles))
                {
                    string[] splitRoleStrings = getContactFilters.AutoCoreRoles.Split(',');

                    if (splitRoleStrings.Length > 0)
                    {
                        for (int i = 0; i < splitRoleStrings.Length; i++)
                        {
                            if (!SelectedRoles.Contains(splitRoleStrings[i]))
                            {
                                SelectedRoles.Add(splitRoleStrings[i]);
                            }
                        }
                    }
                }
                // adding the skills data from local storage
                if (!string.IsNullOrEmpty(getContactFilters.AutoCoreSkills))
                {
                    string[] splitSkillsStrings = getContactFilters.AutoCoreSkills.Split(',');

                    if (splitSkillsStrings.Length > 0)
                    {
                        for (int i = 0; i < splitSkillsStrings.Length; i++)
                        {
                            if (!SelectedSkills.Contains(splitSkillsStrings[i]))
                            {
                                SelectedSkills.Add(splitSkillsStrings[i]);
                            }
                        }
                    }
                }
            }
            InitializeComponent();

            #region setting search bar text
            //auto fill first name text

            if (string.IsNullOrEmpty(searchedText))
            {
                firstName.Text = string.Empty;

                lastName.Text = string.Empty;
            }
            else
            {
                if (!string.IsNullOrEmpty(searchedText))
                {
                    if (searchedText.Contains(" "))
                    {
                        firstName.Text = searchedText.Substring(0, searchedText.IndexOf(" "));

                        lastName.Text = searchedText.Substring(searchedText.LastIndexOf(' ') + 1);
                    }
                    else
                    {
                        firstName.Text = searchedText;
                        lastName.Text = string.Empty;
                    }
                }
                advanceFirstNameTxt = firstName.Text;
            }

            #endregion

            ToolbarItems.Add(new ToolbarItem("Apply", "applyicon.png", async () =>
            {
                var returnMessage = CheckApplyValidation();
                if (!string.IsNullOrEmpty(returnMessage))
                {
                    await App.Current.MainPage.DisplayAlert("Recruitlive", returnMessage, "OK");
                    return;
                }
                ApplyMethod();
            }));

            this.Title = "Contact";

            //platform specific code
            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");
            }

            #region Setting Contact Filters

            if (getContactFilters != null)
            {
                //setting all the data in all the text boxes that is get from the local database of the candidate filters.
                if (!string.IsNullOrEmpty(getContactFilters.FirstName) && string.IsNullOrEmpty(advanceFirstNameTxt))
                {
                    firstName.Text = getContactFilters.FirstName; // First Name TextBox
                }
                if (!string.IsNullOrEmpty(getContactFilters.LastName))
                {
                    lastName.Text = getContactFilters.LastName; //Last Name TextBox
                }
                if (!string.IsNullOrEmpty(getContactFilters.Street))
                    street.Text = getContactFilters.Street; // Street TextBox
                if (!string.IsNullOrEmpty(getContactFilters.PostCode))
                    postCode.Text = getContactFilters.PostCode; //PostCode TextBox
                if (!string.IsNullOrEmpty(getContactFilters.Client))
                    client.Text = getContactFilters.Client; // Client Text Box
                if (!string.IsNullOrEmpty(getContactFilters.AutoSuburb))
                    city.Text = getContactFilters.AutoSuburb; // City Text Box
                if (!string.IsNullOrEmpty(getContactFilters.Email))
                    email.Text = getContactFilters.Email; // Email Text Box
                if (!string.IsNullOrEmpty(getContactFilters.AutoCoreSkills))
                    getSkills = getContactFilters.AutoCoreSkills;
                if (!string.IsNullOrEmpty(getContactFilters.AutoCoreRoles))
                    getRoles = getContactFilters.AutoCoreRoles;
            }
            #endregion
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (SelectedRoles != null)
            {
                if (SelectedRoles.Count > 0)
                {
                    // displaying roles selected data
                    DynamicRolesLayout.Children.Clear();
                    for (int i = 0; i < SelectedRoles.Count; i++)
                    {
                        DynamicRolesLayout.IsVisible = true;

                        Label _rolesText = new Label();
                        _rolesText.TextColor = Color.White;
                        _rolesText.Text = SelectedRoles[i];
                        _rolesText.FontSize = 15;
                        _rolesText.HorizontalOptions = LayoutOptions.StartAndExpand;
                        _rolesText.VerticalOptions = LayoutOptions.Center;
                        _rolesText.LineBreakMode = LineBreakMode.TailTruncation;

                        Image _rolesImage = new Image();
                        _rolesImage.Source = "check_box_filled.png";
                        _rolesImage.HeightRequest = 15;
                        _rolesImage.WidthRequest = 15;
                        _rolesImage.BindingContext = "1";
                        _rolesImage.HorizontalOptions = LayoutOptions.EndAndExpand;
                        _rolesImage.VerticalOptions = LayoutOptions.Center;

                        StackLayout _rolesHorizontalLayout = new StackLayout();
                        _rolesHorizontalLayout.Orientation = StackOrientation.Horizontal;
                        _rolesHorizontalLayout.Padding = new Thickness(5);
                        _rolesHorizontalLayout.ClassId = SelectedRoles[i];

                        TapGestureRecognizer _tapRolesCross = new TapGestureRecognizer();
                        _rolesHorizontalLayout.GestureRecognizers.Add(_tapRolesCross);
                        _tapRolesCross.Tapped += _tapRolesCross_Tapped;

                        _rolesHorizontalLayout.Children.Add(_rolesText);
                        _rolesHorizontalLayout.Children.Add(_rolesImage);

                        DynamicRolesLayout.Children.Add(_rolesHorizontalLayout);
                    }
                }
                else
                {
                    DynamicRolesLayout.Children.Clear();
                }
            }


            if (SelectedSkills != null)
            {
                // displaying skills selected data
                if (SelectedSkills.Count > 0)
                {
                    DynamicSkillsLayout.Children.Clear();
                    for (int i = 0; i < SelectedSkills.Count; i++)
                    {
                        DynamicSkillsLayout.IsVisible = true;

                        Label _skillsText = new Label();
                        _skillsText.TextColor = Color.White;
                        _skillsText.FontSize = 15;
                        _skillsText.VerticalOptions = LayoutOptions.Center;
                        _skillsText.LineBreakMode = LineBreakMode.TailTruncation;
                        _skillsText.Text = SelectedSkills[i];
                        _skillsText.HorizontalOptions = LayoutOptions.StartAndExpand;

                        Image _skillsImage = new Image();
                        _skillsImage.Source = "check_box_filled.png";
                        _skillsImage.HeightRequest = 15;
                        _skillsImage.WidthRequest = 15;
                        _skillsImage.BindingContext = "1";
                        _skillsImage.HorizontalOptions = LayoutOptions.EndAndExpand;
                        _skillsImage.VerticalOptions = LayoutOptions.Center;

                        StackLayout _skillsHorizontalLayout = new StackLayout();
                        _skillsHorizontalLayout.Orientation = StackOrientation.Horizontal;
                        _skillsHorizontalLayout.Padding = new Thickness(5);
                        _skillsHorizontalLayout.ClassId = SelectedSkills[i];

                        TapGestureRecognizer _tapSkillsCross = new TapGestureRecognizer();
                        _skillsHorizontalLayout.GestureRecognizers.Add(_tapSkillsCross);
                        _tapSkillsCross.Tapped += _tapSkillsCross_Tapped;

                        _skillsHorizontalLayout.Children.Add(_skillsText);
                        _skillsHorizontalLayout.Children.Add(_skillsImage);

                        DynamicSkillsLayout.Children.Add(_skillsHorizontalLayout);
                    }
                }
                else
                {
                    DynamicSkillsLayout.Children.Clear(); // clearing the children of the layout
                }
            }

        }

        /// <summary>
        /// methos will used when the cross icon from the skills filters already selected has been pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _tapSkillsCross_Tapped(object sender, EventArgs e)
        {
            var stackLayout = sender as StackLayout;
            string removeSkills = stackLayout.ClassId;

            var checkedImage = stackLayout.Children.Where(a => a.BindingContext != null).FirstOrDefault();
            var control = checkedImage as Image;

            if (control.BindingContext.ToString() == "0")
            {
                // changing the checkbox image 
                control.Source = "check_box_filled.png";
                // removing the selected data
                SelectedSkills.Add(removeSkills);
                control.BindingContext = "1";
            }
            else
            {
                control.Source = "check_box_empty.png";
                // removing the selected data
                var _Data = SelectedSkills.Where(a => a.Equals(removeSkills)).FirstOrDefault();
                SelectedSkills.Remove(removeSkills);
                control.BindingContext = "0";
            }
            var newArray = SelectedSkills.ToArray();
            getSkills = string.Join(",", newArray);
        }

        /// <summary>
        /// methos will used when the cross icon from the roles filters already selected has been pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _tapRolesCross_Tapped(object sender, EventArgs e)
        {
            var stackLayout = sender as StackLayout;
            string removeRoles = stackLayout.ClassId;
            var checkedImage = stackLayout.Children.Where(a => a.BindingContext != null).FirstOrDefault();
            var control = checkedImage as Image;

            if (control.BindingContext.ToString() == "0")
            {
                // changing the checkbox image 
                control.Source = "check_box_filled.png";
                // removing the selected data
                SelectedRoles.Add(removeRoles);
                control.BindingContext = "1";
            }
            else
            {
                control.Source = "check_box_empty.png";
                // removing the selected data
                var _Data = SelectedRoles.Where(a => a.Equals(removeRoles)).FirstOrDefault();
                SelectedRoles.Remove(removeRoles);
                control.BindingContext = "0";
            }

            var newArray = SelectedRoles.ToArray();
            getRoles = string.Join(",", newArray);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            // passing the data to the search candidate page to set in the search bar
            if (!string.IsNullOrEmpty(firstName.Text) && !string.IsNullOrEmpty(lastName.Text))
            {
                ContactListPage.SearchBartxt = firstName.Text + " " + lastName.Text;
            }
            else if (!string.IsNullOrEmpty(firstName.Text) && string.IsNullOrEmpty(lastName.Text))
            {
                ContactListPage.SearchBartxt = firstName.Text;
            }
            else if (string.IsNullOrEmpty(firstName.Text) && !string.IsNullOrEmpty(lastName.Text))
            {
                ContactListPage.SearchBartxt = " " + lastName.Text;
            }
            else
            {
                ContactListPage.SearchBartxt = "";
            }
            ContactFilters filterData = new ContactFilters();
            var getLoginDetails = App.Database.GetLoginUser();
            if (!IsApplied)
            {
                // gettimg candidate filters stored in local storage
                filterData = App.Database.GetContactFilters();
                if (filterData == null)
                {
                    // svaing the candidate filters for the first time
                    filterData = new ContactFilters();
                    filterData.FirstName = firstName.Text;
                    filterData.LastName = lastName.Text;
                    filterData.Section = "Contact";
                    filterData.UserName = getLoginDetails.user_name;
                    filterData.BranchName = getLoginDetails.branch;

                    App.Database.SaveContactFilters(filterData);
                }
                else
                {
                    filterData.FirstName = firstName.Text;
                    filterData.LastName = lastName.Text;
                    filterData.PostCode = filterData.PostCode == postCode.Text ? filterData.PostCode : "";
                    filterData.Street = filterData.Street == street.Text ? filterData.Street : "";
                    filterData.Client = filterData.Client == client.Text ? filterData.Client : "";
                    filterData.AutoSuburb = filterData.AutoSuburb == city.Text ? filterData.AutoSuburb : "";
                    filterData.Email = filterData.Email == email.Text ? filterData.Email : "";
                    filterData.AutoCoreRoles = string.IsNullOrEmpty(filterData.AutoCoreRoles) ? "" : getRoles;
                    filterData.AutoCoreSkills = string.IsNullOrEmpty(filterData.AutoCoreSkills) ? "" : getSkills;

                    filterData.Section = "Contact";
                    filterData.UserName = getLoginDetails.user_name;
                    filterData.BranchName = getLoginDetails.branch;


                    App.Database.SaveContactFilters(filterData);
                }
                ContactListPage.IsHighlighted = true;
            }
            else
            {
                // saving the candidate filters if apply button is clicked
                SaveContactFilters(getLoginDetails);
            }


        }


        /// <summary>
        /// roles text box changed event ot get all the roles from the api.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Roles_Label_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RolesContactListView());
        }

        /// <summary>
        /// skills text box changed event to get all the skills with matched text from the api.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Skills_Label_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SkillsContactListView());
        }

        void more_Tapped(object sender, EventArgs e)
        {
            if (employeraNIdlyt.IsVisible == true)
            {
                moreAndLessLabel.Text = "More";
                moreAndLessImage.Source = "more.png";
                employeraNIdlyt.IsVisible = false;
            }
            else
            {
                moreAndLessLabel.Text = "Less";
                moreAndLessImage.Source = "less.png";
                employeraNIdlyt.IsVisible = true;
            }
        }

        /// <summary>
        /// Method for checking the apply button validation if all the values are null then it give the apply button validation message.
        /// </summary>
        /// <returns></returns>
        public string CheckApplyValidation()
        {
            string msg = string.Empty;
            if (!String.IsNullOrWhiteSpace(email.Text))
            {
                if (!CommonLib.CheckValidEmail(email.Text))
                {
                    msg = "Please Enter Valid Email Address" + Environment.NewLine;
                }
            }
            if (string.IsNullOrEmpty(firstName.Text) && string.IsNullOrEmpty(lastName.Text)
                && string.IsNullOrEmpty(street.Text) && string.IsNullOrEmpty(postCode.Text) && string.IsNullOrEmpty(email.Text) && string.IsNullOrEmpty(city.Text) && string.IsNullOrEmpty(client.Text)
                && string.IsNullOrEmpty(getRoles) && string.IsNullOrEmpty(getSkills))
            {
                msg += "Please select filters first before applying";
            }
            return msg;
        }


        /// <summary>
        /// method that will use to save all the filters into the local database and to set all the 
        /// filters in the object to pass this object on the search candidate page.
        /// </summary>
        public void ApplyMethod()
        {
            IsApplied = true;
            _postData.FirstName = string.IsNullOrEmpty(firstName.Text) ? string.Empty : firstName.Text;
            _postData.LastName = string.IsNullOrEmpty(lastName.Text) ? string.Empty : lastName.Text;
            _postData.Street = string.IsNullOrEmpty(street.Text) ? string.Empty : street.Text;
            _postData.PostCode = string.IsNullOrEmpty(postCode.Text) ? string.Empty : postCode.Text;
            _postData.Client = string.IsNullOrEmpty(client.Text) ? string.Empty : client.Text;
            _postData.AutoSuburb = string.IsNullOrEmpty(city.Text) ? string.Empty : city.Text;
            _postData.AutoCoreRoles = getRoles;
            _postData.AutoCoreSkills = getSkills;

            Email _email = new Email();
            _email.Details = string.IsNullOrEmpty(email.Text) ? string.Empty : email.Text;
            _postData.Email = _email;


            var getNavigationStack = Navigation.NavigationStack.ToList();
            var removeCurrentPage = getNavigationStack[getNavigationStack.Count - 1];
            var removeCandidateListPage = getNavigationStack[getNavigationStack.Count - 2];
            if (Device.RuntimePlatform == Device.Android)
            {
                // Clearing the navigation stack
                Navigation.RemovePage(removeCurrentPage);
                Navigation.RemovePage(removeCandidateListPage);
                Application.Current.MainPage.Navigation.PushAsync(new ContactListPage(_postData, true));
            }
            else
            {
                Navigation.InsertPageBefore(new ContactListPage(_postData, true), Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
                Navigation.RemovePage(this);
                Navigation.RemovePage(removeCandidateListPage);
            }

        }

        /// <summary>
        /// method that will save all the candidate filters in the local storage
        /// </summary>
        /// <returns></returns>
        public bool SaveContactFilters(SaveLoginResponse getLoginDetails)
        {
            //Getting the logged in user

            bool result = false;
            try
            {
                // Saving the filters into the local database.

                #region Saving Details In Local Database

                ContactFilters _obj = new ContactFilters();

                _obj.FirstName = firstName.Text;
                _obj.LastName = lastName.Text;
                _obj.AutoCoreRoles = getRoles;
                _obj.AutoCoreSkills = getSkills;
                _obj.PostCode = postCode.Text;
                _obj.Client = client.Text;
                _obj.AutoSuburb = city.Text;
                _obj.Street = street.Text;
                _obj.Email = email.Text;
                _obj.Section = "Contact";
                _obj.UserName = getLoginDetails.user_name;
                _obj.BranchName = getLoginDetails.branch;

                IsApplied = true;

                // saving the deatils into the database.
                App.Database.SaveContactFilters(_obj);
                #endregion
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            return result;

        }

    }
}
