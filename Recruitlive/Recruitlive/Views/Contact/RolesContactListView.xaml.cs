﻿using Recruitlive.ViewModels;
using Xamarin.Forms;

namespace Recruitlive.Views.Contact
{
    public partial class RolesContactListView : ContentPage
    {
        public RolesContactListView()
        {
            InitializeComponent();

            this.Title = "Select Roles";

            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () =>
            {
                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

            BindingContext = new ContactRolesListViewModel();
        }

        void Search_Bar_TextChanged(object sender, TextChangedEventArgs e)
        {
            var bindingContext = (ContactRolesListViewModel)BindingContext;
            bindingContext.SearchedData();
        }

        /// <summary>
        /// making the list view selection null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Selected_Item(object sender, ItemTappedEventArgs e)
        {
            var listView = sender as ListView;
            if (listView != null)
            {
                listView.SelectedItem = null;
            }
        }
    }
}
