﻿using Recruitlive.Models.Contact;
using Recruitlive.Repo;
using Recruitlive.ViewModels.Contact;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactDetailPage : ContentPage
    {
        HttpClientBase _base = new HttpClientBase();
        public int contactID;
        public static ContactSearchPost _SearchedData;


        public ContactDetailPage(int contactId, ContactSearchPost _data)
        {
            InitializeComponent();

            contactID = contactId;
            _SearchedData = _data;
            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");
            }
            else
            {}
            this.Title = "Contact Detail";


            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () => {

                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

            BindingContext = new ContactDetailsViewModel(contactId,Navigation);
        }

        void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }


    }
}