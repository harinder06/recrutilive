﻿using Recruitlive.Models;
using Recruitlive.Models.Contact;
using Recruitlive.ViewModels;
using Recruitlive.Views.Contact;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactListPage : ContentPage
    {
        double screenHeight = App.ScreenHeight;
        double screenWidth = App.ScreenWidth;
        public static string SearchBartxt;
        public static ContactSearchPost _postData;
        public static bool checkVal;
        public static bool IsHighlighted = false;

        /// <summary>
        /// Constructor declaration
        /// </summary>
        /// <param name="_data"></param>
        /// <param name="check"></param>
        public ContactListPage(ContactSearchPost _data, bool check)
        {
            InitializeComponent();
            _postData = _data;
            checkVal = check;

            this.Title = "Contact List";

            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () =>
            {
                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

            // device platform specific code 

            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");

                if (screenWidth >= 740)
                {
                    searchBar.WidthRequest = screenWidth - 150;
                }
                else
                {
                    searchBar.WidthRequest = 300;
                }

                searchBarFrame.HeightRequest = 70;

                searchBarFrame.Padding = new Thickness(0, 0, 0, 0);

                searchBar.Margin = new Thickness(0, 0, 0, 0);

                advanceFilterIcon.Margin = new Thickness(0, 0, 10, 0);

                advanceFilterIcon.HeightRequest = 30;

                advanceFilterIcon.WidthRequest = 30;

                stacklayoutBarFrame.HeightRequest = 70;

                stacklayoutBarFrame.Padding = new Thickness(0, 0, 0, 0);

                contactList.Margin = new Thickness(0, 10, 0, 10);
            }

            else
            { }

            if (_postData == null)
            {
                BindingContext = new ContactListViewModel(null, checkVal);
            }
            else
            {
                BindingContext = new ContactListViewModel(_postData, checkVal);
            }

        }

        /// <summary>
        /// method runs when the page appeared on the screen
        /// </summary>
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (IsHighlighted == true)
            {
                var bindingContext = (ContactListViewModel)BindingContext;
                bindingContext.CheckIsHighLighted(IsHighlighted);
                IsHighlighted = false;
            }

            if (!string.IsNullOrEmpty(SearchBartxt))
            {
                searchBar.Text = SearchBartxt;
            }
            else
            {
                searchBar.Text = string.Empty;
            }
        }

        /// <summary>
        /// method that will perform the actions when the search button from the search bar pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Search_Button_Pressed(object sender, EventArgs e)
        {
            var searchBar = (SearchBar)sender;

            var getContactFilters = App.Database.GetContactFilters();

            if (getContactFilters != null)
            {
                if (_postData != null)
                {
                    var checkName = _postData.FirstName + " " + _postData.LastName;
                    if (checkName != searchBar.Text)
                    {
                        getContactFilters.AutoCoreRoles = string.Empty;
                        getContactFilters.AutoCoreSkills = string.Empty;
                        getContactFilters.AutoSuburb = string.Empty;
                        getContactFilters.Client = string.Empty;
                        getContactFilters.Email = string.Empty;
                        getContactFilters.PostCode = string.Empty;
                        getContactFilters.Street = string.Empty;
                    }
                }
                App.Database.SaveContactFilters(getContactFilters);
            }

            _postData = new ContactSearchPost();
            if (searchBar.Text.Contains(" "))
            {
                string[] splitString = searchBar.Text.Split(' ');
                _postData.FirstName = splitString[0];
                _postData.LastName = splitString[1];
            }
            else
            {
                _postData.FirstName = searchBar.Text;
            }

            // setting the binding context
            Models.Contact.Email _email = new Models.Contact.Email();
            _email.Details = string.Empty;
            _postData.Email = _email;

            BindingContext = new ContactListViewModel(_postData, false);
            searchedText.IsVisible = true;
            SearchBartxt = searchBar.Text;
        }

        void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            //DisplayAlert("Item Selected", e.SelectedItem.ToString(), "Ok");
            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;
        }
        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selecteditem = e.Item as CandidateListModel;
            var splitContactId = selecteditem.emmployeeID.Split(' ');
            var contactId = Convert.ToInt32(splitContactId[1]);
            Navigation.PushAsync(new ContactDetailPage(contactId, _postData));
        }

        //below tapped push to advanced search page
        async void advancedCandidateFltr_Tapped(object sender, EventArgs e)
        {
            var getContactFilters = App.Database.GetContactFilters();
            if (getContactFilters != null)
            {
                if (!string.IsNullOrEmpty(searchBar.Text))
                {
                    if (searchBar.Text.Contains(" "))
                    {
                        string[] splitString = searchBar.Text.Split(' ');
                        getContactFilters.FirstName = splitString[0];
                        getContactFilters.LastName = splitString[1];
                    }
                    else
                    {
                        getContactFilters.FirstName = searchBar.Text;
                        getContactFilters.LastName = string.Empty;
                    }
                }
                else
                {
                    getContactFilters.FirstName = string.Empty;
                    getContactFilters.LastName = string.Empty;
                }
                App.Database.SaveContactFilters(getContactFilters);
            }
            await Application.Current.MainPage.Navigation.PushAsync(new AdvancedContactSearch(searchBar.Text));
        }
    }
}