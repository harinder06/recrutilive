﻿using Recruitlive.Models;
using Recruitlive.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CandidateDetailView : ContentPage
    {
        double height = App.ScreenHeight;
        double width = App.ScreenWidth;
        public int candidateID;
        public static CandidateSearchPost _SearchedData;

        public CandidateDetailView(int candidateId, CandidateSearchPost _data)
        {
            InitializeComponent();

            candidateID = candidateId;
            _SearchedData = _data;


            //platform specific code

            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");


                //firstLayout.RowSpacing = -20;

                // as per width of screen code
                if (width >= 740)
                {

                    firstframetextSpacing.RowSpacing = 10;

                    firstButton.WidthRequest = width / 2-50;

                    secondButton.WidthRequest = width / 2 - 50;

                    user_Icon.HeightRequest = 150;

                    user_Icon.WidthRequest = 150;

                    user_Icon.Aspect = Aspect.AspectFit;

                    //text size adjustment as per Ipad

                    firstnameTxt.FontSize = 22;
                    lastEmployerFirstTxt.FontSize = 18;
                    lastemployerSecondTxt.FontSize = 18;
                    statusTxt.FontSize = 18;
                    idTxt.FontSize = 18;
                    industrialTxt.FontSize = 18;
                    statusImg.HeightRequest = 10;
                    statusImg.WidthRequest = 10;
                }
                else
                {

                    firstButton.WidthRequest = width / 2 - 20;

                    secondButton.WidthRequest = width / 2 - 20;

                }


            }

            else
            {
                if (width >= 1000)
                {

                    firstButton.WidthRequest = width / 4 - 20;

                    secondButton.WidthRequest = width / 4 - 20;

                    user_Icon.Aspect = Aspect.AspectFit;

                }
                else
                {

                    firstButton.WidthRequest = width / 2 - 20;

                    secondButton.WidthRequest = width / 2 - 20;

                }


            }


            // toolbar icon code

                ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () => {
                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

            this.Title = "Candidate Detail";


            candidateDetails.BindingContext = new CandidateDetailsViewModel(candidateID, Navigation);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;
        }
    }
}