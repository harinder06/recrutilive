﻿using Recruitlive.Models;
using Recruitlive.Models.Candidate;
using Recruitlive.Repo;
using Recruitlive.Views.Candidate;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.RangeSlider.Common;
using Xamarin.RangeSlider.Forms;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CandidateAdvancedSearchBar1 : ContentPage
    {
        HttpClientBase _base = new HttpClientBase();
        public static List<string> SelectedRoles = new List<string>();
        public static List<string> SelectedSkills = new List<string>();
        public Dictionary<List<CandidateTypes>, HttpResponseMessage> result;
        public Dictionary<List<CandidateTypes>, HttpResponseMessage> resultStatus;
        public static List<CandidateTypes> _listTypes = new List<CandidateTypes>();
        public static List<CandidateTypes> _listStatus = new List<CandidateTypes>();
        public static ObservableCollection<string> _loadTypeData = new ObservableCollection<string>();
        public static ObservableCollection<string> _loadStatusData = new ObservableCollection<string>();
        public static string getRoles;
        public static string getSkills;
        static string getStatus = string.Empty;
        static string getType = string.Empty;
        public static CandidateSearchPost _postData = new CandidateSearchPost();
        public bool IsApplied = false;
        string selectedStatusVal = string.Empty;
        string selectedTypeVal = string.Empty;
        public CandidateFilters getCandidateFilters = App.Database.GetCandidateFilters();
        public static string lowerValue = string.Empty;
        public static string upperValue = string.Empty;
        public static string advanceFirstNameTxt;
        public CandidateAdvancedSearchBar1(string searchedText)
        {
            try
            {
                IsApplied = false;

                SelectedRoles = new List<string>();
                SelectedSkills = new List<string>();
                getRoles = string.Empty;
                getSkills = string.Empty;
                getStatus = string.Empty;
                getType = string.Empty;
                lowerValue = string.Empty;
                upperValue = string.Empty;

                if (getCandidateFilters != null)
                {
                    // adding the roles data from local storage
                    if (!string.IsNullOrEmpty(getCandidateFilters.AutoCoreRoles))
                    {
                        string[] splitRoleStrings = getCandidateFilters.AutoCoreRoles.Split(',');

                        if (splitRoleStrings.Length > 0)
                        {
                            for (int i = 0; i < splitRoleStrings.Length; i++)
                            {
                                if (!SelectedRoles.Contains(splitRoleStrings[i]))
                                {
                                    SelectedRoles.Add(splitRoleStrings[i]);
                                }
                            }
                        }
                    }
                    // adding the skills data from local storage
                    if (!string.IsNullOrEmpty(getCandidateFilters.AutoCoreSkills))
                    {
                        string[] splitSkillsStrings = getCandidateFilters.AutoCoreSkills.Split(',');

                        if (splitSkillsStrings.Length > 0)
                        {
                            for (int i = 0; i < splitSkillsStrings.Length; i++)
                            {
                                if (!SelectedSkills.Contains(splitSkillsStrings[i]))
                                {
                                    SelectedSkills.Add(splitSkillsStrings[i]);
                                }
                            }
                        }
                    }
                }

                InitializeComponent();
                try
                {
                    #region setting search bar text
                    //auto fill first name text

                    if (string.IsNullOrEmpty(searchedText))
                    {
                        firstName.Text = string.Empty;

                        lastName.Text = string.Empty;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(searchedText))
                        {
                            if (searchedText.Contains(" "))
                            {
                                firstName.Text = searchedText.Substring(0, searchedText.IndexOf(" "));

                                lastName.Text = searchedText.Substring(searchedText.LastIndexOf(' ') + 1);
                            }
                            else
                            {
                                firstName.Text = searchedText;
                                lastName.Text = string.Empty;
                            }
                        }
                        advanceFirstNameTxt = firstName.Text;
                    }

                    #endregion

                }
                catch (Exception)
                {
                }

                // toolbar visiblity
                ToolbarItems.Add(new ToolbarItem("Apply", "applyicon.png", async () =>
                {
                    var returnMessage = CheckApplyValidation();
                    if (!string.IsNullOrEmpty(returnMessage))
                    {
                        await App.Current.MainPage.DisplayAlert("Recruitlive", returnMessage, "OK");
                        return;
                    }
                    ApplyMethod();
                }));

                this.Title = "Candidate";


                //range slider code
                RangeSlider.FormatLabel = FormaLabel;
                RangeSlider.LowerValueChanged += RangeSlider_LowerValueChanged;
                RangeSlider.UpperValueChanged += RangeSlider_UpperValueChanged;
                RangeSlider.DragStarted += RangeSlider_DragStarted;

                // calling the type and status data method to set data in the dropdowns
                GetTypesStatus();

                //platform specific code
                if (Device.RuntimePlatform == Device.iOS)
                {
                    NavigationPage.SetBackButtonTitle(this, "");

                    RangeSlider.BarHeight = 50;

                    RangeSlider.HeightRequest = 150;
                }

                #region Setting Candidate Filters

                if (getCandidateFilters != null)
                {
                    //setting all the data in all the text boxes that is get from the local database of the candidate filters.
                    if (!string.IsNullOrEmpty(getCandidateFilters.FirstName) && string.IsNullOrEmpty(advanceFirstNameTxt))
                    {
                        firstName.Text = getCandidateFilters.FirstName; // First Name TextBox
                    }
                    if (!string.IsNullOrEmpty(getCandidateFilters.LastName))
                    {
                        lastName.Text = getCandidateFilters.LastName;
                    }
                    if (!string.IsNullOrEmpty(getCandidateFilters.Street))
                        street.Text = getCandidateFilters.Street; // Street TextBox
                    if (!string.IsNullOrEmpty(getCandidateFilters.PostCode))
                        postCode.Text = getCandidateFilters.PostCode; //PostCode TextBox
                    if (!string.IsNullOrEmpty(getCandidateFilters.AutoStatus))
                    {
                        labelPicker.Text = getCandidateFilters.AutoStatus; //AutoStatus TextBox
                        getStatus = getCandidateFilters.AutoStatus;
                    }
                    if (!string.IsNullOrEmpty(getCandidateFilters.AutoType))
                    {
                        labelPickerType.Text = getCandidateFilters.AutoType; //AutoType TextBox  
                        getType = getCandidateFilters.AutoType;
                    }
                    if (!string.IsNullOrEmpty(getCandidateFilters.CurrentEmployer))
                        employer.Text = getCandidateFilters.CurrentEmployer; // Employer Text Box
                    if (!string.IsNullOrEmpty(getCandidateFilters.City))
                        city.Text = getCandidateFilters.City; // City Text Box
                    if (!string.IsNullOrEmpty(getCandidateFilters.Sector))
                        sector.Text = getCandidateFilters.Sector; // Sector Text Box
                    if (!string.IsNullOrEmpty(getCandidateFilters.CandidateId))
                        id.Text = getCandidateFilters.CandidateId; // Id Text Box
                    if (getCandidateFilters.MinSalary != 0)
                        RangeSlider.LowerValue = getCandidateFilters.MinSalary; // Slider Minimum Value
                    if (getCandidateFilters.MaxSalary != 0)
                        RangeSlider.UpperValue = getCandidateFilters.MaxSalary; // Slider Maximum Value
                    if (!string.IsNullOrEmpty(getCandidateFilters.AutoCoreSkills))
                        getSkills = getCandidateFilters.AutoCoreSkills;
                    if (!string.IsNullOrEmpty(getCandidateFilters.AutoCoreRoles))
                        getRoles = getCandidateFilters.AutoCoreRoles;
                }
                #endregion
            }
            catch (Exception)
            {

            }
        }

        // range slider label format method
        private string FormaLabel(Thumb thumb, float val)
        {
            if (thumb == Thumb.Lower)
            {
                return "Min.Sal $" + (val);
            }
            else
            {

                return "Max.Sal $" + (val);
            }
        }

        /// <summary>
        /// Method for checking the apply button validation if all the values are null then it give the apply button validation message.
        /// </summary>
        /// <returns></returns>
        public string CheckApplyValidation()
        {
            string msg = string.Empty;
            var _lowerValue = string.IsNullOrEmpty(lowerValue) ? "0" : RangeSlider.LowerValue.ToString();
            var _upperValue = string.IsNullOrEmpty(upperValue) ? "0" : RangeSlider.UpperValue.ToString();
            if (string.IsNullOrEmpty(firstName.Text) && string.IsNullOrEmpty(lastName.Text) && string.IsNullOrEmpty(getStatus)
                && string.IsNullOrEmpty(getType) && string.IsNullOrEmpty(street.Text) && string.IsNullOrEmpty(postCode.Text)
                && string.IsNullOrEmpty(employer.Text) && string.IsNullOrEmpty(id.Text)
                && string.IsNullOrEmpty(getRoles) && string.IsNullOrEmpty(getSkills)
                && _lowerValue == "0" && _upperValue == "0" && string.IsNullOrEmpty(city.Text) && string.IsNullOrEmpty(sector.Text))
            {
                msg = "Please select filters first before applying";
            }
            return msg;
        }


        /// <summary>
        /// method that will use to save all the filters into the local database and to set all the 
        /// filters in the object to pass this object on the search candidate page.
        /// </summary>
        public void ApplyMethod()
        {
            IsApplied = true;
            _postData.FirstName = string.IsNullOrEmpty(firstName.Text) ? string.Empty : firstName.Text;
            _postData.LastName = string.IsNullOrEmpty(lastName.Text) ? string.Empty : lastName.Text;

            // status data
            if (labelPicker.Text != "Select a Status")
            {
                selectedStatusVal = pickerStatus.SelectedItem != null ? pickerStatus.SelectedItem.ToString() : string.Empty;
                getStatus = !string.IsNullOrEmpty(selectedStatusVal) ? _listStatus[Convert.ToInt32(pickerStatus.SelectedIndex)].value : string.Empty;
            }

            if (labelPickerType.Text != "Select a Type")
            {
                // type data
                selectedTypeVal = pickerType.SelectedItem != null ? pickerType.SelectedItem.ToString() : string.Empty;
                getType = !string.IsNullOrEmpty(selectedTypeVal) ? _listTypes[Convert.ToInt32(pickerType.SelectedIndex)].value : string.Empty;
            }

            _postData.AutoStatus = !string.IsNullOrEmpty(getStatus) ? getStatus : string.Empty;
            _postData.InternalExternal = !string.IsNullOrEmpty(getType) ? getType : string.Empty;
            _postData.Street = string.IsNullOrEmpty(street.Text) ? string.Empty : street.Text;
            _postData.PostCode = string.IsNullOrEmpty(postCode.Text) ? string.Empty : postCode.Text;
            _postData.CurrentEmployer = string.IsNullOrEmpty(employer.Text) ? string.Empty : employer.Text;
            _postData.ID = string.IsNullOrEmpty(id.Text) ? string.Empty : id.Text;
            _postData.AutoCoreRoles = getRoles;
            _postData.AutoCoreSkills = getSkills;
            _postData.AutoSuburb = string.IsNullOrEmpty(city.Text) ? string.Empty : city.Text;
            _postData.IndustrySectors = string.IsNullOrEmpty(sector.Text) ? string.Empty : sector.Text;
            var _lowerValue = string.IsNullOrEmpty(lowerValue) ? string.Empty : lowerValue;
            var _upperValue = string.IsNullOrEmpty(upperValue) ? string.Empty : upperValue;
            _postData.MinSalary = string.IsNullOrEmpty(_lowerValue) ? string.Empty : _lowerValue.ToString();
            _postData.MaxSalary = string.IsNullOrEmpty(_upperValue) ? string.Empty : _upperValue.ToString();


            var getNavigationStack = Navigation.NavigationStack.ToList();
            var removeCurrentPage = getNavigationStack[getNavigationStack.Count - 1];
            var removeCandidateListPage = getNavigationStack[getNavigationStack.Count - 2];
            if (Device.RuntimePlatform == Device.Android)
            {
                // Clearing the navigation stack
                Navigation.RemovePage(removeCurrentPage);
                Navigation.RemovePage(removeCandidateListPage);
                Application.Current.MainPage.Navigation.PushAsync(new SearchCandidatePage(_postData, true));
            }

            else
            {
                Navigation.InsertPageBefore(new SearchCandidatePage(_postData, true), Navigation.NavigationStack[Navigation.NavigationStack.Count - 1]);
                Navigation.RemovePage(this);
                Navigation.RemovePage(removeCandidateListPage);
            }

        }

        private void RangeSlider_DragStarted(object sender, EventArgs e)
        {
            var slider = sender as RangeSlider;
            upperValue = slider.UpperValue.ToString();
            lowerValue = slider.LowerValue.ToString();
        }

        private void RangeSlider_UpperValueChanged(object sender, EventArgs e)
        {
            var slider = sender as RangeSlider;
            upperValue = slider.UpperValue.ToString();
        }

        private void RangeSlider_LowerValueChanged(object sender, EventArgs e)
        {
            var slider = sender as RangeSlider;
            lowerValue = slider.LowerValue.ToString();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            // passing the data to the search candidate page to set in the search bar
            if (!string.IsNullOrEmpty(firstName.Text) && !string.IsNullOrEmpty(lastName.Text))
            {
                SearchCandidatePage.SearchBartxt = firstName.Text + " " + lastName.Text;
            }
            else if (!string.IsNullOrEmpty(firstName.Text) && string.IsNullOrEmpty(lastName.Text))
            {
                SearchCandidatePage.SearchBartxt = firstName.Text;
            }
            else if (string.IsNullOrEmpty(firstName.Text) && !string.IsNullOrEmpty(lastName.Text))
            {
                SearchCandidatePage.SearchBartxt = " " + lastName.Text;
            }
            else
            {
                SearchCandidatePage.SearchBartxt = "";
            }
            CandidateFilters filterData = new Models.Candidate.CandidateFilters();
            var getLoginDetails = App.Database.GetLoginUser();
            if (!IsApplied)
            {
                // gettimg candidate filters stored in local storage
                filterData = App.Database.GetCandidateFilters();
                if (filterData == null)
                {
                    // svaing the candidate filters for the first time
                    filterData = new CandidateFilters();
                    filterData.FirstName = firstName.Text;
                    filterData.LastName = lastName.Text;
                    filterData.Section = "Candidate";
                    filterData.UserName = getLoginDetails.user_name;
                    filterData.BranchName = getLoginDetails.branch;

                    App.Database.SaveCandidateFilters(filterData);
                }
                else
                {
                    filterData.FirstName = firstName.Text;
                    filterData.LastName = lastName.Text;
                    filterData.PostCode = filterData.PostCode == postCode.Text ? filterData.PostCode : "";
                    filterData.Street = filterData.Street == street.Text ? filterData.Street : "";
                    filterData.CurrentEmployer = filterData.CurrentEmployer == employer.Text ? filterData.CurrentEmployer : "";
                    filterData.CandidateId = filterData.CandidateId == id.Text ? filterData.CandidateId : "";
                    filterData.AutoStatus = filterData.AutoStatus == getStatus ? filterData.AutoStatus : "";
                    filterData.AutoType = filterData.AutoType == getType ? filterData.AutoType : "";
                    filterData.MinSalary = filterData.MinSalary == RangeSlider.LowerValue ? filterData.MinSalary : 1;
                    filterData.MaxSalary = filterData.MaxSalary == RangeSlider.UpperValue ? filterData.MaxSalary : 100;
                    filterData.AutoCoreRoles = string.IsNullOrEmpty(filterData.AutoCoreRoles) ? "" : getRoles;
                    filterData.AutoCoreSkills = string.IsNullOrEmpty(filterData.AutoCoreSkills) ? "" : getSkills;
                    filterData.City = filterData.City == city.Text ? filterData.City : "";
                    filterData.Sector = filterData.Sector == sector.Text ? filterData.Sector : "";
                    filterData.Section = "Candidate";
                    filterData.UserName = getLoginDetails.user_name;
                    filterData.BranchName = getLoginDetails.branch;


                    App.Database.SaveCandidateFilters(filterData);
                }
                SearchCandidatePage.IsHighlighted = true;
            }
            else
            {
                // saving the candidate filters if apply button is clicked
                SaveCandidateFilters(getLoginDetails);
            }


        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (SelectedRoles != null)
            {
                if (SelectedRoles.Count > 0)
                {
                    // displaying roles selected data
                    DynamicRolesLayout.Children.Clear();
                    for (int i = 0; i < SelectedRoles.Count; i++)
                    {
                        DynamicRolesLayout.IsVisible = true;

                        Label _rolesText = new Label();
                        _rolesText.TextColor = Color.White;
                        _rolesText.Text = SelectedRoles[i];
                        _rolesText.FontSize = 15;
                        _rolesText.HorizontalOptions = LayoutOptions.StartAndExpand;
                        _rolesText.VerticalOptions = LayoutOptions.Center;
                        _rolesText.LineBreakMode = LineBreakMode.TailTruncation;

                        Image _rolesImage = new Image();
                        _rolesImage.Source = "check_box_filled.png";
                        _rolesImage.HeightRequest = 15;
                        _rolesImage.WidthRequest = 15;
                        _rolesImage.BindingContext = "1";
                        _rolesImage.HorizontalOptions = LayoutOptions.EndAndExpand;
                        _rolesImage.VerticalOptions = LayoutOptions.Center;

                        StackLayout _rolesHorizontalLayout = new StackLayout();
                        _rolesHorizontalLayout.Orientation = StackOrientation.Horizontal;
                        _rolesHorizontalLayout.Padding = new Thickness(5);
                        _rolesHorizontalLayout.ClassId = SelectedRoles[i];

                        TapGestureRecognizer _tapRolesCross = new TapGestureRecognizer();
                        _rolesHorizontalLayout.GestureRecognizers.Add(_tapRolesCross);
                        _tapRolesCross.Tapped += _tapRolesCross_Tapped;

                        _rolesHorizontalLayout.Children.Add(_rolesText);
                        _rolesHorizontalLayout.Children.Add(_rolesImage);

                        DynamicRolesLayout.Children.Add(_rolesHorizontalLayout);
                    }
                }
                else
                {
                    DynamicRolesLayout.Children.Clear();
                }
            }


            if (SelectedSkills != null)
            {
                // displaying skills selected data
                if (SelectedSkills.Count > 0)
                {
                    DynamicSkillsLayout.Children.Clear();
                    for (int i = 0; i < SelectedSkills.Count; i++)
                    {
                        DynamicSkillsLayout.IsVisible = true;

                        Label _skillsText = new Label();
                        _skillsText.TextColor = Color.White;
                        _skillsText.FontSize = 15;
                        _skillsText.VerticalOptions = LayoutOptions.Center;
                        _skillsText.LineBreakMode = LineBreakMode.TailTruncation;
                        _skillsText.Text = SelectedSkills[i];
                        _skillsText.HorizontalOptions = LayoutOptions.StartAndExpand;

                        Image _skillsImage = new Image();
                        _skillsImage.Source = "check_box_filled.png";
                        _skillsImage.HeightRequest = 15;
                        _skillsImage.WidthRequest = 15;
                        _skillsImage.BindingContext = "1";
                        _skillsImage.HorizontalOptions = LayoutOptions.EndAndExpand;
                        _skillsImage.VerticalOptions = LayoutOptions.Center;

                        StackLayout _skillsHorizontalLayout = new StackLayout();
                        _skillsHorizontalLayout.Orientation = StackOrientation.Horizontal;
                        _skillsHorizontalLayout.Padding = new Thickness(5);
                        _skillsHorizontalLayout.ClassId = SelectedSkills[i];

                        TapGestureRecognizer _tapSkillsCross = new TapGestureRecognizer();
                        _skillsHorizontalLayout.GestureRecognizers.Add(_tapSkillsCross);
                        _tapSkillsCross.Tapped += _tapSkillsCross_Tapped;

                        _skillsHorizontalLayout.Children.Add(_skillsText);
                        _skillsHorizontalLayout.Children.Add(_skillsImage);

                        DynamicSkillsLayout.Children.Add(_skillsHorizontalLayout);
                    }
                }
                else
                {
                    DynamicSkillsLayout.Children.Clear(); // clearing the children of the layout
                }
            }

        }
        /// <summary>
        /// methos will used when the cross icon from the skills filters already selected has been pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _tapSkillsCross_Tapped(object sender, EventArgs e)
        {
            var stackLayout = sender as StackLayout;
            string removeSkills = stackLayout.ClassId;

            var checkedImage = stackLayout.Children.Where(a => a.BindingContext != null).FirstOrDefault();
            var control = checkedImage as Image;

            if (control.BindingContext.ToString() == "0")
            {
                // changing the checkbox image 
                control.Source = "check_box_filled.png";
                // removing the selected data
                SelectedSkills.Add(removeSkills);
                control.BindingContext = "1";
            }
            else
            {
                control.Source = "check_box_empty.png";
                // removing the selected data
                var _Data = SelectedSkills.Where(a => a.Equals(removeSkills)).FirstOrDefault();
                SelectedSkills.Remove(removeSkills);
                control.BindingContext = "0";
            }
            var newArray = SelectedSkills.ToArray();
            getSkills = string.Join(",", newArray);
        }

        /// <summary>
        /// methos will used when the cross icon from the roles filters already selected has been pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _tapRolesCross_Tapped(object sender, EventArgs e)
        {
            var stackLayout = sender as StackLayout;
            string removeRoles = stackLayout.ClassId;
            var checkedImage = stackLayout.Children.Where(a => a.BindingContext != null).FirstOrDefault();
            var control = checkedImage as Image;

            if (control.BindingContext.ToString() == "0")
            {
                // changing the checkbox image 
                control.Source = "check_box_filled.png";
                // removing the selected data
                SelectedRoles.Add(removeRoles);
                control.BindingContext = "1";
            }
            else
            {
                control.Source = "check_box_empty.png";
                // removing the selected data
                var _Data = SelectedRoles.Where(a => a.Equals(removeRoles)).FirstOrDefault();
                SelectedRoles.Remove(removeRoles);
                control.BindingContext = "0";
            }

            var newArray = SelectedRoles.ToArray();
            getRoles = string.Join(",", newArray);
        }

        private async void Roles_Label_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RolesListView());
        }

        /// <summary>
        /// skills text box changed event to get all the skills with matched text from the api.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Skills_Label_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SkillsListView());
        }

        /// <summary>
        /// Status Picker OK Button Pressed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Picker_OkButtonClicked(object sender, EventArgs e)
        {
            labelPicker.Text = Convert.ToString(pickerStatus.SelectedItem);
            getStatus = pickerStatus.SelectedItem.ToString();
        }

        /// <summary>
        /// Method that will to get all the data of the type and status from the api.
        /// </summary>
        public async void GetTypesStatus()
        {
            try
            {
                if (_loadTypeData.Count == 0 && _loadStatusData.Count == 0)
                {
                    // Checking the internet connection whether user is connected to the internet or not.
                    if (_base.CheckConnection())
                    {
                        // displaying the loading circle to the user.
                        await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                        result = await _base.GetTypes(CommonLib.MainUrl + ApiUrl.GetTypes);

                        resultStatus = await _base.GetTypes(CommonLib.MainUrl + ApiUrl.GetStatus);

                        foreach (var item in result)
                        {
                            if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                if (item.Key != null) // if api returns data successfully.
                                {
                                    // Setting all the data on their respective fields.
                                    for (int i = 0; i < item.Key.Count; i++)
                                    {
                                        _loadTypeData.Add(item.Key[i].text);
                                        _listTypes.Add(item.Key[i]);
                                    }

                                    LoaderPopup.CloseAllPopup(); // closing the loading popup.
                                }

                                else //if api is not returning the data.
                                {
                                    CommonLib.ApiNotReturningData();
                                }
                            }
                            else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                            {
                                CommonLib.ApiResponseSlow();
                            }
                            else
                            {
                                CommonLib.Unauthorize();
                            }
                        }

                        foreach (var item1 in resultStatus)
                        {
                            if (item1.Value.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                if (item1.Key != null) // if api returns data successfully.
                                {
                                    //_loadStatusData = new ObservableCollection<string>();
                                    //_listStatus = new List<CandidateTypes>();
                                    // Setting all the data on their respective fields.
                                    for (int i = 0; i < item1.Key.Count; i++)
                                    {
                                        _loadStatusData.Add(item1.Key[i].text);
                                        _listStatus.Add(item1.Key[i]);
                                    }

                                    LoaderPopup.CloseAllPopup(); // closing the loading popup.

                                }

                                else //if api is not returning the data.
                                {
                                    CommonLib.ApiNotReturningData();
                                }
                            }
                            else if (item1.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                            {
                                CommonLib.ApiResponseSlow();
                            }
                            else
                            {
                                CommonLib.Unauthorize();
                            }
                        }

                    }
                }
                try
                {
                    pickerType.ItemsSource = _loadTypeData;
                    // pickerType.SelectedItem = _loadTypeData[0];
                    //pickerType.SelectedIndex = 0;


                    if (getCandidateFilters != null)
                    {
                        if (!string.IsNullOrEmpty(getCandidateFilters.AutoType))
                        {
                            pickerType.SelectedItem = getCandidateFilters.AutoType;
                            pickerType.SelectedIndex = Convert.ToInt32(getCandidateFilters.TypePicker);
                        }
                    }
                }
                catch
                {

                }
                pickerStatus.ItemsSource = _loadStatusData;
                //pickerStatus.SelectedItem = _loadStatusData[0];
                //pickerStatus.SelectedIndex = 0;
                if (getCandidateFilters != null)
                {
                    if (!string.IsNullOrEmpty(getCandidateFilters.AutoStatus))
                    {
                        pickerStatus.SelectedItem = getCandidateFilters.AutoStatus;
                        pickerStatus.SelectedIndex = Convert.ToInt32(getCandidateFilters.StatusPicker);
                    }
                }
            }
            catch (Exception ex)
            {
                LoaderPopup.CloseAllPopup(); // closing the loading popup.
            }
            finally
            {

            }
        }

        // Opening of the status dropdown
        private void statusLabel_Tapped(object sender, EventArgs e)
        {
            pickerStatus.Focus();
            // pickerStatus.IsOpen = !pickerStatus.IsOpen;
        }

        /// <summary>
        /// type picker Ok button pressed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Picker_OkButtonClicked1(object sender, EventArgs e)
        {
            labelPickerType.Text = Convert.ToString(pickerType.SelectedItem);
            getType = pickerType.SelectedItem.ToString();
        }

        private void typeLabel_Tapped(object sender, EventArgs e)
        {
            pickerType.Focus();
            //pickerType.IsOpen = !pickerType.IsOpen;
        }

        /// <summary>
        /// making the list view selection null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Selected_Item(object sender, ItemTappedEventArgs e)
        {
            var listView = sender as ListView;
            if (listView != null)
            {
                listView.SelectedItem = null;
            }
        }

        //below moreTapped invoke visiblity of other layout

        void more_Tapped(object sender, EventArgs e)
        {
            if (employeraNIdlyt.IsVisible == true && RangeSlider.IsVisible == true)
            {
                moreAndLessLabel.Text = "More";
                moreAndLessImage.Source = "more.png";
                employeraNIdlyt.IsVisible = false;
                RangeSlider.IsVisible = false;
            }
            else
            {
                moreAndLessLabel.Text = "Less";
                moreAndLessImage.Source = "less.png";
                employeraNIdlyt.IsVisible = true;
                RangeSlider.IsVisible = true;
            }
        }


        /// <summary>
        /// method that will save all the candidate filters in the local storage
        /// </summary>
        /// <returns></returns>
        public bool SaveCandidateFilters(SaveLoginResponse getLoginDetails)
        {
            //Getting the logged in user

            bool result = false;
            try
            {
                // Saving the filters into the local database.

                #region Saving Details In Local Database

                CandidateFilters _obj = new CandidateFilters();

                _obj.FirstName = firstName.Text;
                _obj.LastName = lastName.Text;
                _obj.AutoCoreRoles = getRoles;
                _obj.AutoCoreSkills = getSkills;

                if (labelPicker.Text != "Select a Status")
                {
                    selectedStatusVal = pickerStatus.SelectedItem != null ? pickerStatus.SelectedItem.ToString() : string.Empty;
                }

                if (labelPickerType.Text != "Select a Type")
                {
                    // type data
                    selectedTypeVal = pickerType.SelectedItem != null ? pickerType.SelectedItem.ToString() : string.Empty;
                }

                _obj.AutoStatus = selectedStatusVal;
                _obj.AutoType = selectedTypeVal;
                if (pickerType.SelectedIndex != -1)
                    _obj.TypePicker = pickerType.SelectedIndex.ToString();
                if (pickerStatus.SelectedIndex != -1)
                    _obj.StatusPicker = pickerStatus.SelectedIndex.ToString();
                _obj.MinSalary = RangeSlider.LowerValue;
                _obj.MaxSalary = RangeSlider.UpperValue;
                _obj.PostCode = postCode.Text;
                _obj.CurrentEmployer = employer.Text;
                _obj.Sector = sector.Text;
                _obj.City = city.Text;
                _obj.CandidateId = id.Text;
                _obj.Street = street.Text;
                _obj.Section = "Candidate";
                _obj.UserName = getLoginDetails.user_name;
                _obj.BranchName = getLoginDetails.branch;

                IsApplied = true;

                // saving the deatils into the database.
                App.Database.SaveCandidateFilters(_obj);
                #endregion
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            return result;

        }
    }
}