﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Recruitlive.ViewModels.Candidate;

namespace Recruitlive.Views
{
    public partial class CvPopup : PopupPage
    {

        double height = App.ScreenHeight;
        double width = App.ScreenWidth;
        string _screenName = string.Empty;
        int _resumeId;

        public CvPopup(string resumeId,string ScreenName)
        {
            _resumeId = Convert.ToInt32(resumeId);
            _screenName = ScreenName;
            InitializeComponent();

                FrameContainer.HeightRequest = (height / 3) - 50;
                FrameContainer.WidthRequest = (width / 2) + 100;

                LayoutContainer.HeightRequest = (height / 3) - 80;
                LayoutContainer.WidthRequest = (width / 2) + 100;

                DoneButton.WidthRequest = (width / 3) - 70;

                BackButton.WidthRequest = (width / 3) - 70;
                       
        }

        // popup Close method

        private async void OnClose(object sender, EventArgs e)
        {
            await PopupNavigation.PopAllAsync();
        }

        /// <summary>
        /// performs action when the yes button from the cv popup pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void OnYesTapped(object sender, EventArgs e)
        {
            if (_resumeId > 0)
            {
                BindingContext = new CvDownloadViewModel(_resumeId,_screenName);
            }
            else {
                await Application.Current.MainPage.DisplayAlert("Recruitlive", "No resume found for this candidate.", "OK");
                await PopupNavigation.PopAllAsync();
            }

        }


    }
}