﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Recruitlive.Views.Candidate
{
    public partial class SkillsListView : ContentPage
    {
        public SkillsListView()
        {
            InitializeComponent();

            this.Title = "Select Skills";

            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () =>
            {
                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

            BindingContext = new SkillsListViewModel();
        }

        void Search_Bar_TextChanged(object sender, TextChangedEventArgs e)
        {
            var bindingContext = (SkillsListViewModel)BindingContext;
            bindingContext.SearchedData();
        }

        

        /// <summary>
        /// making the list view selection null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Selected_Item(object sender, ItemTappedEventArgs e)
        {
            var listView = sender as ListView;
            if (listView != null)
            {
                listView.SelectedItem = null;
            }
        }
    }
}
