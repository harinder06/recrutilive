﻿using Recruitlive.Models;
using Recruitlive.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Recruitlive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchCandidatePage : ContentPage
    {
        double screenHeight, screenWidth;
        public static string SearchBartxt;
        public static CandidateSearchPost _postData;
        public static bool checkVal;
        public static bool IsHighlighted = false;

        /// <summary>
        /// Constructor declaration
        /// </summary>
        /// <param name="_data"></param>
        /// <param name="check"></param>
        public SearchCandidatePage(CandidateSearchPost _data, bool check)
        {
            InitializeComponent();
            _postData = _data;
            checkVal = check;
            screenHeight = App.ScreenHeight;
            screenWidth = App.ScreenWidth;
            this.Title = "Candidate";

            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () =>
            {
                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

            // device platform specific code 

            if (Device.RuntimePlatform == Device.iOS)
            {
                NavigationPage.SetBackButtonTitle(this, "");
                if (screenWidth >= 740)
                {
                    searchBar.WidthRequest = screenWidth - 150;
                }
                else
                {
                    searchBar.WidthRequest = 300;
                }

                searchBarFrame.HeightRequest = 70;
                searchBarFrame.Padding = new Thickness(0, 0, 0, 0);
                searchBar.Margin = new Thickness(0, 0, 0, 0);

                advanceFilterIcon.Margin = new Thickness(0, 0, 10, 0);

                advanceFilterIcon.HeightRequest = 30;

                advanceFilterIcon.WidthRequest = 30;

                stacklayoutBarFrame.HeightRequest = 70;

                stacklayoutBarFrame.Padding = new Thickness(0, 0, 0, 0);

                // searchBar.Margin

                candidateList.Margin = new Thickness(0, 10, 0, 10);


            }

            if (_postData == null)
            {
                BindingContext = new CandidateListViewModel(null, checkVal);
            }
            else
            {
                BindingContext = new CandidateListViewModel(_postData, checkVal);
            }
        }


        /// <summary>
        /// method runs when the page appeared on the screen
        /// </summary>
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (IsHighlighted == true)
            {
                var bindingContext = (CandidateListViewModel)BindingContext;
                bindingContext.CheckIsHighLighted(IsHighlighted);
                IsHighlighted = false;
            }

            if (!string.IsNullOrEmpty(SearchBartxt))
            {
                searchBar.Text = SearchBartxt;
            }
            else
            {
                searchBar.Text = string.Empty;
            }
        }

        /// <summary>
        /// method that will perform the actions when the search button from the search bar pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Search_Button_Pressed(object sender, EventArgs e)
        {
            var searchBar = (SearchBar)sender;

            var getCandidateFilters = App.Database.GetCandidateFilters();

            if (getCandidateFilters != null)
            {
                if (_postData != null)
                {
                    var checkName = _postData.FirstName + " " + _postData.LastName;
                    if (checkName != searchBar.Text)
                    {
                        getCandidateFilters.AutoCoreRoles = string.Empty;
                        getCandidateFilters.AutoCoreSkills = string.Empty;
                        getCandidateFilters.AutoStatus = string.Empty;
                        getCandidateFilters.AutoType = string.Empty;
                        getCandidateFilters.CandidateId = string.Empty;
                        getCandidateFilters.CurrentEmployer = string.Empty;
                        getCandidateFilters.MaxSalary = 0;
                        getCandidateFilters.MinSalary = 0;
                        getCandidateFilters.PostCode = string.Empty;
                        getCandidateFilters.StatusPicker = string.Empty;
                        getCandidateFilters.TypePicker = string.Empty;
                        getCandidateFilters.City = string.Empty;
                        getCandidateFilters.Sector = string.Empty;
                    }
                }
                App.Database.SaveCandidateFilters(getCandidateFilters);
            }



            _postData = new CandidateSearchPost();
            if (searchBar.Text.Contains(" "))
            {
                string[] splitString = searchBar.Text.Split(' ');
                _postData.FirstName = splitString[0];
                _postData.LastName = splitString[1];
            }
            else
            {
                _postData.FirstName = searchBar.Text;
            }

            // setting the binding context
            BindingContext = new CandidateListViewModel(_postData, false);
            searchedText.IsVisible = true;
            SearchBartxt = searchBar.Text;
        }

        //below tapped push to advanced search page
        async void advancedCandidateFltr_Tapped(object sender, EventArgs e)
        {
            var getCandidateFilters = App.Database.GetCandidateFilters();
            if (getCandidateFilters != null)
            {
                if (!string.IsNullOrEmpty(searchBar.Text))
                {
                    if (searchBar.Text.Contains(" "))
                    {
                        string[] splitString = searchBar.Text.Split(' ');
                        getCandidateFilters.FirstName = splitString[0];
                        getCandidateFilters.LastName = splitString[1];
                    }
                    else
                    {
                        getCandidateFilters.FirstName = searchBar.Text;
                        getCandidateFilters.LastName = string.Empty;
                    }
                }
                else
                {
                    getCandidateFilters.FirstName = string.Empty;
                    getCandidateFilters.LastName = string.Empty;
                }
                App.Database.SaveCandidateFilters(getCandidateFilters);
            }
            await Application.Current.MainPage.Navigation.PushAsync(new CandidateAdvancedSearchBar1(searchBar.Text));
        }


        void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }

            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;
        }

        /// <summary>
        /// List view item tapped event which will call the candidate detail page based on the candidate id.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selecteditem = e.Item as CandidateListModel;
            var splitCandidateId = selecteditem.emmployeeID.Split(' ');
            var candidateId = Convert.ToInt32(splitCandidateId[1]);
            Navigation.PushAsync(new CandidateDetailView(candidateId, _postData));
        }
    }
}