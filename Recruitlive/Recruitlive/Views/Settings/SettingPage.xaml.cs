﻿using Recruitlive.Models.Settings;
using Recruitlive.Repo;
using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Recruitlive.Views
{
    public partial class SettingPage : ContentPage
    {
        SaveSettings _getsettings = App.Database.GetSettings();
        double height = App.ScreenHeight;
        double width = App.ScreenWidth;
        public SettingPage()
        {
            InitializeComponent();

            this.Title = "Settings";

            if (Device.RuntimePlatform == Device.iOS)
            {
                if (width >= 740)
                {
                    DoneButton.WidthRequest = width / 3;
                }
                else
                {
                    DoneButton.WidthRequest = width / 3;
                }
            }

            else
            {
            }



            ToolbarItems.Add(new ToolbarItem("Info", "ic_home.png", async () => {

                await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
            }));

            InitializeControls();
        }


        // below code text editor
        void InitializeControls()
        {
            txtDesc.Text = string.Empty;

            if (!string.IsNullOrEmpty(_getsettings.Url))
            {
                txtDesc.Text = _getsettings.Url;
            }
            else
            {
                txtDesc.Text = "Enter URL..";
            }
            //  txtDesc.FontAttributes = FontAttributes.Bold;
            txtDesc.FontSize = 14;
            txtDesc.TextColor = Color.Gray;
        }

        private void NotesEditor_Focused(object sender, FocusEventArgs e) //triggered when the user taps on the Editor to interact with it
        {
            if (txtDesc.Text.Equals("Enter URL..")) //if you have the placeholder showing, erase it and set the text color to black
            {
                txtDesc.FontSize = 14;
                // txtDesc.FontAttributes = FontAttributes.Bold;
                txtDesc.TextColor = Color.Gray;
                txtDesc.Text = "";
                txtDesc.TextColor = Color.Black;

            }
        }

        private void NotesEditor_Unfocused(object sender, FocusEventArgs e) //triggered when the user taps "Done" or outside of the Editor to finish the editing
        {
            if (txtDesc.Text.Equals("")) //if there is text there, leave it, if the user erased everything, put the placeholder Text back and set the TextColor to gray
            {

                txtDesc.FontSize = 14;
                //txtDesc.FontAttributes = FontAttributes.Bold;
                txtDesc.TextColor = Color.Black;
                txtDesc.Text = "Enter URL..";
                txtDesc.TextColor = Color.Gray;
            }
        }


        async void Done_Button_Clicked(object sender, EventArgs e)
        {
            if (txtDesc.Text== "Enter URL..")
            {
                await App.Current.MainPage.DisplayAlert("Recruitlive", "Please Enter Url", "OK");
                return;
            }
            if(!CommonLib.CheckValidUrl(txtDesc.Text))
            {
                await App.Current.MainPage.DisplayAlert("Recruitlive", "Please Enter Valid Url", "OK");
                return;
            }
            SaveSettings _obj = new SaveSettings();
            _obj.Url = txtDesc.Text;
            var status = App.Database.SaveSettings(_obj);
            if (status == 1)
            {
                await App.Current.MainPage.DisplayAlert("Recruitlive", "Settings Saved Successfully", "OK");
                CommonLib.MainUrl = _obj.Url+"/";
                await Navigation.PushAsync(new HomePage());
            }
            else
            {
                await App.Current.MainPage.DisplayAlert("Recruitlive", "Something went wrong, Please try again later.", "OK");
            }
        }
    }
}
