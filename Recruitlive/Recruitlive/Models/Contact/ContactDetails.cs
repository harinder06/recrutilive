﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Recruitlive.Models
{

    public class ContactDetails
    {
        [JsonProperty(PropertyName = "$id")]
        public string IdProperty { get; set; }
        public List<Address> addresses { get; set; }
        public string contactName { get; set; }
        public int contactID { get; set; }
        public int clientID { get; set; }
        public string status { get; set; }
        public string jobTitle { get; set; }
        public string skills { get; set; }
        public string clientName { get; set; }
        public string sector { get; set; }
        public string phone { get; set; }
        public string category { get; set; }
    }
}
