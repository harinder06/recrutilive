﻿using SQLite;

namespace Recruitlive.Models
{
    /// <summary>
    /// class that will used to save all the information in the local database for the contact filters.
    /// </summary>
    public class ContactFilters
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AutoCoreSkills { get; set; }
        public string AutoCoreRoles { get; set; }
        public string Client { get; set; }
        public string AutoSuburb { get; set; }
        public string PostCode { get; set; }
        public string Street { get; set; }
        public string Email { get; set; }
        public string BranchName { get; set; }
        public string UserName { get; set; }
        public string Section { get; set; }
    }
}
