﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Recruitlive.Models.Contact
{
    /// <summary>
    /// Class that will used for posting the contact search json data.
    /// </summary>
    public class ContactSearchPost
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Client { get; set; }
        public string ClientValue { get; set; }
        public string AutoCoreSkills { get; set; }
        public string AutoCoreRoles { get; set; }
        public string PostCode { get; set; }
        public string AutoSuburb { get; set; }
        public string Street { get; set; }
        public Email Email { get; set; }
    }

    public class Email
    {
        public string Details { get; set; }
    }

    /// <summary>
    /// Class that will used for getting the deatils of the searched contacts from the api.
    /// </summary>
    public class ContactRecord
    {
        public int contactID { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public DateTime lastUpdatedDate { get; set; }
        public string email { get; set; }

        public string position { get; set; }
    }

    /// <summary>
    /// main class that will deserialize the search contact object.
    /// </summary>
    public class ContactSearchModel
    {
        public List<ContactRecord> records { get; set; }
        public int listID { get; set; }
        public int totalRecords { get; set; }
    }
}
