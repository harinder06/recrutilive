﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Recruitlive.Models
{
    /// <summary>
    /// class that will use to bind all the data of the job applicants into list view.
    /// </summary>
    public class JobApplicantModel : INotifyPropertyChanged
    {
        public bool emailhide { get; set; }
        public string applicantName { get; set; }
        public string email { get; set; }
        public string applicantID { get; set; }
        public bool phonehide { get; set; }
        public string phone { get; set; }
        public string resumeId { get; set; }
        public string date { get; set; }


        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }


    public class JobApplicantGetData
    {
        public string jobApplicantID { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public DateTime createdDate { get; set; }
        public string cv { get; set; }
    }


    public class JobApplicantPostData
    {
        public string AttachmentType { get; set; }
        public string JobReference { get; set; }
        public string ModuleName { get; set; }
    }
}
