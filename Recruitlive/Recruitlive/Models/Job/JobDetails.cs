﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Recruitlive.Models
{
    /// <summary>
    /// Class that will be used for getting all the details for the job from the api. it will desearilize the api
    /// return object.
    /// </summary>
    public class JobDetails
    {
        public int clientID { get; set; }
        public string jobName { get; set; }
        public string client { get; set; }
        public string clientType { get; set; }
        public string status { get; set; }
        public string jobCode { get; set; }
        public string sector { get; set; }
        public string skills { get; set; }
        public string roles { get; set; }
        public string phone { get; set; }
        public int contactID { get; set; }
        public string accountManager { get; set; }
        public string contactName { get; set; }
    }
}
