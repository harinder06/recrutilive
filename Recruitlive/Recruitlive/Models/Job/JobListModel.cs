﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Recruitlive.Models
{
    /// <summary>
    /// Class that will use to set binding of the serach job list view returning from the api.
    /// </summary>
    public class JobListModel: INotifyPropertyChanged
    {
        public string userName { get; set; }
        public string jobCode { get; set; }
        public string jobID { get; set; }
        public string jobTitle { get; set; }
        public string date { get; set; }
        public string phonenumber { get; set; }
        public string email { get; set; }
        public bool phonenumberhide { get; set; }
        public bool emailhide { get; set; }


        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }
}