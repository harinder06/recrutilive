﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitlive.Models.Job
{
    /// <summary>
    /// Class that will used for getting the deatils of the searched clients from the api.
    /// </summary>
    public class JobRecord
    {
        public int jobID { get; set; }
        public string jobCode { get; set; }
        public string name { get; set; }
        public string clientName { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public DateTime lastUpdatedDate { get; set; }
    }

    public class JobSearchModel
    {
        public List<JobRecord> records { get; set; }
        public int listID { get; set; }
        public int totalRecords { get; set; }
    }
}
