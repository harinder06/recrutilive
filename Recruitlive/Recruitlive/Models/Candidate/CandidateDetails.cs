﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace Recruitlive.Models
{
    /// <summary>
    /// Class for getting all the details for the candidate
    /// </summary>
    public class CandidateDetails
    {
        public int candidateID { get; set; }
        public string contractType { get; set; }
        public string status { get; set; }
        public string employer { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public List<Address> addresses { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string candidateProfile { get; set; }
        public string coreRole { get; set; }
        public int resumeID { get; set; }
    }

    /// <summary>
    /// Class that is used in the candidate details for fetching the list of the addresses
    /// </summary>
    public class Address
    {
        public string street1 { get; set; }
        public string autoSuburb { get; set; }
        public string region { get; set; }
        public string postCode { get; set; }
        public string type { get; set; }
    }

    public class BindData : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public string Value { get; set; }

        private string _imgCheckBox;

        public string imgCheckBox
        {
            get { return _imgCheckBox; }
            set
            {
                _imgCheckBox = value;

                OnPropertyChanged(); // Notify, that Image has been changed
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
