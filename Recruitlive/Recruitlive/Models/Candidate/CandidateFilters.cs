﻿using SQLite;

namespace Recruitlive.Models.Candidate
{
    /// <summary>
    /// class that will used to save all the information in the local database for the candidate filters.
    /// </summary>
    public class CandidateFilters
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AutoCoreSkills { get; set; }
        public string AutoCoreRoles { get; set; }
        public string StatusPicker { get; set; }
        public string TypePicker { get; set; }
        public string PostCode { get; set; }
        public string AutoStatus { get; set; }
        public string AutoType { get; set; }
        public string CurrentEmployer { get; set; }
        public string Sector { get; set; }
        public string City { get; set; }
        public string CandidateId { get; set; }
        public string Street { get; set; }
        public float MinSalary { get; set; }
        public float MaxSalary { get; set; }
        public string BranchName { get; set; }
        public string UserName { get; set; }
        public string Section { get; set; }
    }
}
