﻿using Newtonsoft.Json;

namespace Recruitlive.Models
{
    /// <summary>
    /// Class that will used for getting the types data, status data in the candidate filters section.
    /// </summary>
    public class CandidateTypes
    {
        public string text { get; set; }
        public string value { get; set; }
    }
}
