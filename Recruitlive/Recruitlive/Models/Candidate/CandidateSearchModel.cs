﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Recruitlive.Models
{
    /// <summary>
    /// Class that will use to get the result for the candidate search data from the api.
    /// </summary>
    public class Record
    {
        public int candidateID { get; set; }
        public string candidateName { get; set; }
        public DateTime createdDate { get; set; }
        public string mobile { get; set; }
        public string emailID { get; set; }
        public string candidateProfile { get; set; }

    }
    /// <summary>
    /// class that will used to get all the response from the candidate search api.
    /// </summary>
    public class CandidateSearchModel
    {
        public List<Record> records { get; set; }
        public int listID { get; set; }
        public int totalRecords { get; set; }
    }

    /// <summary>
    /// Class that will use to post the json class data when the searching of the candidate performs.
    /// </summary>
    public class CandidateSearchPost
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AutoSuburb { get; set; }
        public string AutoCoreSkills { get; set; }
        public string AutoCoreRoles { get; set; }
        public string PostCode { get; set; }
        public string AutoState { get; set; }
        public string AutoStatus { get; set; }
        public string InternalExternal { get; set; }
        public string IndustrySectors { get; set; }
        public string CurrentEmployer { get; set; }
        public string ID { get; set; }
        public string Street { get; set; }
        public string MinSalary { get; set; }
        public string MaxSalary { get; set; }
    }
}
