﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Recruitlive.Models
{
    /// <summary>
    /// Class that will used for the listview binding in the candidate and client section.
    /// </summary>
    public class CandidateListModel : INotifyPropertyChanged
    {
        public string employeeName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string emmployeeID { get; set; }
        public string jobTitle { get; set; }
        public string date { get; set; }
        public string phonenumber { get; set; }
        public string email { get; set; }
        public bool phonenumberhide { get; set; }
        public bool emailhide { get; set; }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }
}
