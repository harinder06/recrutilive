﻿using Newtonsoft.Json;

namespace Recruitlive.Models.Candidate
{
    /// <summary>
    /// Class that will used for getting the roles data in the candidate filters section.
    /// </summary>
    public class CandidateRoles
    {
        public string value { get; set; }
        public string text { get; set; }
    }
}
