﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitlive.Models.Settings
{
    /// <summary>
    /// class that will used to save the settings url into the local database.
    /// </summary>
    public class SaveSettings
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Url { get; set; }
    }
}
