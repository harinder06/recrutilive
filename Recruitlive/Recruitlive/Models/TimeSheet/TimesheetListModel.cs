﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Recruitlive.Models
{
    public class TimesheetListModel : INotifyPropertyChanged
    {
        public string employeeName { get; set; }
        public string emmployeeID { get; set; }
        public string date { get; set; }
        public string CommentText { get; set; }

        public string dateSubmitted { get; set; }
        public string workedHours { get; set; }
        public string breakTime { get; set; }
        public string timesheetTime { get; set; }
        public string leaveType { get; set; }
        public string comments { get; set; }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }
}