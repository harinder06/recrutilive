﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitlive.Models
{
    public class TimeSheetGetData
    {
        public string employeeName { get; set; }
        public int timesheetID { get; set; }
        public DateTime? dateSubmitted { get; set; }
        public string workedHours { get; set; }
        public string date { get; set; }
        public string breakTime { get; set; }
        public string timesheetTime { get; set; }
        public string leaveType { get; set; }
        public string comments { get; set; }
    }
}
