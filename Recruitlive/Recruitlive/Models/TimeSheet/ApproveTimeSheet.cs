﻿namespace Recruitlive.Models
{
    public class ApproveTimeSheet
    {
        public string Comment { get; set; }

        public string TimesheetIds { get; set; }

        public string RouteTo { get; set; }
    }
}
