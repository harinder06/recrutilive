﻿using SQLite;

namespace Recruitlive.Models
{
    public class LoginResponse
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }
    }

    public class SaveLoginResponse
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }
        public string user_name { get; set; }
        public string branch { get; set; }
        public string company_name { get; set; }
    }
}
