﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Recruitlive.Models
{
    /// <summary>
    /// Class that will used for performing the binding of the list view fro the notes listing section.
    /// </summary>
    public class NoteListItem : INotifyPropertyChanged
    {
        public int NoteId { get; set; }
        public string Title { get; set; }

        public string SubTitle { get; set; }

        public string PostedBy { get; set; }

        public string PostedOn { get; set; }


        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}
