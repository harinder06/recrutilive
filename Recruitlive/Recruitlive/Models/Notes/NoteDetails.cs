﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Recruitlive.Models
{
    /// <summary>
    /// Class that will use for getting the notes details data from the api
    /// </summary>
    public class NoteDetails
    {
        public DateTime dateMade { get; set; }
        public string content { get; set; }
        public string category { get; set; }       
        public string userID { get; set; }             
    }

    /// <summary>
    /// Class that will used for getting the note list view data from the api.
    /// </summary>
    public class NotesList
    {
        public DateTime dateMade { get; set; }
        public string content { get; set; }
        public string category { get; set; }
        public decimal noteID { get; set; }
        public string userNameBranch { get; set; }
    }

    /// <summary>
    /// Class that will be used to bind all the notes into the list view.
    /// </summary>
    public class NotesMainList
    {
        public List<NotesList> records { get; set; }
        public int totalRecords { get; set; }
    }
}
