﻿namespace Recruitlive.Models
{
    /// <summary>
    /// Class that will used for the passing the json data when we are saving the notes.
    /// </summary>
    public class AddNotesModel
    {
        public string Category { get; set; }
        public string Content { get; set; }        
        public string ModuleName { get; set; }
        public int ModuleRecordID { get; set; }        
    }
}
