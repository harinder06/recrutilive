﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Recruitlive.Models
{
    /// <summary>
    /// Class that will used for the getting the client details for performing the binding.
    /// </summary>
    public class ClientDetails
    {
        public int companyID { get; set; }
        public string companyName { get; set; }
        public string sector { get; set; }
        public string status { get; set; }
        public string skills { get; set; }
        public string category { get; set; }
        public string website { get; set; }
        public string phone { get; set; }
        public List<Address> addresses { get; set; }
        public string email { get; set; }
    }

     
}

