﻿using SQLite;

namespace Recruitlive.Models.Client
{
    /// <summary>
    /// class that will used to save all the information in the local database for the client filters.
    /// </summary>
    public class ClientFilters
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StatusPicker { get; set; }
        public string TypePicker { get; set; }
        public string PostCode { get; set; }
        public string AutoStatus { get; set; }
        public string AutoType { get; set; }
        public string City { get; set; }
        public string CompanyName { get; set; }
        public string Street { get; set; }
        public string BranchName { get; set; }
        public string UserName { get; set; }
        public string Section { get; set; }
    }
}
