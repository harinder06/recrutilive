﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Recruitlive.Models
{
    /// <summary>
    /// Class that will used for posting the client search json data.
    /// </summary>
    public class ClientSearchPost
    {
        public string ClientName { get; set; }
        public string Status { get; set; }
        public string AutoSuburb { get; set; }
        public string PostCode { get; set; }
        public string InternalExternal { get; set; }
        public string Street { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
    }

    /// <summary>
    /// Class that will used for getting the deatils of the searched clients from the api.
    /// </summary>
    public class ClientRecord
    {
        [JsonProperty(PropertyName = "$id")]
        public string IdProperty { get; set; }
        public int clientID { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public DateTime lastUpdatedDate { get; set; }
    }

    /// <summary>
    /// main class that will deserialize the search client object.
    /// </summary>
    public class ClientSearchModel
    {
        [JsonProperty(PropertyName = "$id")]
        public string IdProperty { get; set; }
        public List<ClientRecord> records { get; set; }
        public int listID { get; set; }
        public int totalRecords { get; set; }
    }
}
