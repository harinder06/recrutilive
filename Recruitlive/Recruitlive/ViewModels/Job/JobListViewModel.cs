﻿using Recruitlive.Models;
using Recruitlive.Models.Job;
using Recruitlive.Repo;
using Recruitlive.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Extended;

namespace Recruitlive.ViewModels
{
    /// <summary>
    /// this view model will be used for binding all the data for the job listing.
    /// </summary>
    public class JobListViewModel : INotifyPropertyChanged
    {
        HttpClientBase _base = new HttpClientBase();
        public event PropertyChangedEventHandler PropertyChanged;
        public static int totalcount = 0;
        public bool IsFirstHit = false;
        public static int getEventCount = 0;
        public bool isSorted = false;
        public int pageindex = 1;
        public int pageSize = 5;
        public bool HitinProcess = false;
        public InfiniteScrollCollection<JobListModel> JobList { get; set; }

        /// <summary>
        /// property to show need to loading again or not.
        /// </summary>
        public bool _isLoadingMore;
        public bool IsLoadingMore
        {
            get
            {
                return _isLoadingMore;
            }
            set
            {
                _isLoadingMore = value;
                OnPropertyChanged(nameof(IsLoadingMore));
            }
        }

        /// <summary>
        /// property for the visiability for the no of candidates text
        /// </summary>

        private bool _searchVisibles;
        public bool SearchVisibles
        {
            get
            {
                return _searchVisibles;
            }
            set
            {
                _searchVisibles = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SearchVisibles"));
            }
        }

        /// <summary>
        /// property for the show the no of candidates returning from the api.
        /// </summary>
        private string _count;
        public string Count
        {
            get
            {
                return _count;
            }
            set
            {
                _count = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Count"));
            }
        }

        /// <summary>
        /// property for the getting the List Id.
        /// </summary>
        private string _listId;
        public string ListId
        {
            get
            {
                return _listId;
            }
            set
            {
                _listId = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ListId"));
            }
        }

        /// <summary>
        /// property for the specifying the sortcolumn.
        /// </summary>
        private string _sortColumn;
        public string SortColumn
        {
            get
            {
                return _sortColumn;
            }
            set
            {
                _sortColumn = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SortColumn"));
            }
        }

        /// <summary>
        /// property for the specifying the sortdirection.
        /// </summary>
        private string _sortDirection;
        public string SortDirection
        {
            get
            {
                return _sortDirection;
            }
            set
            {
                _sortDirection = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SortDirection"));
            }
        }

        /// <summary>
        /// property for the giving the textcolor to the sorting labels of first name.
        /// </summary>
        private string _textcolorfirstname;
        public string TextColorFirstName
        {
            get
            {
                return _textcolorfirstname;
            }
            set
            {
                _textcolorfirstname = value;
                PropertyChanged(this, new PropertyChangedEventArgs("TextColorFirstName"));
            }
        }


        /// <summary>
        /// property for the specifying the backgroundcolor for the sorting label of first name.
        /// </summary>
        private Color _backgroundfirstname;
        public Color BackgroundFirstName
        {
            get
            {
                return _backgroundfirstname;
            }
            set
            {
                _backgroundfirstname = value;
                PropertyChanged(this, new PropertyChangedEventArgs("BackgroundFirstName"));
            }
        }


        /// <summary>
        /// property for the giving the textcolor to the sorting labels of job name.
        /// </summary>
        private string _textcolorjobname;
        public string TextColorJobName
        {
            get
            {
                return _textcolorjobname;
            }
            set
            {
                _textcolorjobname = value;
                PropertyChanged(this, new PropertyChangedEventArgs("TextColorJobName"));
            }
        }


        /// <summary>
        /// property for the specifying the backgroundcolor for the sorting label of job name.
        /// </summary>
        private Color _backgroundjobname;
        public Color BackgroundJobName
        {
            get
            {
                return _backgroundjobname;
            }
            set
            {
                _backgroundjobname = value;
                PropertyChanged(this, new PropertyChangedEventArgs("BackgroundJobName"));
            }
        }

        private string _noData;
        public string NoData
        {
            get
            {
                return _noData;
            }
            set
            {
                _noData = value;
                PropertyChanged(this, new PropertyChangedEventArgs("NoData"));
            }
        }

        private bool _noDataVisible;
        public bool NoDataVisible
        {
            get
            {
                return _noDataVisible;
            }
            set
            {
                _noDataVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("NoDataVisible"));
            }
        }

        private bool _sortingVisible;
        public bool SortingVisible
        {
            get
            {
                return _sortingVisible;
            }
            set
            {
                _sortingVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SortingVisible"));
            }
        }

        public JobListViewModel()
        {
            _searchVisibles = false;
            //_postData = _data;
            _sortColumn = "JobName";
            _sortDirection = "2";
            _textcolorfirstname = "Gray";
            _backgroundfirstname = Color.White;
            _textcolorjobname = "Gray";
            _backgroundjobname = Color.White;
            _sortingVisible = true;
            SearchJob();
        }


        /// <summary>
        /// This method will used to bind the api to the view with the data returning from the api.
        /// </summary>
        /// <returns></returns>
        public void SearchJob()
        {
            /// <summary>
            /// This method will used to bind the api to the view with the data returning from the api.
            /// </summary>
            /// <returns></returns>

            JobList = new InfiniteScrollCollection<JobListModel>
            {
                OnLoadMore = async () =>
                {
                    var items = new InfiniteScrollCollection<JobListModel>();

                    // Checking the internet connection whether user is connected to the internet or not.
                    if (_base.CheckConnection()) // internet connection is working
                    {
                        // total no of records not exceed from the current records
                        if (totalcount > getEventCount && getEventCount != 0 || IsFirstHit == false)
                        {
                            if (!HitinProcess)
                            {
                                HitinProcess = true;
                                IsLoadingMore = true;

                                Dictionary<JobSearchModel, HttpResponseMessage> response = null;

                                // if the records are fetched first time
                                if (pageindex == 1 && isSorted == false)
                                {
                                    response = await _base.SearchJob(CommonLib.MainUrl + ApiUrl.JobListing + pageSize);
                                }
                                if (isSorted == false && pageindex != 1)
                                {
                                    // getting api response
                                    response = await _base.JobPagingSorting(CommonLib.MainUrl + ApiUrl.JobListPaging + ListId + "/" + pageSize + "/" + pageindex + "/" + SortColumn + "/" + SortDirection);
                                }
                                if (isSorted == true)
                                {
                                    // getting api response
                                    response = await _base.JobPagingSorting(CommonLib.MainUrl + ApiUrl.JobListPaging + ListId + "/" + pageSize + "/" + pageindex + "/" + SortColumn + "/" + SortDirection);
                                }
                                foreach (var data in response)
                                {
                                    // check the response is ok or not.
                                    if (data.Value.StatusCode == HttpStatusCode.OK)
                                    {
                                        foreach (var item in response.Keys)
                                        {
                                            if (response != null && item.records.Count != 0)
                                            {
                                                LoaderPopup.CloseAllPopup();
                                                SearchVisibles = true;
                                                NoDataVisible = false;
                                                if (item.totalRecords > 1)
                                                {
                                                    Count = item.totalRecords + " Jobs";
                                                }
                                                else
                                                {
                                                    Count = item.totalRecords + " Job";
                                                }

                                                pageindex++;
                                                IsFirstHit = true;
                                                totalcount = item.totalRecords;
                                                getEventCount = item.records.Count;
                                                ListId = item.listID.ToString();
                                                items = GetItems(true, item); // calling method to set data in listview
                                                IsLoadingMore = false;
                                                HitinProcess = false;
                                            }
                                            else
                                            {
                                                LoaderPopup.CloseAllPopup();
                                                if (response != null && item.totalRecords == 0)
                                                {
                                                    NoData = "No Jobs Found";
                                                    SearchVisibles = false;
                                                    NoDataVisible = true;
                                                    SortingVisible = false;
                                                }
                                                else
                                                {
                                                    SearchVisibles = true;
                                                    getEventCount = 0;
                                                    HitinProcess = false;
                                                    IsLoadingMore = false;
                                                    SortingVisible = true;
                                                }
                                            }
                                        }
                                    }
                                    else if (data.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                                    {
                                        CommonLib.ApiResponseSlow();
                                    }
                                    else
                                    {
                                        CommonLib.Unauthorize();
                                    }


                                }
                                IsLoadingMore = false;
                            }
                        }
                    }
                    return items;
                }
            };
            JobList.LoadMoreAsync();
        }


        /// <summary>
        /// Method for setting the data in the list view.
        /// </summary>
        /// <param name="clearList"></param>
        /// <param name="clientItemList"></param>
        /// <returns></returns>
        InfiniteScrollCollection<JobListModel> GetItems(bool clearList, JobSearchModel clientItemList)
        {
            InfiniteScrollCollection<JobListModel> items = null;
            try
            {
                if (clearList || JobList == null)
                {
                    items = new InfiniteScrollCollection<JobListModel>();
                }
                else
                {
                    items = new InfiniteScrollCollection<JobListModel>(JobList);
                }


                // setting all the data into the list view by getting from the api.
                foreach (var clientItem in clientItemList.records)
                {
                    items.Add(new JobListModel
                    {
                        jobTitle = clientItem.name,
                        userName = clientItem.clientName,
                        jobID = clientItem.jobID.ToString(),
                        jobCode = !string.IsNullOrEmpty(clientItem.jobCode) ? clientItem.jobCode : string.Empty,
                        date = clientItem.lastUpdatedDate.ToString("dd MMMM yyyy"),
                        phonenumber = clientItem.phone,
                        email = clientItem.email,
                        phonenumberhide = string.IsNullOrEmpty(clientItem.phone) ? false : true,
                        emailhide = string.IsNullOrEmpty(clientItem.email) ? false : true,
                    });
                }
            }
            catch (Exception)
            {

            }
            return items;
        }



        /// <summary>
        /// this method will used for the client name sorting
        /// </summary>
        public Command clientName_Tapped
        {
            get
            {
                return new Command((e) =>
                {
                    if (JobList != null)
                    {
                        if (_backgroundfirstname == Color.White)
                        {
                            _backgroundfirstname = Color.FromHex("#d4d4d4");
                            _textcolorfirstname = "White";
                            _backgroundjobname = Color.White;
                            _textcolorjobname = "Gray";
                            _sortColumn = "ClientName";
                            _sortDirection = "2";
                        }
                        else
                        {
                            _backgroundfirstname = Color.White;
                            _textcolorfirstname = "Gray";
                            _sortColumn = "ClientName";
                            _sortDirection = "2";
                        }
                        HitinProcess = false;
                        pageindex = 1;
                        pageSize = 15;
                        IsFirstHit = false;
                        isSorted = true;
                        JobList = new InfiniteScrollCollection<JobListModel>();
                        RaisePropertyChanged(nameof(JobList));
                        SearchJob();
                        RaisePropertyChanged(nameof(SortColumn));
                        RaisePropertyChanged(nameof(SortDirection));
                        RaisePropertyChanged(nameof(BackgroundFirstName));
                        RaisePropertyChanged(nameof(TextColorFirstName));
                        RaisePropertyChanged(nameof(BackgroundJobName));
                        RaisePropertyChanged(nameof(TextColorJobName));

                    }
                });
            }
        }

        /// <summary>
        /// this method will used for the job name sorting
        /// </summary>
        public Command jobName_Tapped
        {
            get
            {
                return new Command((e) =>
                {
                    if (JobList != null)
                    {
                        if (_backgroundjobname == Color.White)
                        {
                            _backgroundjobname = Color.FromHex("#d4d4d4");
                            _textcolorjobname = "White";
                            _backgroundfirstname = Color.White;
                            _textcolorfirstname = "Gray";
                            _sortColumn = "JobName";
                            _sortDirection = "1";
                        }
                        else
                        {
                            _backgroundjobname = Color.White;
                            _textcolorjobname = "Gray";
                            _sortColumn = "JobName";
                            _sortDirection = "2";
                        }
                        HitinProcess = false;
                        pageindex = 1;
                        pageSize = 15;
                        IsFirstHit = false;
                        isSorted = true;
                        JobList = new InfiniteScrollCollection<JobListModel>();
                        RaisePropertyChanged(nameof(JobList));
                        SearchJob();

                        RaisePropertyChanged(nameof(SortColumn));
                        RaisePropertyChanged(nameof(SortDirection));
                        RaisePropertyChanged(nameof(BackgroundJobName));
                        RaisePropertyChanged(nameof(TextColorJobName));
                        RaisePropertyChanged(nameof(TextColorFirstName));
                        RaisePropertyChanged(nameof(BackgroundFirstName));
                    }
                });
            }
        }


        // we give command to call and email icon on list cell to invoke call function and functioning on device

        /// <summary>
        /// this method will be used for the calling the phone number.
        /// </summary>
        public Command callCommand
        {
            get
            {
                return new Command((e) =>
                {
                    JobListModel _model = e as JobListModel;
                    if (!string.IsNullOrEmpty(_model.phonenumber))
                    {
                        Device.OpenUri(new Uri(string.Format("tel:{0}", _model.phonenumber)));
                    }
                });
            }
        }

        /// <summary>
        /// this method will be used for performing the email functionality.
        /// </summary>
        public Command emailCommand
        {
            get
            {
                return new Command((e) =>
                {
                    JobListModel _model = e as JobListModel;
                    if (!string.IsNullOrEmpty(_model.email))
                    {
                        Device.OpenUri(new Uri(string.Format("mailto:{0}", _model.email)));

                    }
                });
            }
        }

        public void CollectionDidChange(object sender,
                                NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }



    }
}
