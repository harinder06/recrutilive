﻿using Recruitlive.Models;
using Recruitlive.Models.Job;
using Recruitlive.Repo;
using Recruitlive.Views;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Extended;

namespace Recruitlive.ViewModels
{
    public class JobApplicantViewModel : INotifyPropertyChanged
    {
        HttpClientBase _base = new HttpClientBase();
        public JobApplicantPostData _postData;
        public event PropertyChangedEventHandler PropertyChanged;
        public int pageindex = 1;
        public int pageSize = 7;
        public bool HitinProcess = false;
        public int totalCount = 1;
        public int _jobId = 0;
        public int checkValue = 0;
        public InfiniteScrollCollection<JobApplicantModel> applicantList { get; set; }

        /// <summary>
        /// property to show need to loading again or not.
        /// </summary>
        public bool _isLoadingMore;
        public bool IsLoadingMore
        {
            get
            {
                return _isLoadingMore;
            }
            set
            {
                _isLoadingMore = value;
                OnPropertyChanged(nameof(IsLoadingMore));
            }
        }

        public string _noData;

        public string NoData
        {
            get
            {
                return _noData;
            }
            set
            {
                _noData = value;
                OnPropertyChanged(nameof(NoData));
            }
        }

        INavigation navigation;

        public JobApplicantViewModel(JobApplicantPostData data, INavigation _navigation,int jobId)
        {
            _postData = new JobApplicantPostData();
            _postData = data;
            navigation = _navigation;
            _jobId = jobId;
            BindData();
        }

        /// <summary>
        /// This method will used to bind the api to the view with the data returning from the api.
        /// </summary>
        /// <returns></returns>
        public void BindData()
        {

            applicantList = new InfiniteScrollCollection<JobApplicantModel>
            {
                OnLoadMore = async () =>
                {
                    var items = new InfiniteScrollCollection<JobApplicantModel>();

                    // Checking the internet connection whether user is connected to the internet or not.
                    if (_base.CheckConnection())
                    {
                        if (!HitinProcess)
                        {
                            HitinProcess = true;
                            IsLoadingMore = true;

                            var result = await _base.JobApplicants(CommonLib.MainUrl + ApiUrl.JobApplicantsDetails + pageSize + "/" + pageindex + "/null/DateSubmitted DESC", _postData);

                            foreach (var item in result)
                            {
                                if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    if (item.Key.Count > 0) // if api returns data successfully.
                                    {
                                        foreach (var response in result.Keys)
                                        {
                                            if (response != null && response.Count > 0)
                                            {
                                                pageindex++;
                                                items = GetItems(true, response); // calling method to set data in listview
                                                IsLoadingMore = false;
                                                HitinProcess = false;
                                            }
                                            else
                                            {
                                                HitinProcess = true;
                                                IsLoadingMore = false;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (HitinProcess)
                                        {
                                            NoData = "No Data Found";
                                        }
                                        HitinProcess = true;
                                        IsLoadingMore = false;
                                    }                             
                                }
                                else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                                {
                                    CommonLib.ApiResponseSlow();
                                }
                                else
                                {
                                    CommonLib.Unauthorize();
                                }
                            }
                            IsLoadingMore = false;
                        }
                    }
                    return items;
                }
            };
            applicantList.LoadMoreAsync();
        }

        /// <summary>
        /// Method for setting the data in the list view.
        /// </summary>
        /// <param name="clearList"></param>
        /// <param name="noteItemList"></param>
        /// <returns></returns>
        InfiniteScrollCollection<JobApplicantModel> GetItems(bool clearList, List<JobApplicantGetData> applicantsData)
        {
            InfiniteScrollCollection<JobApplicantModel> items = null;
            try
            {
                if (clearList || applicantList == null)
                {
                    items = new InfiniteScrollCollection<JobApplicantModel>();
                }
                else
                {
                    items = new InfiniteScrollCollection<JobApplicantModel>(applicantList);
                }


                // setting all the data into the list view by getting from the api.
                foreach (var Item in applicantsData)
                {
                    items.Add(new JobApplicantModel
                    {
                        applicantName = Item.name,
                        email=Item.email,
                        phone=Item.phone,
                        phonehide = string.IsNullOrEmpty(Item.phone) ? false : true,
                        emailhide = string.IsNullOrEmpty(Item.email) ? false : true,
                        applicantID = "Id " + Item.jobApplicantID,
                        date = Item.createdDate.ToString("dd MMMM yyyy"),
                        resumeId = Item.cv
                    });
                }
            }
            catch (Exception)
            {

            }
            return items;
        }

        public Command cvCommand
        {
            get
            {
                return new Command((e) =>
                {
                    var _model = e as JobApplicantModel;
                    if (_model != null)
                    {
                        navigation.PushPopupAsync(new CvPopup(_model.resumeId,"Job"));
                    }

                });
            }
        }

        public Command rejectCommand
        {
            get
            {
                return new Command(async (e) =>
                {
                    var _model = e as JobApplicantModel;
                    if (_model != null)
                    {
                        var ans = await Application.Current.MainPage.DisplayAlert("Recruitlive", "Are you sure want to reject the applicant", "Yes", "No");
                        if(ans)
                        {
                            string[] splitId = _model.applicantID.Split(' ');
                            if (splitId.Length > 1)
                            {
                                RejectJobApplicant(_jobId, splitId[1]);
                            }
                        }
                    }

                });
            }
        }


        /// <summary>
        /// this is method that will perform the rejection of the timesheet with the help of the api.
        /// </summary>
        /// <returns></returns>
        public async void RejectJobApplicant(int jobID,string jobApplicantID)
        {
            try
            {
                // Checking the internet connection whether user is connected to the internet or not.
                if (_base.CheckConnection())
                {
                    // displaying the loading circle to the user.
                    await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                    // Calling of the api for getting the result.
                    var result = await _base.GetMethod(CommonLib.MainUrl + ApiUrl.JobApplicantsReject+jobID+"/"+jobApplicantID);
                    if (result == "true") // if api returns data successfully.
                    {
                        LoaderPopup.CloseAllPopup();  // closing the loading popup.
                        await App.Current.MainPage.DisplayAlert("Recruitlive", "Job Applicant Rejected Successfully", "OK");
                        HitinProcess = false;
                        pageindex = 1;
                        BindData();
                    }

                    else
                    {
                        CommonLib.ApiNotReturningData();
                    }
                }
            }
            catch (Exception)
            {
                LoaderPopup.CloseAllPopup();  // closing the loading popup.
            }
            finally
            {

            }
        }


        // we give command to call and email icon on list cell to invoke call function and functioning on device
        public Command callCommand
        {
            get
            {
                return new Command((e) =>
                {
                    JobApplicantModel _model = e as JobApplicantModel;

                    if (!string.IsNullOrEmpty(_model.phone))
                    {
                        Device.OpenUri(new Uri(string.Format("tel:{0}", _model.phone)));
                    }
                });
            }
        }


        public Command emailCommand
        {
            get
            {
                return new Command((e) =>
                {
                    JobApplicantModel _model = e as JobApplicantModel;

                    if (!string.IsNullOrEmpty(_model.email))
                    {
                        Device.OpenUri(new Uri(string.Format("mailto:{0}", _model.email)));
                    }
                });
            }
        }

        public void CollectionDidChange(object sender,
                               NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }



    }
}
