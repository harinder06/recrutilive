﻿using Recruitlive.Models.Job;
using Recruitlive.Repo;
using Recruitlive.Views;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace Recruitlive.ViewModels
{
    public class JobDetailViewModel : INotifyPropertyChanged
    {
        public int jobID;
        HttpClientBase _base = new HttpClientBase();
        public ObservableCollection<RolesList> RolesList { get; set; }

        public ObservableCollection<SkillsList> SkillsList { get; set; }

        /// <summary>
        /// property for the job name field
        /// </summary>
        private string _jobName;
        public string JobName
        {
            get
            {
                return _jobName;
            }
            set
            {
                _jobName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("JobName"));
            }
        }

        /// <summary>
        /// property for the job code field
        /// </summary>
        private string _code;
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Code"));
            }
        }

        /// <summary>
        /// property for the industry field
        /// </summary>

        private string _industry;
        public string Industry
        {
            get
            {
                return _industry;
            }
            set
            {
                _industry = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Industry"));
            }
        }

        /// <summary>
        /// property for the client name field
        /// </summary>
        private string _clientName;
        public string ClientName
        {
            get
            {
                return _clientName;
            }
            set
            {
                _clientName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ClientName"));
            }
        }

        /// <summary>
        /// property for the contact name field
        /// </summary>
        private string _contactName;
        public string ContactName
        {
            get
            {
                return _contactName;
            }
            set
            {
                _contactName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ContactName"));
            }
        }

        /// <summary>
        /// property for the status field
        /// </summary>
        private string _status;
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Status"));
            }
        }

        /// <summary>
        /// property for the account manager field
        /// </summary>
        private string _accountManager;
        public string AccountManager
        {
            get
            {
                return _accountManager;
            }
            set
            {
                _accountManager = value;
                PropertyChanged(this, new PropertyChangedEventArgs("AccountManager"));
            }
        }

        /// <summary>
        /// property for the client type field
        /// </summary>

        private string _clientType;
        public string ClientType
        {
            get
            {
                return _clientType;
            }
            set
            {
                _clientType = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ClientType"));
            }
        }

        /// <summary>
        /// property for the contact field
        /// </summary>

        private string _contact;
        public string Contact
        {
            get
            {
                return _contact;
            }
            set
            {
                _contact = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Contact"));
            }
        }

        /// <summary>
        /// property for the category list view height
        /// </summary>
        private double _rolesListHeight = 0;
        public double RolesListHeight
        {
            get
            {
                return _rolesListHeight;
            }
            set
            {
                _rolesListHeight = value;
                PropertyChanged(this, new PropertyChangedEventArgs("RolesListHeight"));
            }
        }

        /// <summary>
        /// property for the skills list view height
        /// </summary>
        private double _skillsListHeight = 0;
        public double SkillsListHeight
        {
            get
            {
                return _skillsListHeight;
            }
            set
            {
                _skillsListHeight = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SkillsListHeight"));
            }
        }

        /// <summary>
        /// property for the client id
        /// </summary>
        private int _clientId;
        public int ClientId
        {
            get
            {
                return _clientId;
            }
            set
            {
                _clientId = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ClientId"));
            }
        }

        /// <summary>
        /// property for the client id
        /// </summary>
        private int _contactId;
        public int ContactId
        {
            get
            {
                return _contactId;
            }
            set
            {
                _contactId = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ContactId"));
            }
        }

        INavigation navigation;
        public JobDetailViewModel(int jobId, INavigation _navigation)
        {
            jobID = jobId;
            navigation = _navigation;
            BindData();
        }

        /// <summary>
        /// This method will used to bind the api to the view with the data returning from the api.
        /// </summary>
        /// <returns></returns>
        public async void BindData()
        {
            try
            {
                // Checking the internet connection whether user is connected to the internet or not.    
                if (_base.CheckConnection())
                {
                    // displaying the loading circle to the user.
                    await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                    // Calling of the api for getting the result.
                    var result = await _base.GetJobDetails(CommonLib.MainUrl + ApiUrl.JobDetails + jobID);
                    foreach (var item in result)
                    {
                        if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            if (item.Key != null) // if api returns data successfully.
                            {
                                // Setting all the data on their respective fields.
                                JobName = item.Key.jobName;
                                Code = item.Key.jobCode;
                                Industry = item.Key.sector;
                                ClientName = item.Key.client;
                                Contact = item.Key.phone;
                                ContactName = item.Key.contactName;
                                Status = item.Key.status;
                                ContactId = item.Key.contactID;
                                AccountManager = item.Key.accountManager;
                                ClientType = item.Key.clientType;
                                ClientId = item.Key.clientID;
                                string[] autoCoreSkills = item.Key.skills != null ? item.Key.skills.Split(';').Where(a => !string.IsNullOrEmpty(a)).ToArray() : null;
                                string[] autoCoreRoles = item.Key.roles != null ? item.Key.roles.Split(';').Where(a => !string.IsNullOrEmpty(a)).ToArray() : null;

                                var autoCoreRolesItems = new ObservableCollection<RolesList>();
                                if (autoCoreRoles != null)
                                {
                                    for (int i = 0; i < autoCoreRoles.Length; i++)
                                    {
                                        autoCoreRolesItems.Add(new RolesList { Roles = autoCoreRoles[i] });
                                    }
                                }

                                var autoCoreSkillsItems = new ObservableCollection<SkillsList>();
                                if (autoCoreSkills != null)
                                {
                                    for (int i = 0; i < autoCoreSkills.Length; i++)
                                    {
                                        autoCoreSkillsItems.Add(new SkillsList { Skills = autoCoreSkills[i] });
                                    }
                                }

                                RolesListHeight = autoCoreRolesItems.Count * 20;

                                SkillsListHeight = autoCoreSkillsItems.Count * 20;

                                SkillsList = autoCoreSkillsItems;
                                RolesList = autoCoreRolesItems;

                                RaisePropertyChanged(nameof(SkillsList));

                                RaisePropertyChanged(nameof(RolesList));
                                // closing the loading popup.
                                LoaderPopup.CloseAllPopup();
                            }
                        }
                        else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                        {
                            CommonLib.ApiResponseSlow();
                        }
                        else
                        {
                            CommonLib.Unauthorize();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoaderPopup.CloseAllPopup(); // closing the loading popup.
                await App.Current.MainPage.DisplayAlert("", ex.Message, "OK");

            }
            finally
            {

            }

        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void CollectionDidChange(object sender,
                               NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
