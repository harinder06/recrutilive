﻿using Recruitlive.Repo;
using System.ComponentModel;
using Rg.Plugins.Popup.Extensions;
using Recruitlive.Views;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Collections.Specialized;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms.Maps;
using Xamarin.Forms;

namespace Recruitlive.ViewModels.Contact
{
    /// <summary>
    /// View Model that will used for binding the details for the contact section based on the contact id.
    /// </summary>
    public class ContactDetailsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        HttpClientBase _base = new HttpClientBase();
        public ObservableCollection<SkillsList> SkillsList { get; set; }
        public ObservableCollection<CategoriesList> CategoryList { get; set; }


        /// <summary>
        /// property for the contact name field
        /// </summary>
        private string _contactName;
        public string ContactName
        {
            get
            {
                return _contactName;
            }
            set
            {
                _contactName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ContactName"));
            }
        }

        /// <summary>
        /// property for the id field
        /// </summary>
        private string _id;
        public string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
        }

        /// <summary>
        /// property for the status field
        /// </summary>
        private string _status;
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Status"));
            }
        }

        /// <summary>
        /// property for the jobtitle field
        /// </summary>
        private string _jobtitle;
        public string JobTitle
        {
            get
            {
                return _jobtitle;
            }
            set
            {
                _jobtitle = value;
                PropertyChanged(this, new PropertyChangedEventArgs("JobTitle"));
            }
        }

        /// <summary>
        /// property for the mobile text field
        /// </summary>
        private string _mobile;
        public string Mobile
        {
            get
            {
                return _mobile;
            }
            set
            {
                _mobile = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Mobile"));
            }
        }

        /// <summary>
        /// property for the sector text field
        /// </summary>
        private string _sector;
        public string Sector
        {
            get
            {
                return _sector;
            }
            set
            {
                _sector = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Sector"));
            }
        }

        /// <summary>
        /// property for the address field
        /// </summary>
        private string _address;
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Address"));
            }
        }

        /// <summary>
        /// property for the category list view height
        /// </summary>
        private double _categoryListHeight = 0;
        public double CategoryListHeight
        {
            get
            {
                return _categoryListHeight;
            }
            set
            {
                _categoryListHeight = value;
                PropertyChanged(this, new PropertyChangedEventArgs("CategoryListHeight"));
            }
        }

        /// <summary>
        /// property for the skills list view height
        /// </summary>
        private double _skillsListHeight = 0;
        public double SkillsListHeight
        {
            get
            {
                return _skillsListHeight;
            }
            set
            {
                _skillsListHeight = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SkillsListHeight"));
            }
        }

        /// <summary>
        /// property for the client name field
        /// </summary>
        private string _clientName;
        public string ClientName
        {
            get
            {
                return _clientName;
            }
            set
            {
                _clientName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ClientName"));
            }
        }

        /// <summary>
        /// property for the address icon visible
        /// </summary>
        private bool _addressVisible;
        public bool AddressVisible
        {
            get
            {
                return _addressVisible;
            }
            set
            {
                _addressVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("AddressVisible"));
            }
        }

        /// <summary>
        /// property for the work address field
        /// </summary>
        private double _lat;
        public double Lat
        {
            get
            {
                return _lat;
            }
            set
            {
                _lat = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Lat"));
            }
        }

        /// <summary>
        /// property for the work address field
        /// </summary>
        private double _long;
        public double Long
        {
            get
            {
                return _long;
            }
            set
            {
                _long = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Long"));
            }
        }
        /// <summary>
        /// property for the phone number icon visible
        /// </summary>
        private bool _phoneNumberVisible;
        public bool PhoneNumberVisible
        {
            get
            {
                return _phoneNumberVisible;
            }
            set
            {
                _phoneNumberVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("PhoneNumberVisible"));
            }
        }


        /// <summary>
        /// property for the phone number field
        /// </summary>
        private int _clientId;
        public int ClientId
        {
            get
            {
                return _clientId;
            }
            set
            {
                _clientId = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ClientId"));
            }
        }

        int _ContactID;

        INavigation navigation;
        public ContactDetailsViewModel(int contactId, INavigation _navigation)
        {
            navigation = _navigation;
            _ContactID = contactId;
            BindData(_ContactID);
        }

        /// <summary>
        /// This method will used to bind the api to the view with the data returning from the api.
        /// </summary>
        /// <returns></returns>
        public async void BindData(int candidateId)
        {
            try
            {
                // Checking the internet connection whether user is connected to the internet or not.

                if (_base.CheckConnection())
                {
                    // displaying the loading circle to the user.
                    await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                    // Calling of the api for getting the result.
                    var result = await _base.GetContactDetails(CommonLib.MainUrl + ApiUrl.ContactDetails + candidateId);
                    foreach (var item in result)
                    {
                        if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            if (item.Key != null) // if api returns data successfully.
                            {
                                ContactName = item.Key.contactName;
                                ID = item.Key.contactID.ToString();
                                JobTitle = item.Key.jobTitle;
                                Status = item.Key.status;
                                Mobile = item.Key.phone;
                                PhoneNumberVisible = string.IsNullOrEmpty(Mobile) ? false : true;
                                Sector = item.Key.sector;
                                ClientId = item.Key.clientID;
                                ClientName = item.Key.clientName;
                                string[] categories = item.Key.category != null ? item.Key.category.Split(';').Where(a => !string.IsNullOrEmpty(a)).ToArray() : null;
                                string[] skills = item.Key.skills != null ? item.Key.skills.Split(';').Where(a => !string.IsNullOrEmpty(a)).ToArray() : null;
                                var items = new ObservableCollection<SkillsList>();
                                if (skills != null)
                                {
                                    for (int i = 0; i < skills.Length; i++)
                                    {
                                        items.Add(new SkillsList { Skills = skills[i] });
                                    }
                                }
                                var itemsCategory = new ObservableCollection<CategoriesList>();
                                if (categories != null)
                                {
                                    for (int i = 0; i < categories.Length; i++)
                                    {
                                        itemsCategory.Add(new CategoriesList { Category = categories[i] });
                                    }
                                }
                                SkillsList = items;
                                CategoryList = itemsCategory;

                                CategoryListHeight = itemsCategory.Count * 20;

                                SkillsListHeight = items.Count * 20;

                                RaisePropertyChanged(nameof(SkillsList));
                                RaisePropertyChanged(nameof(CategoryList));

                                if (item.Key.addresses != null)
                                {
                                    for (int i = 0; i < item.Key.addresses.Count; i++)
                                    {
                                        if (string.IsNullOrEmpty(Address))
                                            Address = item.Key.addresses[i].type == "Address" ? item.Key.addresses[i].street1 + " " + item.Key.addresses[i].autoSuburb + " " + item.Key.addresses[i].region + " " + item.Key.addresses[i].postCode : string.Empty;
                                    }
                                }

                                AddressVisible = string.IsNullOrEmpty(Address) ? false : true;
                                if (!string.IsNullOrEmpty(Address))
                                {
                                    var latLongResult = await CommonLib.GetLatLong(Address);
                                    foreach (var latLongItem in latLongResult)
                                    {
                                        Lat = latLongItem.Key;
                                        Long = latLongItem.Value;
                                    }
                                }
                                // closing the loading popup.
                                LoaderPopup.CloseAllPopup();
                            }
                            else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                            {
                                CommonLib.ApiResponseSlow();
                            }
                            else //if api is not returning the data.
                            {
                                CommonLib.ApiNotReturningData();
                            }
                        }
                        else
                        {
                            CommonLib.Unauthorize();
                        }
                    }
                }

            }
            catch (Exception)
            {
                LoaderPopup.CloseAllPopup(); // closing the loading popup.
            }
            finally
            {

            }

        }

        /// <summary>
        /// This tapped event will be used for opening the map.
        /// </summary>
        public Command addressCommand
        {
            get
            {
                return new Command(async (e) =>
                {

                    if (!string.IsNullOrEmpty(Address))
                    {
                        //Models.Address
                        await navigation.PushAsync(new mapView(Lat, Long, Address));
                    }

                });
            }
        }

        /// <summary>
        /// this method will be used for the calling the phone number.
        /// </summary>
        public Command callCommand
        {
            get
            {
                return new Command((e) =>
                {
                    if (!string.IsNullOrEmpty(Mobile))
                    {
                        Device.OpenUri(new Uri(string.Format("tel:{0}", Mobile)));
                    }

                });
            }
        }
        public Command clientDetailsCommand
        {
            get
            {
                return new Command(async (e) =>
                {
                    await Application.Current.MainPage.Navigation.PushAsync(new ClientDetailPage(ClientId));

                });
            }
        }
        public void CollectionDidChange(object sender,
                        NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
