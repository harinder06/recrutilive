﻿using Recruitlive.Models;
using Recruitlive.Repo;
using Recruitlive.Views;
using Recruitlive.Views.Contact;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace Recruitlive.ViewModels
{
    public class ContactRolesListViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// property for the searched text.
        /// </summary>
        public string _searchBarText;
        public string SearchBarText
        {
            get
            {
                return _searchBarText;
            }
            set
            {
                _searchBarText = value;
                OnPropertyChanged(nameof(SearchBarText));
            }
        }


        public static List<BindData> _loadRolesData = new List<BindData>();
        public event PropertyChangedEventHandler PropertyChanged;
        HttpClientBase _base = new HttpClientBase();
        public List<BindData> Roles { get; set; }
        public ContactRolesListViewModel()
        {
            GetRoles();
        }

        /// <summary>
        /// Methos to get all the data for the roles autocomplete text box
        /// </summary>
        /// <param name="searchedText"></param>
        /// <returns></returns>
        public async void GetRoles()
        {
            if (_loadRolesData.Count == 0)
            {
                try
                {
                    // Checking the internet connection whether user is connected to the internet or not.
                    if (_base.CheckConnection())
                    {
                        // displaying the loading circle to the user.
                        await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                        var rolesResult = await _base.GetRoles(CommonLib.MainUrl + ApiUrl.GetAllRoles);

                        foreach (var item in rolesResult)
                        {
                            if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                if (item.Key != null)// api is returning data successfully.
                                {
                                    _loadRolesData = new List<BindData>();
                                    for (int i = 0; i < item.Key.Count; i++)
                                    {
                                        if (!string.IsNullOrEmpty(item.Key[i].text))
                                        {
                                            _loadRolesData.Add(new BindData { Name = item.Key[i].text, Value = item.Key[i].value, imgCheckBox = "checkboxemptyblack.png" });
                                        }
                                    }
                                    LoaderPopup.CloseAllPopup();
                                }
                                else //if api is not returning the data.
                                {
                                    CommonLib.ApiNotReturningData();
                                }
                            }
                            else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                            {
                                CommonLib.ApiResponseSlow();
                            }
                            else
                            {
                                CommonLib.Unauthorize();
                            }
                        }
                    }

                }
                catch (Exception)
                {

                }
                finally
                {

                }
            }
            for (int i = 0; i < _loadRolesData.Count; i++)
            {
                if (AdvancedContactSearch.SelectedRoles != null)
                {
                    if (AdvancedContactSearch.SelectedRoles.Contains(_loadRolesData[i].Name))
                    {
                        _loadRolesData[i].imgCheckBox = "checkboxfilledblack.png";
                    }
                    else
                    {
                        _loadRolesData[i].imgCheckBox = "checkboxemptyblack.png";
                    }
                }
            }
            Roles = _loadRolesData;
            RaisePropertyChanged(nameof(Roles));
        }

        /// <summary>
        /// This method will be used for performing the search based on the value filled in the search text box of the roles page.
        /// </summary>
        public void SearchedData()
        {
            if (_loadRolesData != null || _loadRolesData.Count > 0)
            {
                if (_loadRolesData != null || _loadRolesData.Count > 0)
                {
                    if (string.IsNullOrWhiteSpace(SearchBarText))
                    {
                        Roles = _loadRolesData;
                    }
                    else
                    {
                        Roles = _loadRolesData.Where(i => i.Name.ToLower().Contains(SearchBarText.ToLower())).ToList();
                    }
                    RaisePropertyChanged(nameof(Roles));
                }
            }
        }


        /// <summary>
        /// method that will change the source of the checkbox image and will add or remove the data from the 
        /// list based on selection or unselection.
        /// </summary>
        public Command checkbox_Roles_Tapped
        {
            get
            {
                return new Command((e) =>
                {
                    BindData _model = e as BindData;
                    var index = Roles.ToList().FindIndex(x => x.Name == _model.Name);


                    if (_model.imgCheckBox == "checkboxemptyblack.png")
                    {
                        // changing the checkbox image
                        Roles[index].imgCheckBox = "checkboxfilledblack.png";

                        if (AdvancedContactSearch.SelectedRoles == null)
                        {
                            AdvancedContactSearch.SelectedRoles = new List<string>();
                        }
                        if (!AdvancedContactSearch.SelectedRoles.Contains(_model.Name))
                        {
                            AdvancedContactSearch.SelectedRoles.Add(_model.Name);
                        }
                        AdvancedContactSearch.getRoles = string.Join(",", AdvancedContactSearch.SelectedRoles);
                    }
                    else
                    {
                        // changing the checkbox image
                        Roles[index].imgCheckBox = "checkboxemptyblack.png";
                        AdvancedContactSearch.SelectedRoles.Remove(_model.Name);
                        AdvancedContactSearch.getRoles = string.Join(",", AdvancedContactSearch.SelectedRoles);
                    }
                    RaisePropertyChanged(nameof(Roles));

                });
            }
        }
        public void CollectionDidChange(object sender,
                                  NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
