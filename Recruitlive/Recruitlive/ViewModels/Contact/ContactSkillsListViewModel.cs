﻿using Recruitlive.Models;
using Recruitlive.Repo;
using Recruitlive.Views;
using Recruitlive.Views.Contact;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace Recruitlive.ViewModels.Contact
{
    public class ContactSkillsListViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// property for the serached text.
        /// </summary>
        public string _searchBarText;
        public string SearchBarText
        {
            get
            {
                return _searchBarText;
            }
            set
            {
                _searchBarText = value;
                OnPropertyChanged(nameof(SearchBarText));
            }
        }
        public static List<BindData> _loadSkillsData = new List<BindData>();

        HttpClientBase _base = new HttpClientBase();
        public List<BindData> Skills { get; set; }

        public ContactSkillsListViewModel()
        {
            GetSkills();
        }

        /// <summary>
        /// Method to get all the data for the skills autocomplete text box
        /// </summary>
        /// <returns></returns>
        public async void GetSkills()
        {
            if (_loadSkillsData.Count == 0)
            {
                try
                {
                    // Checking the internet connection whether user is connected to the internet or not.
                    if (_base.CheckConnection())
                    {
                        // displaying the loading circle to the user.
                        await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                        var rolesResult = await _base.GetRoles(CommonLib.MainUrl + ApiUrl.GetAllSkills);

                        foreach (var item in rolesResult)
                        {
                            if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                if (item.Key != null)// api is returning data successfully.
                                {
                                    _loadSkillsData = new List<BindData>();
                                    for (int i = 0; i < item.Key.Count; i++)
                                    {
                                        if (!string.IsNullOrEmpty(item.Key[i].text))
                                        {
                                            _loadSkillsData.Add(new BindData { Name = item.Key[i].text, Value = item.Key[i].value, imgCheckBox = "checkboxemptyblack.png" });
                                        }
                                    }                                   
                                    LoaderPopup.CloseAllPopup();
                                }
                                else //if api is not returning the data.
                                {
                                    CommonLib.ApiNotReturningData();
                                }
                            }
                            else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                            {
                                CommonLib.ApiResponseSlow();
                            }
                            else
                            {
                                CommonLib.Unauthorize();
                            }
                        }
                    }

                }
                catch (Exception)
                {

                }
                finally
                {

                }
            }
            for (int i = 0; i < _loadSkillsData.Count; i++)
            {
                if (AdvancedContactSearch.SelectedSkills != null)
                {
                    if (AdvancedContactSearch.SelectedSkills.Contains(_loadSkillsData[i].Name))
                    {
                        _loadSkillsData[i].imgCheckBox = "checkboxfilledblack.png";
                    }
                    else
                    {
                        _loadSkillsData[i].imgCheckBox = "checkboxemptyblack.png";
                    }
                }
            }
            Skills = _loadSkillsData;
            RaisePropertyChanged(nameof(Skills));
        }

        /// <summary>
        /// This method will be used for performing the search based on the value filled in the search text box of the skills page.
        /// </summary>
        public void SearchedData()
        {
            if (_loadSkillsData != null || _loadSkillsData.Count > 0)
            {
                if (string.IsNullOrWhiteSpace(SearchBarText))
                {
                    Skills = _loadSkillsData;
                }
                else
                {
                    Skills = _loadSkillsData.Where(i => i.Name.ToLower().Contains(SearchBarText.ToLower())).ToList();
                }
                RaisePropertyChanged(nameof(Skills));
            }
        }

        /// <summary>
        /// method that will change the source of the checkbox image and will add or remove the data from the 
        /// list based on selection or unselection.
        /// </summary>
        public Command checkbox_Skills_Tapped
        {
            get
            {
                return new Command((e) =>
                {
                    BindData _model = e as BindData;
                    var index = Skills.ToList().FindIndex(x => x.Name == _model.Name);


                    if (_model.imgCheckBox == "checkboxemptyblack.png")
                    {
                        // changing the checkbox image
                        Skills[index].imgCheckBox = "checkboxfilledblack.png";

                        if (AdvancedContactSearch.SelectedSkills == null)
                        {
                            AdvancedContactSearch.SelectedSkills = new List<string>();
                        }
                        if (!AdvancedContactSearch.SelectedSkills.Contains(_model.Name))
                        {
                            AdvancedContactSearch.SelectedSkills.Add(_model.Name);
                        }
                        AdvancedContactSearch.getSkills = string.Join(",", AdvancedContactSearch.SelectedSkills);
                    }
                    else
                    {
                        Skills[index].imgCheckBox = "checkboxemptyblack.png";
                        AdvancedContactSearch.SelectedSkills.Remove(_model.Name);
                        AdvancedContactSearch.getSkills = string.Join(",", AdvancedContactSearch.SelectedSkills);
                    }
                    RaisePropertyChanged(nameof(Skills));

                });
            }
        }

        public void CollectionDidChange(object sender,
                                  NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
