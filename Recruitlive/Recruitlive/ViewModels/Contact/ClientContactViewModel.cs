﻿using Recruitlive.Models;
using Recruitlive.Models.Contact;
using Recruitlive.Repo;
using Recruitlive.Views;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Extended;

namespace Recruitlive.ViewModels.Contact
{
    public class ClientContactViewModel : INotifyPropertyChanged
    {
        HttpClientBase _base = new HttpClientBase();
        public event PropertyChangedEventHandler PropertyChanged;
        public static int totalcount = 0;
        public bool IsFirstHit = false;
        public static int getEventCount = 0;
        public int pageindex = 1;
        public int pageSize = 5;
        public bool HitinProcess = false;
        public static ContactSearchPost _postData;
        public InfiniteScrollCollection<CandidateListModel> ContactList { get; set; }

        /// <summary>
        /// property to show need to loading again or not.
        /// </summary>
        public bool _isLoadingMore;
        public bool IsLoadingMore
        {
            get
            {
                return _isLoadingMore;
            }
            set
            {
                _isLoadingMore = value;
                OnPropertyChanged(nameof(IsLoadingMore));
            }
        }

        /// <summary>
        /// property for the getting the List Id.
        /// </summary>
        private string _listId;
        public string ListId
        {
            get
            {
                return _listId;
            }
            set
            {
                _listId = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ListId"));
            }
        }

        /// <summary>
        /// property for the specifying the sortcolumn.
        /// </summary>
        private string _sortColumn;
        public string SortColumn
        {
            get
            {
                return _sortColumn;
            }
            set
            {
                _sortColumn = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SortColumn"));
            }
        }

        /// <summary>
        /// property for the specifying the sortdirection.
        /// </summary>
        private string _sortDirection;
        public string SortDirection
        {
            get
            {
                return _sortDirection;
            }
            set
            {
                _sortDirection = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SortDirection"));
            }
        }

        /// <summary>
        /// property for the specifying the nodata.
        /// </summary>
        private string _noData;
        public string NoData
        {
            get
            {
                return _noData;
            }
            set
            {
                _noData = value;
                PropertyChanged(this, new PropertyChangedEventArgs("NoData"));
            }
        }

        private bool _searchVisibles;
        public bool SearchVisibles
        {
            get
            {
                return _searchVisibles;
            }
            set
            {
                _searchVisibles = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SearchVisibles"));
            }
        }

        private bool _noDataVisible;
        public bool NoDataVisible
        {
            get
            {
                return _noDataVisible;
            }
            set
            {
                _noDataVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("NoDataVisible"));
            }
        }

        /// <summary>
        /// property for the show the no of candidates returning from the api.
        /// </summary>
        private string _count;
        public string Count
        {
            get
            {
                return _count;
            }
            set
            {
                _count = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Count"));
            }
        }

        public ClientContactViewModel(ContactSearchPost _data)
        {
            _postData = _data;
            _sortColumn = "FirstName";
            _sortDirection = "1";
            if (_data != null)
            {
                BindContactList();
            }
        }

        /// <summary>
        /// This method will used to bind the api to the view with the data returning from the api.
        /// </summary>
        /// <returns></returns>
        public void BindContactList()
        {
            /// <summary>
            /// This method will used to bind the api to the view with the data returning from the api.
            /// </summary>
            /// <returns></returns>

            ContactList = new InfiniteScrollCollection<CandidateListModel>
            {
                OnLoadMore = async () =>
                {
                    var items = new InfiniteScrollCollection<CandidateListModel>();

                    // Checking the internet connection whether user is connected to the internet or not.
                    if (_base.CheckConnection()) // internet connection is working
                    {
                        // total no of records not exceed from the current records
                        if (totalcount > getEventCount && getEventCount != 0 || IsFirstHit == false)
                        {
                            if (!HitinProcess)
                            {
                                HitinProcess = true;
                                IsLoadingMore = true;

                                Dictionary<ContactSearchModel, HttpResponseMessage> response = null;

                                // if the records are fetched first time
                                if (pageindex == 1)
                                {
                                    response = await _base.SearchContact(CommonLib.MainUrl + ApiUrl.ContactListing + pageSize, _postData);
                                }
                                // if the page index exceeds from 1 
                                else
                                {
                                    // getting api response
                                    response = await _base.ContactPagingSorting(CommonLib.MainUrl + ApiUrl.ContactListPaging + ListId + "/" + pageSize + "/" + pageindex + "/" + SortColumn + "/" + SortDirection);
                                }
                                foreach (var data in response)
                                {
                                    // check the response is ok or not.
                                    if (data.Value.StatusCode == HttpStatusCode.OK)
                                    {
                                        foreach (var item in response.Keys)
                                        {
                                            if (response != null && item.records.Count != 0)
                                            {
                                                LoaderPopup.CloseAllPopup();

                                                SearchVisibles = true;

                                                NoDataVisible = false;                                              

                                                if (item.totalRecords > 1)
                                                {
                                                    Count = item.totalRecords + " Contacts";
                                                }
                                                else
                                                {
                                                    Count = item.totalRecords + " Contact";
                                                }

                                                pageindex++;
                                                IsFirstHit = true;
                                                totalcount = item.totalRecords;
                                                getEventCount = item.records.Count;
                                                ListId = item.listID.ToString();
                                                items = GetItems(true, item); // calling method to set data in listview
                                                IsLoadingMore = false;
                                                HitinProcess = false;
                                            }
                                            else
                                            {
                                                LoaderPopup.CloseAllPopup();
                                                if (response != null && item.totalRecords == 0)
                                                {
                                                    NoData = "No Contacts Found";
                                                    SearchVisibles = false;
                                                    NoDataVisible = true;
                                                }
                                                else
                                                {
                                                    HitinProcess = false;
                                                    IsLoadingMore = false;
                                                    getEventCount = 0;
                                                }
                                            }
                                        }
                                    }
                                    else if (data.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                                    {
                                        CommonLib.ApiResponseSlow();
                                    }
                                    else
                                    {
                                        CommonLib.Unauthorize();
                                    }


                                }
                                IsLoadingMore = false;
                            }
                        }
                    }
                    return items;
                }
            };
            ContactList.LoadMoreAsync();
        }

        /// <summary>
        /// Method for setting the data in the list view.
        /// </summary>
        /// <param name="clearList"></param>
        /// <param name="contactItemList"></param>
        /// <returns></returns>
        InfiniteScrollCollection<CandidateListModel> GetItems(bool clearList, ContactSearchModel contactItemList)
        {
            InfiniteScrollCollection<CandidateListModel> items = null;
            try
            {
                if (clearList || ContactList == null)
                {
                    items = new InfiniteScrollCollection<CandidateListModel>();
                }
                else
                {
                    items = new InfiniteScrollCollection<CandidateListModel>(ContactList);
                }


                // setting all the data into the list view by getting from the api.
                foreach (var contactItem in contactItemList.records)
                {
                    string[] splitName = !string.IsNullOrEmpty(contactItem.name) ? contactItem.name.Split(' ') : null;
                    string FirstName = splitName != null ? splitName[0] : string.Empty;
                    string LastName = splitName != null || splitName.Length > 1 ? splitName[1] : string.Empty;
                    items.Add(new CandidateListModel
                    {
                        employeeName = contactItem.name,
                        firstName = FirstName,
                        lastName = LastName,
                        jobTitle=contactItem.position,
                        emmployeeID = "ID " + contactItem.contactID.ToString(),
                        date = contactItem.lastUpdatedDate.ToString("dd MMMM yyyy"),
                        phonenumber = contactItem.phone,
                        email = contactItem.email,
                        phonenumberhide = string.IsNullOrEmpty(contactItem.phone) ? false : true,
                        emailhide = string.IsNullOrEmpty(contactItem.email) ? false : true,
                    });
                }
            }
            catch (Exception)
            {

            }
            return items;
        }

        /// <summary>
        /// this method will be used for performing the phone number calling functionality.
        /// </summary>
        public Command callCommand
        {
            get
            {
                return new Command((e) =>
                {
                    CandidateListModel _model = e as CandidateListModel;

                    if (!string.IsNullOrEmpty(_model.phonenumber))
                    {
                        Device.OpenUri(new Uri(string.Format("tel:{0}", _model.phonenumber)));
                    }
                });
            }
        }

        /// <summary>
        /// this method will be used for performing the email functionality.
        /// </summary>
        public Command emailCommand
        {
            get
            {
                return new Command((e) =>
                {
                    CandidateListModel _model = e as CandidateListModel;

                    if (!string.IsNullOrEmpty(_model.email))
                    {
                        Device.OpenUri(new Uri(string.Format("mailto:{0}", _model.email)));
                    }

                });
            }
        }

        public void CollectionDidChange(object sender,
                             NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
