﻿using Recruitlive.Repo;
using Recruitlive.Views;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Recruitlive.ViewModels
{
    /// <summary>
    /// View Model that will used for binding the details for the client section based on the client id.
    /// </summary>

    public class ClientDetailsViewModel : INotifyPropertyChanged
    {
        HttpClientBase _base = new HttpClientBase();
        public int clientID;
        public ObservableCollection<SkillsList> SkillsList { get; set; }
        public ObservableCollection<CategoriesList> CategoryList { get; set; }

        /// <summary>
        /// property for the company name field
        /// </summary>
        private string _companyName;
        public string CompanyName
        {
            get
            {
                return _companyName;
            }
            set
            {
                _companyName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("CompanyName"));
            }
        }

        /// <summary>
        /// property for the company name field
        /// </summary>
        private string _website;
        public string Website
        {
            get
            {
                return _website;
            }
            set
            {
                _website = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Website"));
            }
        }

        /// <summary>
        /// property for the sector field
        /// </summary>
        private string _sector;
        public string Sector
        {
            get
            {
                return _sector;
            }
            set
            {
                _sector = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Sector"));
            }
        }

        /// <summary>
        /// property for the id field
        /// </summary>

        private string _id;
        public string Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Id"));
            }
        }

        /// <summary>
        /// property for the status field
        /// </summary>
        private string _status;
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Status"));
            }
        }
        /// <summary>
        /// property for the work address field
        /// </summary>
        private string _workAddress;
        public string WorkAddress
        {
            get
            {
                return _workAddress;
            }
            set
            {
                _workAddress = value;
                PropertyChanged(this, new PropertyChangedEventArgs("WorkAddress"));
            }
        }

        /// <summary>
        /// property for the email field
        /// </summary>
        private string _email;
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Email"));
            }
        }

        /// <summary>
        /// property for the phone number field
        /// </summary>
        private string _phonenumber;
        public string PhoneNumber
        {
            get
            {
                return _phonenumber;
            }
            set
            {
                _phonenumber = value;
                PropertyChanged(this, new PropertyChangedEventArgs("PhoneNumber"));
            }
        }

        /// <summary>
        /// property for the address field
        /// </summary>
        private string _address;
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Address"));
            }
        }

        /// <summary>
        /// property for the category list view height
        /// </summary>
        private double _categoryListHeight = 0;
        public double CategoryListHeight
        {
            get
            {
                return _categoryListHeight;
            }
            set
            {
                _categoryListHeight = value;
                PropertyChanged(this, new PropertyChangedEventArgs("CategoryListHeight"));
            }
        }

        /// <summary>
        /// property for the skills list view height
        /// </summary>
        private double _skillsListHeight = 0;
        public double SkillsListHeight
        {
            get
            {
                return _skillsListHeight;
            }
            set
            {
                _skillsListHeight = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SkillsListHeight"));
            }
        }

        /// <summary>
        /// property for the work address field
        /// </summary>
        private double _lat;
        public double Lat
        {
            get
            {
                return _lat;
            }
            set
            {
                _lat = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Lat"));
            }
        }

        /// <summary>
        /// property for the work address field
        /// </summary>
        private double _long;
        public double Long
        {
            get
            {
                return _long;
            }
            set
            {
                _long = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Long"));
            }
        }

        /// <summary>
        /// property for the phone number icon visible
        /// </summary>
        private bool _phoneNumberVisible;
        public bool PhoneNumberVisible
        {
            get
            {
                return _phoneNumberVisible;
            }
            set
            {
                _phoneNumberVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("PhoneNumberVisible"));
            }
        }

        /// <summary>
        /// property for the email icon visible
        /// </summary>
        private bool _emailVisible;
        public bool EmailVisible
        {
            get
            {
                return _emailVisible;
            }
            set
            {
                _emailVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("EmailVisible"));
            }
        }

        /// <summary>
        /// property for the address icon visible
        /// </summary>
        private bool _addressVisible;
        public bool AddressVisible
        {
            get
            {
                return _addressVisible;
            }
            set
            {
                _addressVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("AddressVisible"));
            }
        }

        INavigation navigation;
        public ClientDetailsViewModel(int clientId, INavigation _navigation)
        {
            clientID = clientId;
            navigation = _navigation;
            BindData();
        }

        /// <summary>
        /// This method will used to bind the api to the view with the data returning from the api.
        /// </summary>
        /// <returns></returns>
        public async void BindData()
        {
            try
            {
                // Checking the internet connection whether user is connected to the internet or not.
                if (_base.CheckConnection())
                {
                    // displaying the loading circle to the user.
                    await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                    // Calling of the api for getting the result.
                    var result = await _base.GetClientDetails(CommonLib.MainUrl + ApiUrl.ClientViewDetails + clientID);
                    foreach (var item in result)
                    {
                        if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            if (item.Key != null) // if api returns data successfully.
                            {
                                CompanyName = item.Key.companyName;
                                Sector = !string.IsNullOrEmpty(item.Key.sector) ? item.Key.sector.Trim(';') : string.Empty;
                                Id = item.Key.companyID.ToString();
                                Status = item.Key.status;
                                Email = item.Key.email;
                                PhoneNumber = item.Key.phone;
                                PhoneNumberVisible = string.IsNullOrEmpty(PhoneNumber) ? false : true;
                                EmailVisible = string.IsNullOrEmpty(Email) ? false : true;
                                Website = item.Key.website;

                                string[] categories = item.Key.category != null ? item.Key.category.Split(';').Where(a => !string.IsNullOrEmpty(a)).ToArray() : null;
                                string[] skills = item.Key.skills != null ? item.Key.skills.Split(';').Where(a => !string.IsNullOrEmpty(a)).ToArray() : null;
                                var items = new ObservableCollection<SkillsList>();
                                if (skills != null)
                                {
                                    for (int i = 0; i < skills.Length; i++)
                                    {
                                        items.Add(new SkillsList { Skills = skills[i] });
                                    }
                                }
                                var itemsCategory = new ObservableCollection<CategoriesList>();
                                if (categories != null)
                                {
                                    for (int i = 0; i < categories.Length; i++)
                                    {
                                        itemsCategory.Add(new CategoriesList { Category = categories[i] });
                                    }
                                }
                                SkillsList = items;
                                CategoryList = itemsCategory;

                                CategoryListHeight = itemsCategory.Count * 20;

                                SkillsListHeight = items.Count * 20;

                                RaisePropertyChanged(nameof(SkillsList));
                                RaisePropertyChanged(nameof(CategoryList));

                                if (item.Key.addresses != null)
                                {
                                    for (int i = 0; i < item.Key.addresses.Count(); i++)
                                    {
                                        if (string.IsNullOrEmpty(Address))
                                        {
                                            Address = item.Key.addresses[i].type == "Address" ? item.Key.addresses[i].street1 + " " + item.Key.addresses[i].autoSuburb + " " + item.Key.addresses[i].region + " " + item.Key.addresses[i].postCode : string.Empty;
                                        }
                                        if (string.IsNullOrEmpty(WorkAddress))
                                            WorkAddress = item.Key.addresses[i].type == "Work" ? item.Key.addresses[i].street1 + " " + item.Key.addresses[i].autoSuburb + " " + item.Key.addresses[i].region + " " + item.Key.addresses[i].postCode : string.Empty;

                                    }
                                }
                                AddressVisible = !string.IsNullOrEmpty(Address) && !string.IsNullOrEmpty(Address.Trim()) ? true : false;
                                if (!string.IsNullOrEmpty(Address))
                                {
                                    var latLongResult = await CommonLib.GetLatLong(Address);
                                    foreach (var latLongItem in latLongResult)
                                    {
                                        Lat = latLongItem.Key;
                                        Long = latLongItem.Value;
                                    }
                                }
                            }
                            else //if api is not returning the data.
                            {
                                CommonLib.ApiNotReturningData();
                            }
                        }
                        
                        else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                        {
                            CommonLib.ApiResponseSlow();
                        }
                        else
                        {
                            CommonLib.Unauthorize();
                        }
                    }
                    // closing the loading popup.
                    LoaderPopup.CloseAllPopup();
                }

                else //if api is not returning the data.
                {
                    CommonLib.ApiNotReturningData();
                }
            }
            catch (Exception)
            {
                LoaderPopup.CloseAllPopup(); // closing the loading popup.
            }
            finally
            {

            }

        }

        // we give command to call and email icon on list cell to invoke call function and functioning on device

        /// <summary>
        /// this method will be used for the calling the phone number.
        /// </summary>
        public Command callCommand
        {
            get
            {
                return new Command((e) =>
                {
                    if (!string.IsNullOrEmpty(PhoneNumber))
                    {
                        Device.OpenUri(new Uri(string.Format("tel:{0}", PhoneNumber)));
                    }

                });
            }
        }

        /// <summary>
        /// this method will be used for performing the email functionality.
        /// </summary>
        public Command emailCommand
        {
            get
            {
                return new Command((e) =>
                {
                    if (!string.IsNullOrEmpty(Email))
                    {
                        Device.OpenUri(new Uri(string.Format("mailto:{0}", Email)));
                    }

                });
            }
        }

        /// <summary>
        /// This tapped event will be used for opening the map.
        /// </summary>
        public Command addressCommand
        {
            get
            {
                return new Command(async (e) =>
                {

                    if (!string.IsNullOrEmpty(Address))
                    {
                        //Models.Address
                        await navigation.PushAsync(new mapView(Lat, Long, Address));
                    }

                });
            }
        }

        /// <summary>
        /// This tapped event will be used for opening the notes page.
        /// </summary>
        public Command notesCommand
        {
            get
            {
                return new Command((e) =>
                {
                    navigation.PushAsync(new NoteslistPage(clientID,"Client"));

                });
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void CollectionDidChange(object sender,
                                NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
