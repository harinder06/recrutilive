﻿using Recruitlive.Models;
using Recruitlive.Repo;
using Recruitlive.Views;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Extended;

namespace Recruitlive.ViewModels
{
    /// <summary>
    /// this view model will be used for binding all the data for the candidate listing.
    /// </summary>
    public class CandidateListViewModel : INotifyPropertyChanged
    {
        HttpClientBase _base = new HttpClientBase();
        public event PropertyChangedEventHandler PropertyChanged;
        public static int totalcount = 0;
        public bool IsFirstHit = false;
        public static int getEventCount = 0;
        public int pageindex = 1;
        public bool isSorted = false;
        public int pageSize = 5;
        public bool HitinProcess = false;
        public static CandidateSearchPost _postData;
        public static bool checkVal;
        public InfiniteScrollCollection<CandidateListModel> CandidateList { get; set; }

        /// <summary>
        /// property to show need to loading again or not.
        /// </summary>
        public bool _isLoadingMore;
        public bool IsLoadingMore
        {
            get
            {
                return _isLoadingMore;
            }
            set
            {
                _isLoadingMore = value;
                OnPropertyChanged(nameof(IsLoadingMore));
            }
        }

        /// <summary>
        /// property for the visiability for the search result text
        /// </summary>
        private bool _searchVisible;
        public bool SearchVisible
        {
            get
            {
                return _searchVisible;
            }
            set
            {
                _searchVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SearchVisible"));
            }
        }

        /// <summary>
        /// property for the visiability for the no of candidates text
        /// </summary>
        private bool _searchVisibles;
        public bool SearchVisibles
        {
            get
            {
                return _searchVisibles;
            }
            set
            {
                _searchVisibles = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SearchVisibles"));
            }
        }

        /// <summary>
        /// property for the show the no of candidates returning from the api.
        /// </summary>
        private string _count;
        public string Count
        {
            get
            {
                return _count;
            }
            set
            {
                _count = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Count"));
            }
        }

        /// <summary>
        /// property for the show the search result returning from the api.
        /// </summary>
        private string _searchResult;
        public string SearchResult
        {
            get
            {
                return _searchResult;
            }
            set
            {
                _searchResult = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SearchResult"));
            }
        }

        /// <summary>
        /// property for the getting the List Id.
        /// </summary>
        private string _listId;
        public string ListId
        {
            get
            {
                return _listId;
            }
            set
            {
                _listId = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ListId"));
            }
        }

        /// <summary>
        /// property for the specifying the sortcolumn.
        /// </summary>
        private string _sortColumn;
        public string SortColumn
        {
            get
            {
                return _sortColumn;
            }
            set
            {
                _sortColumn = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SortColumn"));
            }
        }

        /// <summary>
        /// property for the specifying the sortdirection.
        /// </summary>
        private string _sortDirection;
        public string SortDirection
        {
            get
            {
                return _sortDirection;
            }
            set
            {
                _sortDirection = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SortDirection"));
            }
        }

        /// <summary>
        /// property for the specifying the imageFilterSource.
        /// </summary>
        private string _imagefilter;
        public string ImageFilter
        {
            get
            {
                return _imagefilter;
            }
            set
            {
                _imagefilter = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ImageFilter"));
            }
        }

        /// <summary>
        /// property for the giving the textcolor to the sorting labels of first name.
        /// </summary>
        private string _textcolorfirstname;
        public string TextColorFirstName
        {
            get
            {
                return _textcolorfirstname;
            }
            set
            {
                _textcolorfirstname = value;
                PropertyChanged(this, new PropertyChangedEventArgs("TextColorFirstName"));
            }
        }

        /// <summary>
        /// property for the giving the textcolor to the sorting label of last name.
        /// </summary>
        private string _textcolorlastname;
        public string TextColorLastName
        {
            get
            {
                return _textcolorlastname;
            }
            set
            {
                _textcolorlastname = value;
                PropertyChanged(this, new PropertyChangedEventArgs("TextColorLastName"));
            }
        }

        /// <summary>
        /// property for the specifying the backgroundcolor for the sorting label of first name.
        /// </summary>
        private Color _backgroundfirstname;
        public Color BackgroundFirstName
        {
            get
            {
                return _backgroundfirstname;
            }
            set
            {
                _backgroundfirstname = value;
                PropertyChanged(this, new PropertyChangedEventArgs("BackgroundFirstName"));
            }
        }

        /// <summary>
        /// property for the specifying the backgroundcolor for the sorting label of last name.
        /// </summary>
        private Color _backgroundlastname;
        public Color BackgroundLastName
        {
            get
            {
                return _backgroundlastname;
            }
            set
            {
                _backgroundlastname = value;
                PropertyChanged(this, new PropertyChangedEventArgs("BackgroundLastName"));
            }
        }

        public void CheckIsHighLighted(bool value)
        {
            if (value)
            {
                _imagefilter = "icadvnc.png";
            }
            else
            {
                _imagefilter = "ichighlightfilter.png";
            }
            RaisePropertyChanged(nameof(ImageFilter));
        }

        public CandidateListViewModel(CandidateSearchPost _data, bool check)
        {
            try
            {
                checkVal = check;
                _searchVisible = false;
                _searchVisibles = false;
                _postData = _data;
                _sortColumn = "Dateresumeupdated";
                _sortDirection = "2";
                _textcolorfirstname = "Gray";
                _backgroundfirstname = Color.White;
                _textcolorlastname = "Gray";
                _backgroundlastname = Color.White;
                if (!check)
                {
                    _imagefilter = "icadvnc.png";
                }
                else
                {
                    _imagefilter = "ichighlightfilter.png";
                }
                if (_data != null)
                {
                    SearchCandidateMethod(check);
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// This method will used to bind the api to the view with the data returning from the api.
        /// </summary>
        /// <returns></returns>

        public void SearchCandidateMethod(bool check)
        {

            /// <summary>
            /// This method will used to bind the api to the view with the data returning from the api.
            /// </summary>
            /// <returns></returns>

            CandidateList = new InfiniteScrollCollection<CandidateListModel>
            {
                OnLoadMore = async () =>
                {
                    var items = new InfiniteScrollCollection<CandidateListModel>();

                    // Checking the internet connection whether user is connected to the internet or not.
                    if (_base.CheckConnection()) // internet connection is working
                    {
                        // total no of records not exceed from the current records
                        if (totalcount > getEventCount && getEventCount != 0 || IsFirstHit == false)
                        {
                            if (!HitinProcess)
                            {
                                HitinProcess = true;
                                IsLoadingMore = true;

                                Dictionary<CandidateSearchModel, HttpResponseMessage> response=null;

                                // if the records are fetched first time
                                if (pageindex == 1 && isSorted == false)
                                {
                                    response = await _base.SearchCandidate(CommonLib.MainUrl + ApiUrl.CandidateListing + pageSize, _postData);
                                }
                                if (isSorted == false && pageindex != 1)
                                {
                                    // getting api response
                                    response = await _base.CandidatePagingSorting(CommonLib.MainUrl + ApiUrl.CandidateListPaging + ListId + "/" + pageSize + "/" + pageindex + "/" + SortColumn + "/" + SortDirection);
                                }
                                if (isSorted == true)
                                {
                                    // getting api response
                                    response = await _base.CandidatePagingSorting(CommonLib.MainUrl + ApiUrl.CandidateListPaging + ListId + "/" + pageSize + "/" + pageindex + "/" + SortColumn + "/" + SortDirection);
                                }
                                // if the page index exceeds from 1 
                                
                                foreach (var data in response)
                                {
                                    // check the response is ok or not.
                                    if (data.Value.StatusCode == HttpStatusCode.OK)
                                    {
                                        foreach (var item in response.Keys)
                                        {
                                            if (response != null && item.records.Count != 0)
                                            {
                                                LoaderPopup.CloseAllPopup();
                                                SearchVisible = true;
                                                SearchVisibles = true;

                                                if (!check)
                                                {
                                                    _imagefilter = "icadvnc.png";
                                                }
                                                else
                                                {
                                                    _imagefilter = "ichighlightfilter.png";
                                                }

                                                if (item.totalRecords > 1)
                                                {
                                                    Count = item.totalRecords + " Candidates";
                                                }
                                                else
                                                {
                                                    Count = item.totalRecords + " Candidate";
                                                }
                                                SearchResult = "Search Result";

                                                pageindex++;
                                                IsFirstHit = true;
                                                totalcount = item.totalRecords;
                                                getEventCount = item.records.Count;
                                                ListId = item.listID.ToString();
                                                items = GetItems(true, item); // calling method to set data in listview
                                                IsLoadingMore = false;
                                                HitinProcess = false;
                                            }
                                            else
                                            {
                                                LoaderPopup.CloseAllPopup();
                                                if (response != null && item.totalRecords == 0)
                                                {
                                                    SearchVisible = true;
                                                    SearchResult = "No Search Result";
                                                    if (!check)
                                                    {
                                                        _imagefilter = "icadvnc.png";
                                                    }
                                                    else
                                                    {
                                                        _imagefilter = "ichighlightfilter.png";
                                                    }
                                                }
                                                else
                                                {
                                                    SearchVisible = true;
                                                    SearchVisibles = true;
                                                    SearchResult = "Search Result";
                                                    getEventCount = 0;
                                                    HitinProcess = false;
                                                    IsLoadingMore = false;
                                                    if (!check)
                                                    {
                                                        _imagefilter = "icadvnc.png";
                                                    }
                                                    else
                                                    {
                                                        _imagefilter = "ichighlightfilter.png";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (data.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                                    {
                                        CommonLib.ApiResponseSlow();
                                    }
                                    else
                                    {
                                        CommonLib.Unauthorize();
                                    }


                                }
                                IsLoadingMore = false;
                            }
                        }
                    }
                    return items;
                }
            };
            CandidateList.LoadMoreAsync();
        }

        /// <summary>
        /// Method for setting the data in the list view.
        /// </summary>
        /// <param name="clearList"></param>
        /// <param name="candidateItemList"></param>
        /// <returns></returns>
        InfiniteScrollCollection<CandidateListModel> GetItems(bool clearList, CandidateSearchModel candidateItemList, List<CandidateListModel> _list = null)
        {
            InfiniteScrollCollection<CandidateListModel> items = null;
            try
            {
                if (clearList || CandidateList == null)
                {
                    items = new InfiniteScrollCollection<CandidateListModel>();
                }
                else
                {
                    items = new InfiniteScrollCollection<CandidateListModel>(CandidateList);
                }
                if (_list != null && _list.Count > 0)
                {
                    foreach (var candidateItem in _list)
                    {
                        items.Add(new CandidateListModel
                        {
                            employeeName = candidateItem.employeeName,
                            firstName = candidateItem.firstName,
                            lastName = candidateItem.lastName,
                            emmployeeID = candidateItem.emmployeeID,
                            jobTitle = !string.IsNullOrEmpty(candidateItem.jobTitle) ? candidateItem.jobTitle.Trim(';') : string.Empty,
                            date = candidateItem.date,
                            phonenumber = candidateItem.phonenumber,
                            email = candidateItem.email,
                            phonenumberhide = candidateItem.phonenumberhide,
                            emailhide = candidateItem.emailhide,
                        });
                    }
                }

                // setting all the data into the list view by getting from the api.
                foreach (var candidateItem in candidateItemList.records)
                {
                    string[] splitName = !string.IsNullOrEmpty(candidateItem.candidateName) ? 
                                                candidateItem.candidateName.Split(' ') : null;
                    string FirstName = splitName != null ? splitName[0] : string.Empty;
                    string LastName = splitName != null || splitName.Length > 1 ? splitName[1] : string.Empty;

                    items.Add(new CandidateListModel
                    {
                        employeeName = candidateItem.candidateName,
                        firstName = FirstName,
                        lastName = LastName,
                        emmployeeID = "ID " + candidateItem.candidateID.ToString(),
                        jobTitle = !string.IsNullOrEmpty(candidateItem.candidateProfile) ? candidateItem.candidateProfile.Trim(';') : string.Empty,
                        date = candidateItem.createdDate.ToString("dd MMMM yyyy"),
                        phonenumber = candidateItem.mobile,
                        email = candidateItem.emailID,
                        phonenumberhide = string.IsNullOrEmpty(candidateItem.mobile) ? false : true,
                        emailhide = string.IsNullOrEmpty(candidateItem.emailID) ? false : true,
                    });
                }
            }
            catch (Exception)
            {

            }


            return items;
        }


        /// <summary>
        /// this method will be used for the calling the phone number.
        /// </summary>
        public Command callCommand
        {
            get
            {
                return new Command((e) =>
                {
                    CandidateListModel model = e as CandidateListModel;

                    if (!string.IsNullOrEmpty(model.phonenumber))
                    {
                        Device.OpenUri(new Uri(string.Format("tel:{0}", model.phonenumber)));

                    }
                });
            }
        }

        /// <summary>
        /// this method will used for the last name sorting
        /// </summary>
        public Command lastName_Tapped
        {

            get
            {
                return new Command((e) =>
                {
                    if (CandidateList != null && CandidateList.Count != 0)
                    {
                        if (_backgroundlastname == Color.White)
                        {
                            _backgroundlastname = Color.FromHex("#d4d4d4");
                            _textcolorlastname = "White";
                            _backgroundfirstname = Color.White;
                            _textcolorfirstname = "Gray";
                            _sortColumn = "familyname";
                            _sortDirection = "1";
                        }
                        else
                        {
                            _backgroundlastname = Color.White;
                            _textcolorlastname = "Gray";
                            _sortColumn = "Dateresumeupdated";
                            _sortDirection = "2";
                        }

                        HitinProcess = false;
                        pageindex = 1;
                        pageSize = 15;
                        IsFirstHit = false;
                        isSorted = true;
                        CandidateList = new InfiniteScrollCollection<CandidateListModel>();
                        RaisePropertyChanged(nameof(CandidateList));
                        SearchCandidateMethod(checkVal);

                        RaisePropertyChanged(nameof(SortColumn));
                        RaisePropertyChanged(nameof(SortDirection));
                        RaisePropertyChanged(nameof(BackgroundLastName));
                        RaisePropertyChanged(nameof(BackgroundFirstName));
                        RaisePropertyChanged(nameof(TextColorFirstName));
                        RaisePropertyChanged(nameof(TextColorLastName));
                    }
                });

            }
        }


        /// <summary>
        /// this method will used for the first name sorting
        /// </summary>
        public Command firstName_Tapped
        {
            get
            {
                return new Command((e) =>
                {
                    if (CandidateList != null && CandidateList.Count > 0)
                    {
                        if (_backgroundfirstname == Color.White)
                        {
                            _backgroundfirstname = Color.FromHex("#d4d4d4");
                            _textcolorfirstname = "White";
                            _backgroundlastname = Color.White;
                            _textcolorlastname = "Gray";
                            _sortColumn = "FirstName";
                            _sortDirection = "1";
                        }
                        else
                        {
                            _backgroundfirstname = Color.White;
                            _textcolorfirstname = "Gray";
                            _sortColumn = "Dateresumeupdated";
                            _sortDirection = "2";
                        }

                        HitinProcess = false;
                        pageindex = 1;
                        pageSize = 15;
                        IsFirstHit = false;
                        totalcount = 0;
                        isSorted = true;
                        getEventCount = 0;
                        CandidateList = new InfiniteScrollCollection<CandidateListModel>();
                        RaisePropertyChanged(nameof(CandidateList));
                        SearchCandidateMethod(checkVal);

                        RaisePropertyChanged(nameof(SortColumn));
                        RaisePropertyChanged(nameof(SortDirection));
                        RaisePropertyChanged(nameof(BackgroundFirstName));
                        RaisePropertyChanged(nameof(TextColorFirstName));
                        RaisePropertyChanged(nameof(BackgroundLastName));
                        RaisePropertyChanged(nameof(TextColorLastName));
                    }
                });
            }
        }

        /// <summary>
        /// this method will be used for performing the email functionality.
        /// </summary>
        public Command emailCommand
        {
            get
            {
                return new Command((e) =>
                {
                    CandidateListModel model = e as CandidateListModel;

                    if (!string.IsNullOrEmpty(model.email))
                    {
                        Device.OpenUri(new Uri(string.Format("mailto:{0}", model.email)));
                    }

                });
            }
        }

        public void CollectionDidChange(object sender,
                               NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
