﻿using Recruitlive.DependencyInterface;
using Recruitlive.Repo;
using Recruitlive.Views;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Recruitlive.ViewModels.Candidate
{
    public class CvDownloadViewModel : INotifyPropertyChanged
    {
        HttpClientBase _base = new HttpClientBase();
        public event PropertyChangedEventHandler PropertyChanged;

        public CvDownloadViewModel(int resumeID, string screenName)
        {
            BindData(resumeID, screenName);
        }

        /// <summary>
        /// This method will used to bind the api to the view with the data returning from the api.
        /// </summary>
        /// <returns></returns>
        public async void BindData(int resumeId, string screenName)
        {
            try
            {
                // Checking the internet connection whether user is connected to the internet or not.
                if (_base.CheckConnection())
                {
                    // displaying the loading circle to the user.
                    await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                    // Calling of the api for getting the result.

                   Dictionary<byte[], HttpResponseMessage> result;

                    if (screenName == "Job")
                    {
                        result = await _base.GetResume(CommonLib.MainUrl + ApiUrl.JobApplicantsResume + resumeId);
                    }
                    else
                    {
                        result = await _base.GetResume(CommonLib.MainUrl + ApiUrl.CandidateResume + resumeId);
                    }

                    foreach (var item in result)
                    {
                        if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            if (item.Key != null) // if api returns data successfully.
                            {
                                DependencyService.Get<IGeneratePDF>().SaveFiles("Resume" + Guid.NewGuid(), item.Key);
                            }

                            else //if api is not returning the data.
                            {
                                CommonLib.ApiNotReturningData();
                            }
                        }
                        else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                        {
                            CommonLib.ApiResponseSlow();
                        }
                        else if (item.Value.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                        {
                            CommonLib.Unauthorize();
                        }
                        else
                        {
                            LoaderPopup.CloseAllPopup(); // closing the loading popup.
                            await App.Current.MainPage.DisplayAlert("Recruitlive", "No Resume Found.", "OK");
                        }
                    }
                }
                LoaderPopup.CloseAllPopup(); // closing the loading popup.

            }
            catch (Exception)
            {
                LoaderPopup.CloseAllPopup(); // closing the loading popup.
            }
            finally
            {

            }

        }

        public void CollectionDidChange(object sender,
                        NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
