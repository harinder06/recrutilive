﻿using Recruitlive.Repo;
using Recruitlive.Views;
using System.Collections.ObjectModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Recruitlive.ViewModels
{
    /// <summary>
    /// View Model that will used for binding the details for the candidate section based on the candidate id.
    /// </summary>
    public class CandidateDetailsViewModel : INotifyPropertyChanged
    {
        HttpClientBase _base = new HttpClientBase();
        public ObservableCollection<CategoriesList> CoreList { get; set; }

        /// <summary>
        /// property for the first name field
        /// </summary>
        private string _firstName;
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("FirstName"));
            }
        }

        /// <summary>
        /// property for the last name field
        /// </summary>
        private string _lastName;
        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("LastName"));
            }
        }

        /// <summary>
        /// property for the id field
        /// </summary>
        private string _id;
        public string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
        }

        /// <summary>
        /// property for the full name field
        /// </summary>
        private string _fullname;
        public string FullName
        {
            get
            {
                return _fullname;
            }
            set
            {
                _fullname = value;
                PropertyChanged(this, new PropertyChangedEventArgs("FullName"));
            }
        }

        /// <summary>
        /// property for the employer name field
        /// </summary>
        private string _employer;
        public string Employer
        {
            get
            {
                return _employer;
            }
            set
            {
                _employer = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Employer"));
            }
        }

        /// <summary>
        /// property for the employer type field
        /// </summary>
        private string _employerType;
        public string EmployerType
        {
            get
            {
                return _employerType;
            }
            set
            {
                _employerType = value;
                PropertyChanged(this, new PropertyChangedEventArgs("EmployerType"));
            }
        }

        /// <summary>
        /// property for the status field
        /// </summary>
        private string _status;
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Status"));
            }
        }

        /// <summary>
        /// property for the mobile text field
        /// </summary>
        private string _mobile;
        public string Mobile
        {
            get
            {
                return _mobile;
            }
            set
            {
                _mobile = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Mobile"));
            }
        }

        /// <summary>
        /// property for the email text field
        /// </summary>
        private string _email;
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Email"));
            }
        }

        /// <summary>
        /// property for the candidate profile field
        /// </summary>
        private string _candidateProfile;
        public string CandidateProfile
        {
            get
            {
                return _candidateProfile;
            }
            set
            {
                _candidateProfile = value;
                PropertyChanged(this, new PropertyChangedEventArgs("CandidateProfile"));
            }
        }

        /// <summary>
        /// property for the home address field
        /// </summary>
        /// 
        private string _homeAddress;
        public string HomeAddress
        {
            get
            {
                return _homeAddress;
            }
            set
            {
                _homeAddress = value;
                PropertyChanged(this, new PropertyChangedEventArgs("HomeAddress"));
            }
        }

        /// <summary>
        /// property for the address field
        /// </summary>
        private string _address;
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Address"));
            }
        }

        /// <summary>
        /// property for the work address field
        /// </summary>
        private string _workAddress;
        public string WorkAddress
        {
            get
            {
                return _workAddress;
            }
            set
            {
                _workAddress = value;
                PropertyChanged(this, new PropertyChangedEventArgs("WorkAddress"));
            }
        }

        /// <summary>
        /// property for the work address field
        /// </summary>
        private string _resumeId;
        public string ResumeId
        {
            get
            {
                return _resumeId;
            }
            set
            {
                _resumeId = value;
                PropertyChanged(this, new PropertyChangedEventArgs("ResumeId"));
            }
        }

        /// <summary>
        /// property for the work address field
        /// </summary>
        private double _lat;
        public double Lat
        {
            get
            {
                return _lat;
            }
            set
            {
                _lat = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Lat"));
            }
        }

        /// <summary>
        /// property for the work address field
        /// </summary>
        private double _long;
        public double Long
        {
            get
            {
                return _long;
            }
            set
            {
                _long = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Long"));
            }
        }

        /// <summary>
        /// property for the phone number icon visible
        /// </summary>
        private bool _phoneNumberVisible;
        public bool PhoneNumberVisible
        {
            get
            {
                return _phoneNumberVisible;
            }
            set
            {
                _phoneNumberVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("PhoneNumberVisible"));
            }
        }

        /// <summary>
        /// property for the email icon visible
        /// </summary>
        private bool _emailVisible;
        public bool EmailVisible
        {
            get
            {
                return _emailVisible;
            }
            set
            {
                _emailVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("EmailVisible"));
            }
        }

        /// <summary>
        /// property for the address icon visible
        /// </summary>
        private bool _addressVisible;
        public bool AddressVisible
        {
            get
            {
                return _addressVisible;
            }
            set
            {
                _addressVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("AddressVisible"));
            }
        }

        /// <summary>
        /// property for the status field
        /// </summary>
        private string _lastEmployer;
        public string LastEmployer
        {
            get
            {
                return _lastEmployer;
            }
            set
            {
                _lastEmployer = value;
                PropertyChanged(this, new PropertyChangedEventArgs("LastEmployer"));
            }
        }

        /// <summary>
        /// property for the status image field
        /// </summary>
        private string _statusImage;
        public string StatusImage
        {
            get
            {
                return _statusImage;
            }
            set
            {
                _statusImage = value;
                PropertyChanged(this, new PropertyChangedEventArgs("StatusImage"));
            }
        }

        /// <summary>
        /// property for the skills list view height
        /// </summary>
        private double _coreListHeight = 0;
        public double CoreListHeight
        {
            get
            {
                return _coreListHeight;
            }
            set
            {
                _coreListHeight = value;
                PropertyChanged(this, new PropertyChangedEventArgs("CoreListHeight"));
            }
        }

        INavigation navigation;
        int CandidateID;
        public CandidateDetailsViewModel(int candidateId, INavigation _navigation)
        {
            CandidateID = candidateId;
            navigation = _navigation;
            BindData(candidateId);
        }

        /// <summary>
        /// This method will used to bind the api to the view with the data returning from the api.
        /// </summary>
        /// <returns></returns>
        public async void BindData(int candidateId)
        {
            try
            {
                // Checking the internet connection whether user is connected to the internet or not.

                if (_base.CheckConnection())
                {
                    // displaying the loading circle to the user.
                    await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                    // Calling of the api for getting the result.
                    var result = await _base.GetCandidateDetails(CommonLib.MainUrl + ApiUrl.CandidateDetails + candidateId);
                    foreach (var item in result)
                    {
                        if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            if (item.Key != null) // if api returns data successfully.
                            {
                                FirstName = item.Key.firstName;
                                LastName = item.Key.lastName;
                                ID = item.Key.candidateID.ToString();
                                FullName = item.Key.firstName + " " + item.Key.lastName;
                                Employer = item.Key.employer;
                                EmployerType = item.Key.contractType;
                                Status = item.Key.status;
                                Mobile = item.Key.mobile;
                                Email = item.Key.email;
                                if (!string.IsNullOrEmpty(item.Key.status))
                                    StatusImage = item.Key.status.ToLower() == "active" ? "green_circle.png" : "red_circle.png";
                                PhoneNumberVisible = string.IsNullOrEmpty(Mobile) ? false : true;
                                EmailVisible = string.IsNullOrEmpty(Email) ? false : true;
                                CandidateProfile = item.Key.candidateProfile;
                                ResumeId = item.Key.resumeID.ToString();
                                var lastEmployer = !string.IsNullOrEmpty(item.Key.employer) ? item.Key.employer.Split(';') : null;
                                LastEmployer = lastEmployer != null ? lastEmployer[0] : string.Empty;
                                if (item.Key.addresses != null)
                                {
                                    for (int i = 0; i < item.Key.addresses.Count(); i++)
                                    {
                                        if (string.IsNullOrEmpty(Address))
                                            Address = item.Key.addresses[i].type == "Address" ? item.Key.addresses[i].street1 + " " + item.Key.addresses[i].autoSuburb + " " + item.Key.addresses[i].region + " " + item.Key.addresses[i].postCode : string.Empty;
                                        if (string.IsNullOrEmpty(HomeAddress))
                                            HomeAddress = item.Key.addresses[i].type == "Home" ? item.Key.addresses[i].street1 + " " + item.Key.addresses[i].autoSuburb + " " + item.Key.addresses[i].region + " " + item.Key.addresses[i].postCode : string.Empty;
                                        if (string.IsNullOrEmpty(WorkAddress))
                                            WorkAddress = item.Key.addresses[i].type == "Work" ? item.Key.addresses[i].street1 + " " + item.Key.addresses[i].autoSuburb + " " + item.Key.addresses[i].region + " " + item.Key.addresses[i].postCode : string.Empty;

                                    }
                                }
                                AddressVisible = string.IsNullOrEmpty(Address) ? false : true;
                                if (!string.IsNullOrEmpty(Address))
                                {
                                    var latLongResult = await CommonLib.GetLatLong(Address);
                                    foreach (var latLongItem in latLongResult)
                                    {
                                        Lat = latLongItem.Key;
                                        Long = latLongItem.Value;
                                    }
                                }
                                string[] coreRoles = item.Key.coreRole != null ? item.Key.coreRole.Split(';').Where(a => !string.IsNullOrEmpty(a)).ToArray() : null;
                                var items = new ObservableCollection<CategoriesList>();
                                if (coreRoles != null)
                                {
                                    for (int i = 0; i < coreRoles.Length; i++)
                                    {
                                        items.Add(new CategoriesList { Category = coreRoles[i] });
                                    }
                                }

                                CoreList = items;

                                CoreListHeight = items.Count * 20;

                                RaisePropertyChanged(nameof(CoreList));
                                // closing the loading popup.
                                LoaderPopup.CloseAllPopup();
                            }

                            else //if api is not returning the data.
                            {
                                CommonLib.ApiNotReturningData();
                            }
                        }
                        else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                        {
                            CommonLib.ApiResponseSlow();
                        }
                        else
                        {
                            CommonLib.Unauthorize();
                        }
                    }
                }

            }
            catch (Exception)
            {
                LoaderPopup.CloseAllPopup(); // closing the loading popup.
            }
            finally
            {

            }

        }

        /// <summary>
        /// this method will be used for the calling the phone number.
        /// </summary>
        public Command callCommand
        {
            get
            {
                return new Command((e) =>
                {
                    if (!string.IsNullOrEmpty(Mobile))
                    {
                        Device.OpenUri(new Uri(string.Format("tel:{0}", Mobile)));
                    }

                });
            }
        }

        /// <summary>
        /// this method will be used for performing the email functionality.
        /// </summary>
        public Command emailCommand
        {
            get
            {
                return new Command((e) =>
                {

                    if (!string.IsNullOrEmpty(Email))
                    {
                        Device.OpenUri(new Uri(string.Format("mailto:{0}", Email)));
                    }

                });
            }
        }

        /// <summary>
        /// This tapped event will be used for opening the map.
        /// </summary>
        public Command addressCommand
        {
            get
            {
                return new Command(async (e) =>
                {

                    if (!string.IsNullOrEmpty(Address))
                    {
                        //Models.Address
                        await navigation.PushAsync(new mapView(Lat, Long, Address));
                    }

                });
            }
        }

        /// <summary>
        /// This tapped event will be used for opening the notes page.
        /// </summary>
        public Command notesCommand
        {
            get
            {
                return new Command((e) =>
                {
                    navigation.PushAsync(new NoteslistPage(CandidateID,"Candidate"));

                });
            }
        }

        /// <summary>
        /// This tapped event will be used for opening the cv popup.
        /// </summary>
        public Command cvCommand
        {
            get
            {
                return new Command((e) =>
                {
                    navigation.PushPopupAsync(new CvPopup(ResumeId, "Candidate"));

                });
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void CollectionDidChange(object sender,
                                NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }




}
