﻿using Recruitlive.Models;
using Recruitlive.Repo;
using Recruitlive.Views;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Extended;

namespace Recruitlive.ViewModels
{
    public class TimesheetApproveViewModel : INotifyPropertyChanged
    {
        HttpClientBase _base = new HttpClientBase();
        public event PropertyChangedEventHandler PropertyChanged;
        public int pageindex = 1;
        public int pageSize = 7;
        public bool HitinProcess = false;
        public InfiniteScrollCollection<TimesheetListModel> TimesheettList { get; set; }

        /// <summary>
        /// property to show need to loading again or not.
        /// </summary>
        public bool _isLoadingMore;
        public bool IsLoadingMore
        {
            get
            {
                return _isLoadingMore;
            }
            set
            {
                _isLoadingMore = value;
                OnPropertyChanged(nameof(IsLoadingMore));
            }
        }

        /// <summary>
        /// property for the specifying the sortcolumn.
        /// </summary>
        private string _sortColumn;
        public string SortColumn
        {
            get
            {
                return _sortColumn;
            }
            set
            {
                _sortColumn = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SortColumn"));
            }
        }

        /// <summary>
        /// property for the specifying the sortdirection.
        /// </summary>
        private string _sortDirection;
        public string SortDirection
        {
            get
            {
                return _sortDirection;
            }
            set
            {
                _sortDirection = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SortDirection"));
            }
        }

        public string _noData;

        public string NoData
        {
            get
            {
                return _noData;
            }
            set
            {
                _noData = value;
                PropertyChanged(this, new PropertyChangedEventArgs("NoData"));
            }
        }

        public bool _noDataVisible;
        public bool NoDataVisible
        {
            get
            {
                return _noDataVisible;
            }
            set
            {
                _noDataVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("NoDataVisible"));
            }
        }

        public TimesheetApproveViewModel()
        {
            _sortColumn = "EmployeeName";
            _sortDirection = "Asc";
            BindData();
        }


        /// <summary>
        /// This method will used to bind the api to the view with the data returning from the api.
        /// </summary>
        /// <returns></returns>
        public void BindData()
        {

            TimesheettList = new InfiniteScrollCollection<TimesheetListModel>
            {
                OnLoadMore = async () =>
                {
                    var items = new InfiniteScrollCollection<TimesheetListModel>();

                    // Checking the internet connection whether user is connected to the internet or not.
                    if (_base.CheckConnection())
                    {

                        if (!HitinProcess)
                        {
                            HitinProcess = true;
                            IsLoadingMore = true;

                            var result = await _base.GetTimeSheet(CommonLib.MainUrl + ApiUrl.GetTimeSheet + pageSize + "/" + pageindex + "/" + SortColumn + "/" + SortDirection);

                            foreach (var item in result)
                            {
                                if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    if (item.Key.Count > 0) // if api returns data successfully.
                                    {
                                        foreach (var response in result.Keys)
                                        {
                                            if (response != null && response.Count > 0)
                                            {
                                                pageindex++;
                                                items = GetItems(true, response); // calling method to set data in listview
                                                IsLoadingMore = false;
                                                HitinProcess = false;
                                                NoDataVisible = false;
                                            }
                                            else
                                            {
                                                if (HitinProcess)
                                                {
                                                    NoData = "No Data Found";
                                                    NoDataVisible = true;
                                                }
                                                HitinProcess = true;
                                                IsLoadingMore = false;
                                            }
                                        }
                                    }
                                }
                                else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                                {
                                    CommonLib.ApiResponseSlow();
                                }
                                else
                                {
                                    CommonLib.Unauthorize();
                                }
                            }
                            IsLoadingMore = false;
                        }
                    }
                    return items;
                }
            };
            TimesheettList.LoadMoreAsync();
        }

        /// <summary>
        /// Method for setting the data in the list view.
        /// </summary>
        /// <param name="clearList"></param>
        /// <param name="noteItemList"></param>
        /// <returns></returns>
        InfiniteScrollCollection<TimesheetListModel> GetItems(bool clearList, List<TimeSheetGetData> timesheetData)
        {
            InfiniteScrollCollection<TimesheetListModel> items = null;
            try
            {
                if (clearList || TimesheettList == null)
                {
                    items = new InfiniteScrollCollection<TimesheetListModel>();
                }
                else
                {
                    items = new InfiniteScrollCollection<TimesheetListModel>(TimesheettList);
                }


                // setting all the data into the list view by getting from the api.
                foreach (var Item in timesheetData)
                {
                    items.Add(new TimesheetListModel
                    {
                        employeeName = Item.employeeName,
                        emmployeeID = "Id " + Item.timesheetID,
                        date = Item.date,
                        dateSubmitted = Item.dateSubmitted.HasValue==true?Item.dateSubmitted.Value.ToString("dd MMMM yyyy"):string.Empty,
                        breakTime = Item.breakTime,
                        comments = Item.comments,
                        leaveType = Item.leaveType,
                        timesheetTime = Item.timesheetTime,
                        workedHours = Item.workedHours,
                        CommentText = string.Empty
                    });
                }
                TimesheettList.CollectionChanged += CollectionDidChange;
                RaisePropertyChanged(nameof(TimesheettList));
            }
            catch (Exception)
            {

            }
            return items;
        }

        public Command Reject_TimeSheet_Tapped
        {
            get
            {
                return new Command(async (e) =>
                {
                    TimesheetListModel _model = e as TimesheetListModel;
                    if (_model != null)
                    {
                        if (string.IsNullOrEmpty(_model.CommentText))
                        {
                            await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please add comment.", "OK");
                        }
                        else
                        {
                            var timesheetId = _model.emmployeeID.Split(' ');
                            ApproveTimeSheet _passJson = new ApproveTimeSheet();
                            _passJson.TimesheetIds = timesheetId[1];
                            _passJson.Comment = _model.CommentText;
                            RejectTimeSheet(_passJson);
                        }
                    }
                });
            }
        }

        public Command Approve_TimeSheet_Tapped
        {
            get
            {
                return new Command((e) =>
                {
                    TimesheetListModel _model = e as TimesheetListModel;
                    string msg = string.Empty;
                    if (_model != null)
                    {
                        var timesheetId = _model.emmployeeID.Split(' ');
                        ApproveTimeSheet _passJson = new ApproveTimeSheet();
                        _passJson.TimesheetIds = timesheetId[1];
                        _passJson.Comment = _model.CommentText;
                        ApproveTimeSheet(_passJson);
                    }
                });
            }
        }


        /// <summary>
        /// this is method that will perform the Approval of the timesheet with the help of the api.
        /// </summary>
        /// <returns></returns>
        public async void ApproveTimeSheet(ApproveTimeSheet _timesheet)
        {
            try
            {
                // Checking the internet connection whether user is connected to the internet or not.
                if (_base.CheckConnection())
                {
                    // displaying the loading circle to the user.
                    await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                    // Calling of the api for getting the result.
                    var result = await _base.ApproveTimeSheet(CommonLib.MainUrl + ApiUrl.ApproveTimeSheet, _timesheet);
                    if (result == "true") // if api returns data successfully.
                    {
                        LoaderPopup.CloseAllPopup();  // closing the loading popup.
                        await App.Current.MainPage.DisplayAlert("Recruitlive", "Timesheet Approved Successfully", "OK");
                        HitinProcess = false;
                        pageindex = 1;
                        BindData();
                    }

                    else
                    {
                        CommonLib.ApiNotReturningData();
                    }
                }
            }
            catch (Exception)
            {
                LoaderPopup.CloseAllPopup();  // closing the loading popup.
            }
            finally
            {

            }
        }


        /// <summary>
        /// this is method that will perform the rejection of the timesheet with the help of the api.
        /// </summary>
        /// <returns></returns>
        public async void RejectTimeSheet(ApproveTimeSheet _timesheet)
        {
            try
            {
                // Checking the internet connection whether user is connected to the internet or not.
                if (_base.CheckConnection())
                {
                    // displaying the loading circle to the user.
                    await App.Current.MainPage.Navigation.PushPopupAsync(new LoaderPopup());

                    // Calling of the api for getting the result.
                    var result = await _base.RejectTimeSheet(CommonLib.MainUrl + ApiUrl.RejectTimeSheet, _timesheet);
                    if (result == "true") // if api returns data successfully.
                    {
                        LoaderPopup.CloseAllPopup();  // closing the loading popup.
                        await App.Current.MainPage.DisplayAlert("Recruitlive", "Timesheet Rejected Successfully", "OK");
                        HitinProcess = false;
                        pageindex = 1;
                        BindData();
                    }

                    else
                    {
                        CommonLib.ApiNotReturningData();
                    }
                }
            }
            catch (Exception)
            {
                LoaderPopup.CloseAllPopup();  // closing the loading popup.
            }
            finally
            {

            }
        }


        public void CollectionDidChange(object sender,
                                NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }



    }
}
