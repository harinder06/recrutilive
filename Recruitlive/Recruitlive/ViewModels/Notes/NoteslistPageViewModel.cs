﻿using Recruitlive.Models;
using Recruitlive.Repo;
using Recruitlive.Views;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms.Extended;

namespace Recruitlive.ViewModels
{
    /// <summary>
    /// this view model will be used for the note list view binding 
    /// </summary>
    public class NoteslistPageViewModel : INotifyPropertyChanged
    {
        HttpClientBase _base = new HttpClientBase();

        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<NoteListItem> NoteList { get; set; }

        public int candidateID;
        public string moduleName=String.Empty;
        public static int totalcount = 0;
        public bool IsFirstHit = false;
        public static int getEventCount = 0;
        public int pageindex = 1;
        public int pageSize = 5;
        public bool HitinProcess = false;

        public InfiniteScrollCollection<NoteListItem> NotesList { get; set; }

        /// <summary>
        /// property to show need to loading again or not.
        /// </summary>
        public bool _isLoadingMore;
        public bool IsLoadingMore
        {
            get
            {
                return _isLoadingMore;
            }
            set
            {
                _isLoadingMore = value;
                OnPropertyChanged(nameof(IsLoadingMore));
            }
        }


        public NoteslistPageViewModel(int candidateId,string _moduleName)
        {
            candidateID = candidateId;
            moduleName = _moduleName;
            BindData();

        }

        /// <summary>
        /// This method will used to bind the api to the view with the data returning from the api.
        /// </summary>
        /// <returns></returns>
        public void BindData()
        {

            NotesList = new InfiniteScrollCollection<NoteListItem>
            {
                OnLoadMore = async () =>
                {
                    var items = new InfiniteScrollCollection<NoteListItem>();

                    // Checking the internet connection whether user is connected to the internet or not.
                    if(_base.CheckConnection())
                    {
                        // total no of records not exceed from the current records
                        if (totalcount > getEventCount && getEventCount != 0 || IsFirstHit == false)
                        {
                            if (!HitinProcess)
                            {
                                HitinProcess = true;
                                IsLoadingMore = true;

                                var result = await _base.GetNotes(CommonLib.MainUrl + ApiUrl.NotesList + pageSize + "/" + pageindex + "/"+moduleName+"/" + candidateID);

                                foreach (var item in result)
                                {
                                    if (item.Value.StatusCode == System.Net.HttpStatusCode.OK)
                                    {
                                        if (item.Key != null) // if api returns data successfully.
                                        {
                                            foreach (var response in result.Keys)
                                            {
                                                if (response != null && response.records.Count != 0)
                                                {
                                                    pageindex++;
                                                    IsFirstHit = true;
                                                    totalcount = response.totalRecords;
                                                    getEventCount = response.records.Count;

                                                    items = GetItems(true, response); // calling method to set data in listview
                                                    IsLoadingMore = false;
                                                    HitinProcess = false;
                                                }
                                                else
                                                {
                                                    getEventCount = 0;
                                                    HitinProcess = false;
                                                    IsLoadingMore = false;
                                                }

                                            }
                                        }
                                    }
                                    else if (item.Value.StatusCode == System.Net.HttpStatusCode.BadGateway)
                                    {
                                        CommonLib.ApiResponseSlow();
                                    }
                                    else
                                    {
                                        CommonLib.Unauthorize();
                                    }
                                }
                                IsLoadingMore = false;
                            }
                        }
                    }
                    return items;
                }
            };
            NotesList.LoadMoreAsync();
        }


        /// <summary>
        /// Method for setting the data in the list view.
        /// </summary>
        /// <param name="clearList"></param>
        /// <param name="noteItemList"></param>
        /// <returns></returns>
        InfiniteScrollCollection<NoteListItem> GetItems(bool clearList, NotesMainList noteItemList)
        {
            InfiniteScrollCollection<NoteListItem> items = null;
            try
            {
                if (clearList || NoteList == null)
                {
                    items = new InfiniteScrollCollection<NoteListItem>();
                }
                else
                {
                    items = new InfiniteScrollCollection<NoteListItem>(NoteList);
                }


                // setting all the data into the list view by getting from the api.
                foreach (var Item in noteItemList.records)
                {
                    items.Add(new NoteListItem
                    {
                        NoteId = Convert.ToInt32(Item.noteID),
                        Title = Item.category,
                        SubTitle = Item.content,
                        PostedBy = Item.userNameBranch,
                        PostedOn = Item.dateMade.ToString("MMMM dd, yyyy"),

                    });
                }
            }
            catch (Exception)
            {

            }
            return items;
        }

        public void CollectionDidChange(object sender,
                              NotifyCollectionChangedEventArgs e)
        {

        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }




    }
}
