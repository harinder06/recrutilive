﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitlive.DependencyInterface
{
    /// <summary>
    /// this interface will help to generate the pdf in the candidate section for the resume.
    /// </summary>
    public interface IGeneratePDF
    {
        string SaveFiles(string filename,byte[] bytes);
    }
}
