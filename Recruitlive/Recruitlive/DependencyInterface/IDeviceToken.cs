﻿namespace Recruitlive.DependencyInterface
{
    /// <summary>
    /// this interface is used to get all the unique number to send into the login api on each of platform.
    /// it will return a string from each platform.
    /// </summary>
    public interface IDevice
    {
        string GetIdentifier();
    }
}
