﻿namespace Recruitlive.DependencyInterface
{
    public interface IIPAddressManager
    {
        string GetIPAddress();
        string GetVersion();
    }
}
