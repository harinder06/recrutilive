﻿using Plugin.Connectivity;
using Recruitlive.Models;
using Recruitlive.ViewModels;
using Recruitlive.ViewModels.Candidate;
using Recruitlive.ViewModels.Contact;
using Recruitlive.Views;
using Recruitlive.Views.Contact;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Recruitlive.Repo
{
    public class CommonLib
    {
        static HttpClientBase _base = new HttpClientBase();
        // The main url of the api.
        public static string MainUrl = string.Empty;

        /// <summary>
        /// This is the method that will check the connectivity of the internet and returns the response true if internet is
        /// connected or false if the internet is not connected.
        /// </summary>
        /// <returns></returns>
        public static bool checkconnection()
        {
            var con = CrossConnectivity.Current.IsConnected;
            return con == true ? true : false; // returning the bool value either true or false based on connectivity.
        }


        /// <summary>
        /// Method that will used in the settings section to check whether the entered url is valid or not.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static bool CheckValidUrl(string url)
        {
            Uri uri = null;
            if (!Uri.TryCreate(url, UriKind.Absolute, out uri) || null == uri)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Method that will check whether the given email is valid or not.
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public static bool CheckValidEmail(string Email)
        {
            bool isEmail = Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            return isEmail;
        }

        /// <summary>
        /// Method that will returns the latitude and longitude for the given address.
        /// </summary>
        /// <param name="Address"></param>
        /// <returns></returns>
        public static async Task<Dictionary<double, double>> GetLatLong(string Address)
        {
            Dictionary<double, double> _returnData = new Dictionary<double, double>();
            Geocoder _coder = new Geocoder();
            var data = await _coder.GetPositionsForAddressAsync(Address);
            foreach (Position pos in data)
            {
                _returnData.Add(pos.Latitude,pos.Longitude);
            }
            return _returnData;
        }

        /// <summary>
        /// Method that will used to allow the user to logout from the application when the unauthorized response
        /// from the api returns.
        /// </summary>

        public async static void Unauthorize()
        {
            await App.Current.MainPage.DisplayAlert("Recruitlive", "Session expires, Please login again to continue.", "OK");

            // calling the clear details method from the common library that will clear all the details of the logge in user.
            ClearAllData();

            // presenting the login page to the user.
            await Application.Current.MainPage.Navigation.PushAsync(new LoginPage());
        }

        /// <summary>
        /// method that will use to clear all the data on logout.
        /// </summary>
        public static void ClearAllData()
        {
            App.Database.ClearLoginDetails();
            App.Database.ClearCandidateFilterDetails();
            App.Database.ClearClientFilterDetails();
            App.Database.ClearContactFilterDetails();
            CandidateAdvancedSearchBar1.SelectedRoles = null;
            CandidateAdvancedSearchBar1.SelectedSkills = null;
            CandidateAdvancedSearchBar1.getRoles = string.Empty;
            CandidateAdvancedSearchBar1.getSkills = string.Empty;
            CandidateAdvancedSearchBar1._loadTypeData = new ObservableCollection<string>();
            CandidateAdvancedSearchBar1._loadStatusData = new ObservableCollection<string>();
            CandidateAdvancedSearchBar1._listTypes = new List<CandidateTypes>();
            CandidateAdvancedSearchBar1._listStatus = new List<CandidateTypes>();
            ClientAdvancedSearchList._loadTypeData = new ObservableCollection<string>();
            ClientAdvancedSearchList._loadStatusData = new ObservableCollection<string>();
            ClientAdvancedSearchList._listTypes = new List<CandidateTypes>();
            ClientAdvancedSearchList._listStatus = new List<CandidateTypes>();
            AdvancedContactSearch.SelectedRoles = null;
            AdvancedContactSearch.SelectedSkills = null;
            AdvancedContactSearch.getRoles = string.Empty;
            AdvancedContactSearch.getSkills = string.Empty;
            ContactRolesListViewModel._loadRolesData = new List<BindData>();
            ContactSkillsListViewModel._loadSkillsData = new List<BindData>();
            RolesListViewModel._loadRolesData=new List<BindData>();
            SkillsListViewModel._loadSkillsData = new List<BindData>();
        }

        /// <summary>
        /// This method will run when the api not returns any data
        /// </summary>
        public static async void ApiNotReturningData()
        {
            LoaderPopup.CloseAllPopup(); // closing the loading popup.
            await App.Current.MainPage.DisplayAlert("Recruitlive", "Something went wrong Please try again later.", "OK");
        } 

        /// <summary>
        /// This method will be used when the response from the api is not coming on fast basis.
        /// </summary>
        public static async void ApiResponseSlow()
        {
            LoaderPopup.CloseAllPopup(); // closing the loading popup.
            await App.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
        }
    }
}
