﻿using Newtonsoft.Json;
using Plugin.Geolocator;
using Recruitlive.Models;
using Recruitlive.Models.Candidate;
using Recruitlive.Models.Contact;
using Recruitlive.Models.Job;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Recruitlive.Repo
{
    public class HttpClientBase : HttpClient
    {
        private static readonly HttpClientBase _instance = new HttpClientBase();
        CancellationTokenSource cts;
        SaveLoginResponse getLoginDetails;
        static HttpClientBase()
        {
            GetLocation();
        }
        public HttpClientBase() : base()
        {
            // Getting the Login details stored in the local DB.
            getLoginDetails = App.Database.GetLoginUser();

            TimeSpan time = new TimeSpan(0, 0, 60);
            Timeout = time;
            cts = new CancellationTokenSource();
            cts.CancelAfter(time);
            DefaultRequestHeaders.Add("DeviceId", App.deviceToken);
            DefaultRequestHeaders.Add("OSType", Device.RuntimePlatform);
            DefaultRequestHeaders.Add("OSVersion", App.osVersion);
            DefaultRequestHeaders.Add("Longitude", App.longitude);
            DefaultRequestHeaders.Add("Latitude", App.latitude);
            DefaultRequestHeaders.Add("IPAddress", App.ipAddress);
        }

        /// <summary>
        /// Method that will return the current lat and long.
        /// </summary>
        /// <returns></returns>
        public async static void GetLocation()
        {
            try
            {
                var locator = CrossGeolocator.Current;
                if (locator.IsGeolocationEnabled)
                {
                    locator.DesiredAccuracy = 50;
                    var position = await locator.GetPositionAsync();
                    if (position != null)
                    {
                        App.latitude = position.Latitude.ToString();
                        App.longitude = position.Longitude.ToString();
                    }
                }

            }
            catch (Exception)
            {

            }
        }

        public bool CheckConnection()
        {
            bool check;
            if (!CommonLib.checkconnection())
            {
                App.Current.MainPage.DisplayAlert("Recruitlive", "Please check your internet connection", "OK");
                check = false;
            }
            else
            {
                check = true;
            }
            return check;
        }

        public async Task<string> GetMethod(string url)
        {
            string response = string.Empty;
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                // getting the details
                var result = await GetAsync(new Uri(url));
                if (result.IsSuccessStatusCode)
                {
                    response = result.Content.ReadAsStringAsync().Result;
                }
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return response;
        }
        public static HttpClientBase Instance
        {
            get
            {
                return _instance;
            }
        }


        #region Login LogOut

        /// <summary>
        /// This is the method that will perform the login operation in the application.
        /// we are sending the post data here which includes company name, user name, branch, grant_type and password.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        public async Task<LoginResponse> Login(string url, string postData)
        {
            // making the object of the response class which will use for deserialization of the response.
            LoginResponse objData = new LoginResponse();
            try
            {
                StringContent str = new StringContent(postData, Encoding.UTF8, "application/x-www-form-urlencoded");
                var result = await PostAsync(new Uri(url), str);
                var place = result.Content.ReadAsStringAsync().Result;
                // deserialization of the response returning from the api
                objData = JsonConvert.DeserializeObject<LoginResponse>(await result.Content.ReadAsStringAsync());

            }

            catch (Exception) { }
            finally { }
            return objData; // returning the object of the response
        }       

        #endregion

        #region Client

        /// <summary>
        /// This is the method that will give the list of the searched client based on the filters apply
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        public async Task<Dictionary<ClientSearchModel, HttpResponseMessage>> SearchClient(string url, ClientSearchPost _postData)
        {
            // making the object of the response class which will use for deserialization of the response.
            ClientSearchModel objData = new ClientSearchModel();
            Dictionary<ClientSearchModel, HttpResponseMessage> _returnData = new Dictionary<ClientSearchModel, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var json = JsonConvert.SerializeObject(_postData);
                StringContent str = new StringContent(json, Encoding.UTF8, "application/json");
                var result = await PostAsync(new Uri(url), str);

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the response returning from the api
                    objData = JsonConvert.DeserializeObject<ClientSearchModel>(content);
                }
                _returnData.Add(objData, result);
                // deserialization of the response returning from the api               

            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnData; // returning the object of the response
        }


        /// <summary>
        /// This is the method that will give the list of the candidate with paging and sorting
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<ClientSearchModel, HttpResponseMessage>> ClientPagingSorting(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            ClientSearchModel objData = new ClientSearchModel();
            Dictionary<ClientSearchModel, HttpResponseMessage> _returnData = new Dictionary<ClientSearchModel, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var result = await GetAsync(new Uri(url));

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the response returning from the api
                    objData = JsonConvert.DeserializeObject<ClientSearchModel>(content);
                }
                _returnData.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnData; // returning the object of the response
        }

        /// <summary>
        /// This is the method that will get all the details for the client based on the client id.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<ClientDetails, HttpResponseMessage>> GetClientDetails(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            ClientDetails objData = new ClientDetails();

            Dictionary<ClientDetails, HttpResponseMessage> _returnResult = new Dictionary<ClientDetails, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var result = await GetAsync(url);

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the data returning from the api.
                    objData = JsonConvert.DeserializeObject<ClientDetails>(content);
                }

                _returnResult.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnResult; // returning the object of response.
        }
        #endregion

        #region Contact

        /// <summary>
        /// This is the method that will give the list of the searched contact based on the filters apply
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        public async Task<Dictionary<ContactSearchModel, HttpResponseMessage>> SearchContact(string url, ContactSearchPost _postData)
        {
            // making the object of the response class which will use for deserialization of the response.
            ContactSearchModel objData = new ContactSearchModel();
            Dictionary<ContactSearchModel, HttpResponseMessage> _returnData = new Dictionary<ContactSearchModel, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var json = JsonConvert.SerializeObject(_postData);
                StringContent str = new StringContent(json, Encoding.UTF8, "application/json");
                var result = await PostAsync(new Uri(url), str);

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the response returning from the api
                    objData = JsonConvert.DeserializeObject<ContactSearchModel>(content);
                }
                _returnData.Add(objData, result);
                // deserialization of the response returning from the api               

            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnData; // returning the object of the response
        }


        /// <summary>
        /// This is the method that will give the list of the candidate with paging and sorting
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<ContactSearchModel, HttpResponseMessage>> ContactPagingSorting(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            ContactSearchModel objData = new ContactSearchModel();
            Dictionary<ContactSearchModel, HttpResponseMessage> _returnData = new Dictionary<ContactSearchModel, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var result = await GetAsync(new Uri(url));

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the response returning from the api
                    objData = JsonConvert.DeserializeObject<ContactSearchModel>(content);
                }
                _returnData.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnData; // returning the object of the response
        }


        /// <summary>
        /// This is the method that will return the contact detail of a contact person based on the contact id.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<ContactDetails, HttpResponseMessage>> GetContactDetails(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            ContactDetails objData = new ContactDetails();

            Dictionary<ContactDetails, HttpResponseMessage> _returnResult = new Dictionary<ContactDetails, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var result = await GetAsync(url);

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the data returning from the api.
                    objData = JsonConvert.DeserializeObject<ContactDetails>(content);
                }

                _returnResult.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnResult; // returning the object of response.
        }

        #endregion

        #region Job

        /// <summary>
        /// This is the method that will give the list of the searched job based on the filters apply
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        public async Task<Dictionary<JobSearchModel, HttpResponseMessage>> SearchJob(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            JobSearchModel objData = new JobSearchModel();
            Dictionary<JobSearchModel, HttpResponseMessage> _returnData = new Dictionary<JobSearchModel, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var result = await GetAsync(new Uri(url));

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the response returning from the api
                    objData = JsonConvert.DeserializeObject<JobSearchModel>(content);
                }
                _returnData.Add(objData, result);
                // deserialization of the response returning from the api               

            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnData; // returning the object of the response
        }

        /// <summary>
        /// This is the method that will give the list of the job with paging and sorting
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<JobSearchModel, HttpResponseMessage>> JobPagingSorting(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            JobSearchModel objData = new JobSearchModel();
            Dictionary<JobSearchModel, HttpResponseMessage> _returnData = new Dictionary<JobSearchModel, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var result = await GetAsync(new Uri(url));
                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the response returning from the api
                    objData = JsonConvert.DeserializeObject<JobSearchModel>(content);
                }
                _returnData.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnData; // returning the object of the response
        }


        /// <summary>
        /// This is the method that is getting all the job details based on the job id.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<JobDetails, HttpResponseMessage>> GetJobDetails(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            JobDetails objData = new JobDetails();

            Dictionary<JobDetails, HttpResponseMessage> _returnResult = new Dictionary<JobDetails, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var result = await GetAsync(url);

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the data returning from the api.
                    objData = JsonConvert.DeserializeObject<JobDetails>(content);
                }
                _returnResult.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnResult; // returning the object of response.
        }


        /// <summary>
        /// This is the method that will give the list of the job applicants
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<List<JobApplicantGetData>, HttpResponseMessage>> JobApplicants(string url, JobApplicantPostData _postData)
        {
            // making the object of the response class which will use for deserialization of the response.
            List<JobApplicantGetData> objData = new List<JobApplicantGetData>();
            Dictionary<List<JobApplicantGetData>, HttpResponseMessage> _returnData = new Dictionary<List<JobApplicantGetData>, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var json = JsonConvert.SerializeObject(_postData);
                StringContent str = new StringContent(json, Encoding.UTF8, "application/json");
                var result = await PostAsync(new Uri(url), str);
                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the response returning from the api
                    objData = JsonConvert.DeserializeObject<List<JobApplicantGetData>>(content);
                }
                _returnData.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnData; // returning the object of the response
        }       

        #endregion

        #region Notes

        /// <summary>
        /// This is the method that is getting all the note details based on the note id.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<NoteDetails, HttpResponseMessage>> GetNoteDetails(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            NoteDetails objData = new NoteDetails();
            HttpResponseMessage result;
            Dictionary<NoteDetails, HttpResponseMessage> _returnResult = new Dictionary<NoteDetails, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                result = await GetAsync(url);

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the data returning from the api.
                    objData = JsonConvert.DeserializeObject<NoteDetails>(content);
                }
                _returnResult.Add(objData, result);

            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnResult; // returning the object of response.
        }

        /// <summary>
        /// This is the method that will perform the login operation in the application.
        /// we are sending the post data here which includes company name, user name, branch, grant_type and password.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, HttpResponseMessage>> AddNotes(string url, AddNotesModel _model)
        {
            string response = string.Empty;
            HttpResponseMessage result;
            Dictionary<string, HttpResponseMessage> _returnResult = new Dictionary<string, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                string json = JsonConvert.SerializeObject(_model);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
                result = await PostAsync(new Uri(url), stringContent);
                if (result.IsSuccessStatusCode)
                {
                    response = result.Content.ReadAsStringAsync().Result;
                }
                _returnResult.Add(response, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnResult; // returning the object of the response
        }


        /// <summary>
        /// This is the method that will give the list of the searched candidate based on the filters apply
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        public async Task<Dictionary<NotesMainList, HttpResponseMessage>> GetNotes(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            NotesMainList objData = new NotesMainList();
            Dictionary<NotesMainList, HttpResponseMessage> _returnResult = new Dictionary<NotesMainList, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);

                var result = await GetAsync(new Uri(url));

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the response returning from the api
                    objData = JsonConvert.DeserializeObject<NotesMainList>(content);
                }
                _returnResult.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnResult; // returning the object of the response
        }

        #endregion

        #region Timesheet

        /// <summary>
        /// This is the method that will reject the timesheet.
        /// In theis method the timesheet is rejected with the comment and the timesheet id. The api 
        /// will return the resonse as true if the timesheet rejected successfully.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        public async Task<string> RejectTimeSheet(string url, ApproveTimeSheet _data)
        {
            string response = string.Empty;
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var json = JsonConvert.SerializeObject(_data);
                StringContent str = new StringContent(json, Encoding.UTF8, "application/json");
                var result = await PostAsync(new Uri(url), str);
                response = result.Content.ReadAsStringAsync().Result; // returning the response from the api
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return response;
        }

        /// <summary>
        /// This is the method that will give the list of the job applicants
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<List<TimeSheetGetData>, HttpResponseMessage>> GetTimeSheet(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            List<TimeSheetGetData> objData = new List<TimeSheetGetData>();
            Dictionary<List<TimeSheetGetData>, HttpResponseMessage> _returnData = new Dictionary<List<TimeSheetGetData>, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var result = await GetAsync(new Uri(url));
                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the response returning from the api
                    objData = JsonConvert.DeserializeObject<List<TimeSheetGetData>>(content);
                }
                _returnData.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnData; // returning the object of the response
        }


        /// <summary>
        /// This is the method that will approve the timesheet.
        /// In theis method the timesheet is approved with the comment and the timesheet id. The api 
        /// will return the response as true if the timesheet approved successfully.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        public async Task<string> ApproveTimeSheet(string url, ApproveTimeSheet _data)
        {
            string response = string.Empty;
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var json = JsonConvert.SerializeObject(_data);
                StringContent str = new StringContent(json, Encoding.UTF8, "application/json");
                var result = await PostAsync(new Uri(url), str);
                response = result.Content.ReadAsStringAsync().Result; // returning the response from the api
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return response;
        }

        #endregion

        #region Candidate

        /// <summary>
        /// This is the method that will give the list of the searched candidate based on the filters apply
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        public async Task<Dictionary<CandidateSearchModel, HttpResponseMessage>> SearchCandidate(string url, CandidateSearchPost _postData)
        {
            // making the object of the response class which will use for deserialization of the response.
            CandidateSearchModel objData = new CandidateSearchModel();
            Dictionary<CandidateSearchModel, HttpResponseMessage> _returnData = new Dictionary<CandidateSearchModel, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var json = JsonConvert.SerializeObject(_postData);
                StringContent str = new StringContent(json, Encoding.UTF8, "application/json");
                var result = await PostAsync(new Uri(url), str);
                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the response returning from the api
                    objData = JsonConvert.DeserializeObject<CandidateSearchModel>(content);
                }
                _returnData.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception ex) {
                throw ex;
            }
            finally { }
            return _returnData; // returning the object of the response
        }

        /// <summary>
        /// This is the method that will give the list of the candidate with paging and sorting
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<CandidateSearchModel, HttpResponseMessage>> CandidatePagingSorting(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            CandidateSearchModel objData = new CandidateSearchModel();
            Dictionary<CandidateSearchModel, HttpResponseMessage> _returnData = new Dictionary<CandidateSearchModel, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var result = await GetAsync(new Uri(url));
                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the response returning from the api
                    objData = JsonConvert.DeserializeObject<CandidateSearchModel>(content);
                }
                _returnData.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }

            finally { }
            return _returnData; // returning the object of the response
        }


        /// <summary>
        /// This is the method that will get all the details for the candidate based on the client id.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<CandidateDetails, HttpResponseMessage>> GetCandidateDetails(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            CandidateDetails objData = new CandidateDetails();

            Dictionary<CandidateDetails, HttpResponseMessage> _returnResult = new Dictionary<CandidateDetails, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var result = await GetAsync(url);

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the data returning from the api.
                    objData = JsonConvert.DeserializeObject<CandidateDetails>(content);
                }

                _returnResult.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnResult; // returning the object of response.
        }

        /// <summary>
        /// This is the method that will get all the types for the candidate.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<List<CandidateTypes>, HttpResponseMessage>> GetTypes(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            List<CandidateTypes> objData = new List<CandidateTypes>();

            Dictionary<List<CandidateTypes>, HttpResponseMessage> _returnResult = new Dictionary<List<CandidateTypes>, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var result = await GetAsync(url);

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the data returning from the api.
                    objData = JsonConvert.DeserializeObject<List<CandidateTypes>>(content);
                }
                _returnResult.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnResult; // returning the object of response.
        }

        /// <summary>
        /// This is the method that will get all the skills and roles for the candidate.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<List<CandidateRoles>, HttpResponseMessage>> GetRoles(string url)
        {
            // making the object of the response class which will use for deserialization of the response.
            List<CandidateRoles> objData = new List<CandidateRoles>();

            HttpResponseMessage result;

            Dictionary<List<CandidateRoles>, HttpResponseMessage> _returnResult = new Dictionary<List<CandidateRoles>, HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                result = await GetAsync(url);

                if (result.IsSuccessStatusCode)
                {
                    var stream = result.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    // deserialization of the data returning from the api.
                    objData = JsonConvert.DeserializeObject<List<CandidateRoles>>(content);
                }
                _returnResult.Add(objData, result);
            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnResult; // returning the object of response.
        }


        /// <summary>
        /// This is the method that will get all the resume data for the candidate.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public async Task<Dictionary<byte[], HttpResponseMessage>> GetResume(string url)
        {
            Dictionary<byte[], HttpResponseMessage> _returnResult = new Dictionary<byte[], HttpResponseMessage>();
            try
            {
                // sending the token in the header of the request.
                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", getLoginDetails.access_token);
                var result = await GetAsync(url);

                byte[] dataBuffer = await result.Content.ReadAsByteArrayAsync();

                _returnResult.Add(dataBuffer, result);

            }
            catch (TaskCanceledException)
            {
                if (!cts.Token.IsCancellationRequested)
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Recruitlive", "Please try again later as server is slow now.", "OK");
                }
            }
            catch (Exception) { }
            finally { }
            return _returnResult; // returning the object of response.
        }
        #endregion
    }
}
