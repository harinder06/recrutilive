﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitlive.Repo
{
    public class ApiUrl
    {
        #region Login LogOut

        public const string LoginUrl = "token";

        public const string LogOut = "user/logout";

        #endregion

        #region Candidate

        public const string CandidateDetails = "candidate/viewDetails/";

        public const string CandidateResume = "candidate/viewResume/";

        public const string CandidateListing = "candidate/list/";

        public const string CandidateListPaging = "candidate/listPaging/";

        public const string GetTypes = "candidate/getTypes";

        public const string GetStatus = "candidate/getStatus";

        #endregion

        #region Client

        public const string ClientListing = "client/getClientList/";

        public const string ClientListPaging = "client/listPaging/";

        public const string ClientViewDetails = "client/viewDetails/";

        public const string GetClientTypes = "client/getTypes";

        public const string GetClientStatus = "client/getStatus";

        #endregion

        #region Common

        public const string GetAllRoles = "common/getAllRoles";


        public const string GetAllSkills = "common/getAllSkills";

        #endregion

        #region Notes

        public const string CandidateNotesCategory = "note/getNoteCategory/Candidate";

        public const string SaveNotes = "note/saveNote";

        public const string NotesDetails = "note/GetNote/";

        public const string NotesList = "note/listPaging/";

        #endregion

        #region Job

        public const string JobListing = "job/getJobList/";

        public const string JobListPaging = "job/listPaging/";

        public const string JobDetails = "job/viewDetails/";

        public const string JobApplicantsDetails = "job/getJobAdResponses/";

        public const string JobApplicantsResume = "job/viewResume/";

        public const string JobApplicantsReject = "job/rejectJobApplications/";

        #endregion

        #region Contact

        public const string ContactListing = "contact/getContactList/";

        public const string ContactListPaging = "contact/listPaging/";

        public const string ContactDetails = "contact/viewDetails/";

        #endregion

        #region TimeSheet

        public const string GetTimeSheet = "timesheet/getTimesheetList/";

        public const string RejectTimeSheet= "timesheet/rejectInternalTS";

        public const string ApproveTimeSheet = "timesheet/approverInternalTS";

        #endregion

    }
}
