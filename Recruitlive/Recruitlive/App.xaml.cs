﻿using Recruitlive.Data;
using Recruitlive.DependencyInterface;
using Recruitlive.Models.Settings;
using Recruitlive.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Recruitlive
{
    public partial class App : Application
    {

        public static double ScreenHeight;
        public static double ScreenWidth;
        public static string deviceToken = string.Empty;
        public static string ipAddress = string.Empty;
        public static string longitude = string.Empty;
        public static string latitude = string.Empty;
        public static string osVersion = string.Empty;



        public App()
        {
           // Register Syncfusion license
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NjQxN0AzMTM2MmUzMjJlMzBPKzU1STRkNDA1Y29rWlhzSnQ4bHl2N0FJLzNFWFFnaFcxamswdHQ1cHFFPQ==");
            InitializeComponent();
            //navigation bar styling below code

            Current.Resources = new ResourceDictionary();
            Current.Resources.Add("UlycesColor", Color.FromHex("#19305e"));
            var navigationStyle = new Style(typeof(NavigationPage));
            var barTextColorSetter = new Setter { Property = NavigationPage.BarTextColorProperty, Value = Color.White };
            var barBackgroundColorSetter = new Setter { Property = NavigationPage.BarBackgroundColorProperty, Value = Color.FromHex("#19305e") };

            navigationStyle.Setters.Add(barTextColorSetter);
            navigationStyle.Setters.Add(barBackgroundColorSetter);

            Current.Resources.Add(navigationStyle);

            GetMainPage();

            deviceToken = DependencyService.Get<IDevice>().GetIdentifier();
            ipAddress = DependencyService.Get<IIPAddressManager>().GetIPAddress();
            osVersion = DependencyService.Get<IIPAddressManager>().GetVersion();


            SaveSettings getSettings = App.Database.GetSettings();
            if (!string.IsNullOrEmpty(getSettings.Url))
            {
                Repo.CommonLib.MainUrl = getSettings.Url + "/";
            }
            else
            {
                //Repo.CommonLib.MainUrl = "https://ppsapi.recruitlive.com.au/";
                Repo.CommonLib.MainUrl = "https://api.recruitlive.com.au/";
            }

        }

        /// <summary>
        /// Method that will return which page will be first page of the application.
        /// </summary>
        void GetMainPage()
        {
            try
            {
                // getting the details of the logged in user
                var getLoginDetails = App.Database.GetLoginUser();

                if (!string.IsNullOrEmpty(getLoginDetails.access_token))
                {
                    MainPage = new NavigationPage(new HomePage());
                }
                else
                {
                    MainPage = new NavigationPage(new LoginPage());
                }

            }
            catch (Exception ex)
            {

            }
            finally { }

        }

        private static Database _database;

        /// <summary>
        /// Getting the database path to be used statically in th entire application.
        /// </summary>
        public static Database Database
        {
            get
            {
                if (_database == null)
                {
                    try
                    {
                        // initialization of the database.
                        _database = new Database(DependencyService.Get<IFileHelper>().GetLocalFilePath("DBRecruitilive.db3"));
                    }
                    catch (Exception ex)
                    {
                    }
                    finally { }
                }
                return _database;
            }
        }



    }
}