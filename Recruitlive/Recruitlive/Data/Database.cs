﻿using Recruitlive.Models;
using Recruitlive.Models.Candidate;
using Recruitlive.Models.Client;
using Recruitlive.Models.Job;
using Recruitlive.Models.Settings;
using SQLite;
using System;
using System.Linq;

namespace Recruitlive.Data
{
    public class Database
    {
        /// <summary>
        /// Declaration part
        /// </summary>
        readonly SQLiteConnection database;

        /// <summary>
        /// Constructor part
        /// </summary>
        /// <param name="dbPath"></param>
        public Database(string dbPath)
        {
            try
            {
                // Intialization of the database while dbpath is getting from the platform specific code.
                database = new SQLiteConnection(dbPath);

                // Creation of the login table that will save all the details that will be retireved from the login response.
                database.CreateTable<SaveLoginResponse>();

                // Creation of the candidate filters that will used to retain the previous filters.
                database.CreateTable<CandidateFilters>();

                // Creation of the client filters that will used to retain the previous filters.
                database.CreateTable<ClientFilters>();

                // Creation of the contact filters that will used to retain the previous filters.
                database.CreateTable<ContactFilters>();


                // Creation of the settings table to get the main url for the api.
                database.CreateTable<SaveSettings>();
            }

            catch (Exception) { }
            finally { }
        }

        #region Login
        /// <summary>
        /// This is the method of get type which will return all the details of the login which we have stored 
        /// in the table when we logged in to the application.
        /// </summary>
        /// <returns></returns>
        public SaveLoginResponse GetLoginUser()
        {
            var user = new SaveLoginResponse();
            try
            {
                // getting the login details from the table
                user = database.Table<SaveLoginResponse>().First();
            }
            catch (Exception) { }
            finally { }
            return user; // returning the deatils of the logged in user.
        }

        /// <summary>
        /// this is the method that will store all the details of the logged in user into the table
        /// which will help further for getting all the details.
        /// </summary>
        /// <param name="objLoggedUser"></param>
        /// <returns></returns>
        public int SaveLoggedUser(SaveLoginResponse objLoggedUser)
        {
            int status = 0;
            try
            {
                // deleting all the records from the logged in table as if there is any record present.
                database.DeleteAll<SaveLoginResponse>();

                // saving the login details into the login table.
                status = database.Insert(objLoggedUser);
            }
            catch (Exception)
            {
                status = 0;
            }
            finally { }
            return status; // returning the status either 1 or 0.
        }

        /// <summary>
        /// This is the method that will clear all the details of logged in user from the table. 
        /// </summary>
        /// <returns></returns>
        public int ClearLoginDetails()
        {
            var status = 0;
            try
            {
                // getting all the data first from login table in order to perform delete operation.
                var data = database.Table<SaveLoginResponse>().ToList();

                foreach (var item in data)
                {
                    // deleting all the details from the login table.
                    status = database.Delete(item);
                }

            }
            catch (Exception)
            {
                status = 0;
            }
            finally { }
            return status; // returning the status either 0 or 1.
        }

        #endregion

        #region Candidate Filters
        /// <summary>
        /// this is the method that will store all the filters of the candidate in the table
        /// </summary>
        /// <param name="objCandidateFilters"></param>
        /// <returns></returns>
        public int SaveCandidateFilters(CandidateFilters objCandidateFilters)
        {
            int status = 0;
            try
            {
                // deleting all the records from the filters table as if there is any record present.
                database.DeleteAll<CandidateFilters>();

                // saving the filters records in the candidate filter table.
                status = database.Insert(objCandidateFilters);
            }
            catch (Exception)
            {
                status = 0;
            }
            finally { }
            return status; // returning the status either 1 or 0.
        }


        /// <summary>
        /// This is the method of getting the deatils of the filters based on the section.
        /// </summary>
        /// <returns></returns>
        public CandidateFilters GetCandidateFilters()
        {
            var user = new CandidateFilters();
            try
            {
                // getting the candiadte filters details from the table
                string query = "SELECT CF.FirstName,CF.LastName,CF.AutoCoreSkills,CF.City,CF.Sector,CF.AutoCoreRoles,CF.PostCode,CF.AutoStatus,CF.AutoType,CF.CurrentEmployer,CF.CandidateId,CF.Street,CF.MinSalary,CF.MaxSalary,CF.TypePicker,CF.StatusPicker,CF.BranchName,CF.Section,CF.UserName FROM CandidateFilters CF" +
                     " INNER JOIN SaveLoginResponse login on CF.UserName=login.user_name" +
                     " WHERE CF.Section='Candidate'";
                var cmd = database.CreateCommand(query);
                user = cmd.ExecuteQuery<CandidateFilters>().FirstOrDefault();
            }
            catch (Exception) { }
            finally { }
            return user; // returning the deatils of the logged in user.
        }


        /// <summary>
        /// This is the method that will clear all the details of candidate filters from the table. 
        /// </summary>
        /// <returns></returns>
        public int ClearCandidateFilterDetails()
        {
            var status = 0;
            try
            {
                // deleting all the details from the candidate filters table.
                status = database.DeleteAll<CandidateFilters>();

            }
            catch (Exception)
            {
                status = 0;
            }
            finally { }
            return status; // returning the status either 0 or 1.
        }
        #endregion

        #region Client Filters
        /// <summary>
        /// this is the method that will store all the filters of the client in the table
        /// </summary>
        /// <param name="objClientFilters"></param>
        /// <returns></returns>
        public int SaveClientFilters(ClientFilters objClientFilters)
        {
            int status = 0;
            try
            {
                // deleting all the records from the filters table as if there is any record present.
                database.DeleteAll<ClientFilters>();

                // saving the filters records in the client filter table.
                status = database.Insert(objClientFilters);
            }
            catch (Exception)
            {
                status = 0;
            }
            finally { }
            return status; // returning the status either 1 or 0.
        }


        /// <summary>
        /// This is the method of getting the deatils of the filters based on the section.
        /// </summary>
        /// <returns></returns>
        public ClientFilters GetClientFilters()
        {
            var user = new ClientFilters();
            try
            {
                // getting the client filters details from the table
                string query = "SELECT CF.FirstName,CF.LastName,CF.CompanyName,CF.PostCode,CF.AutoStatus,CF.AutoType,CF.City,CF.Street,CF.TypePicker,CF.StatusPicker,CF.BranchName,CF.Section,CF.UserName FROM ClientFilters CF" +
                     " INNER JOIN SaveLoginResponse login on CF.UserName=login.user_name" +
                     " WHERE CF.Section='Client'";
                var cmd = database.CreateCommand(query);
                user = cmd.ExecuteQuery<ClientFilters>().FirstOrDefault();
            }
            catch (Exception)
            {

            }
            finally { }
            return user; // returning the deatils of the logged in user.
        }


        /// <summary>
        /// This is the method that will clear all the details of client filters from the table. 
        /// </summary>
        /// <returns></returns>
        public int ClearClientFilterDetails()
        {
            var status = 0;
            try
            {
                // deleting all the details from the client filters table.
                status = database.DeleteAll<ClientFilters>();
            }
            catch (Exception)
            {
                status = 0;
            }
            finally { }
            return status; // returning the status either 0 or 1.
        }
        #endregion

        #region Settings

        /// <summary>
        /// this is the method that will store all the settings in the table
        /// </summary>
        /// <param name="objSettings"></param>
        /// <returns></returns>
        public int SaveSettings(SaveSettings objSettings)
        {
            int status = 0;
            try
            {
                // deleting all the records from the settings table as if there is any record present.
                database.DeleteAll<SaveSettings>();

                // saving the filters records in the candidate filter table.
                status = database.Insert(objSettings);
            }
            catch (Exception)
            {
                status = 0;
            }
            finally { }
            return status; // returning the status either 1 or 0.
        }
        /// <summary>
        /// This is the method of get all the settings that are stored in the table.
        /// </summary>
        /// <returns></returns>
        public SaveSettings GetSettings()
        {
            var user = new SaveSettings();
            try
            {
                // getting the login details from the table
                user = database.Table<SaveSettings>().First();
            }
            catch (Exception) { }
            finally { }
            return user; // returning the details of the settings.
        }

        #endregion


        #region Contact Filters
        /// <summary>
        /// this is the method that will store all the filters of the contact in the table
        /// </summary>
        /// <param name="objContactFilters"></param>
        /// <returns></returns>
        public int SaveContactFilters(ContactFilters objContactFilters)
        {
            int status = 0;
            try
            {
                // deleting all the records from the filters table as if there is any record present.
                database.DeleteAll<ContactFilters>();

                // saving the filters records in the contact filter table.
                status = database.Insert(objContactFilters);
            }
            catch (Exception)
            {
                status = 0;
            }
            finally { }
            return status; // returning the status either 1 or 0.
        }

        /// <summary>
        /// This is the method of getting the deatils of the filters based on the section.
        /// </summary>
        /// <returns></returns>
        public ContactFilters GetContactFilters()
        {
            var user = new ContactFilters();
            try
            {
                // getting the contact filters details from the table
                string query = "SELECT CF.FirstName,CF.LastName,CF.AutoCoreSkills,CF.AutoCoreRoles,CF.PostCode,CF.Client,CF.AutoSuburb,CF.Street,CF.Email,CF.BranchName,CF.Section,CF.UserName FROM ContactFilters CF" +
                     " INNER JOIN SaveLoginResponse login on CF.UserName=login.user_name" +
                     " WHERE CF.Section='Contact'";
                var cmd = database.CreateCommand(query);
                user = cmd.ExecuteQuery<ContactFilters>().FirstOrDefault();
            }
            catch (Exception) { }
            finally { }
            return user; // returning the deatils of the logged in user.
        }

        /// <summary>
        /// This is the method that will clear all the details of contact filters from the table. 
        /// </summary>
        /// <returns></returns>
        public int ClearContactFilterDetails()
        {
            var status = 0;
            try
            {
                // deleting all the details from the contact filters table.
                status = database.DeleteAll<ContactFilters>();
            }
            catch (Exception)
            {
                status = 0;
            }
            finally { }
            return status; // returning the status either 0 or 1.
        }
        #endregion
    }
}
