﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Recruitlive.Repository
{
    public static class App_FontFamilies
    {        
        public static string PageTitles
        {
            get
            {
                if (Device.RuntimePlatform == "iOS")
                {
                    return "Roboto Medium";
                }
                else
                {
                    return "Roboto-Medium";
                }
            }
        }

        public static string PageSubTitle
        {
            get
            {
                if (Device.RuntimePlatform == "iOS")
                {
                    return "Roboto Regular";
                }
                else
                {
                    return "Roboto-Regular";
                }
            }
        }







    }
}
